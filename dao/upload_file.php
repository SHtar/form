<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include('../function/first_load.php');

/* get post data. begin */
$_max_file_size=$_POST['MAX_FILE_SIZE'];

$_upload_file=$_FILES['_upload_file'];
//print_r($_FILES);

$_field_id=$_POST['_field_id'];
$_callback=$_POST['_callback'];
$_parameters=$_POST['_parameters'];

$_file_type=isset($_POST['_file_typ'])?$_POST['_file_typ']:NULL;

$_module_name=isset($_POST['_module_name'])&&$_POST['_module_name']?$_POST['_module_name']:NULL;
$_system_id=isset($_POST['_system_id'])&&$_POST['_system_id']?$_POST['_system_id']:NULL;



$_save_file_path=isset($_POST['_save_file_path'])&&$_POST['_save_file_path']?$_POST['_save_file_path']:NULL;
$_save_file_name=isset($_POST['_save_file_name'])&&$_POST['_save_file_name']?$_POST['_save_file_name']:NULL;
$_save_file_prefix=isset($_POST['_save_file_prefix'])&&$_POST['_save_file_prefix']?$_POST['_save_file_prefix']:NULL;
$_save_file_subfix=isset($_POST['_save_file_subfix'])&&$_POST['_save_file_subfix']?$_POST['_save_file_subfix']:NULL;
/* get post data. end */

//$upload_dir=$_doc_dir; //$_doc_path.$_source_name;
$upload_uri=$_save_file_path?$_save_file_path:'/file';
$upload_uri=substr($upload_uri, strlen($upload_uri)-1, 1)=='/'?$upload_uri:$upload_uri.'/';
$upload_uri.=$_module_name?$_module_name.'/':'';
$upload_uri.=$_system_id?$_system_id.'/':'';

$upload_dir=str_replace(substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/')), '', getcwd()).$upload_uri;

echo 'REQUEST_URI : '.$_SERVER['REQUEST_URI']."<br />";
echo 'getcwd() : '.getcwd()."<br />";
echo 'upload_dir : '.$upload_dir."<br />";

function createdir ($path, $mode) {
	if (!is_readable($path)) {
    	createdir(dirname($path), $mode);
    	if (!is_file($path)) mkdir($path, 0777);
	}
}
if (!(file_exists($upload_dir) && is_dir($upload_dir))) {
	createdir($upload_dir, 0777);
}
$upload_file_error=$_upload_file['error'];
$error_message='';
$script_text='';

if ('image'==$_file_type) {
	if (
	(
	($_upload_file['type']=='image/gif')
	||
	($_upload_file['type']=='image/jpeg')
	||
	($_upload_file['type']=='image/pjpeg')
	||
	($_upload_file['type']=='image/png')
	)
	&& ($_upload_file['size']<$_max_file_size)
	) {
		
	} else {
		
	}
} else if ('audio'==$_file_type) {
	if ($_upload_file['size']<$_max_file_size) {
		
	} else {
		
	}
} else {
	if ($_upload_file['size']<$_max_file_size) {
		
	} else {
		
	}
}

$path_parts=pathinfo($_upload_file['name']);
$save_file_name=$_save_file_name?$_save_file_name.'.'.$path_parts['extension']:$_upload_file['name'];
echo '$save_file_name : '.$save_file_name.' '.$_upload_file['size']."<br />";

/*------ process upload file. begin ------*/
if ($upload_file_error==0) {	// pass upload file.
	if (@move_uploaded_file($_upload_file['tmp_name'], $upload_dir.$save_file_name)) {	// upload succeed.
		$error_message.=get_name('upload_succeed').'<br>';
		
		$script_text='';
		$script_text.='parent.'.$_callback.'("'.$_field_id.'", "'.$upload_uri.$save_file_name.'")';
	} else {	// upload error.
		$error_message.=get_name('upload_lost').'<br>';
		$error_message.=$php_errormsg.'<br>';
		$script_text='parent.alert("'.$error_message.'");parent.hideRunMask();';
	}
} else if ($upload_file_error==1){	// can't upload file. over system define size.
	$error_message.=get_name('upload_over_sys');
	$script_text='parent.alert("'.$error_message.'");parent.hideRunMask();';
} else if ($upload_file_error==2){	// can't upload file. over custom define size.
	$error_message.=get_name('upload_over_def').' '.($_max_file_size).'k';
	$script_text='parent.alert("'.$error_message.'");parent.hideRunMask();';
} else if ($upload_file_error==3){	// can't upload file. half-baked file.
	$error_message.=get_name('upload_section');
	$script_text='parent.alert("'.$error_message.'");parent.hideRunMask();';
} else if ($upload_file_error==4){	// can't upload file. none file.
	$error_message.=get_name('upload_no_file');
	$script_text='parent.alert("'.$error_message.'");parent.hideRunMask();';
}
/*------ process upload file. end ------*/
?>
<?php echo $script_text; ?>
<script language='JavaScript'>
<?php echo $script_text; ?>
</script>