<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include '../function/first_load.php';

$_module=$_POST['_module'];
$_key=$_POST['_key'];
$_value=$_POST['_value'];
$_equal=$_POST['_equal'];
$_like=$_POST['_like'];
$_text=$_POST['_text'];
$_order_by=$_POST['_order_by'];
$_system_id=intval($_POST['_system_id']);

$_likes=explode(',', $_like);
for ($index=0; $index<count($_likes); $index++) {
	 $_likes[$index]=$_likes[$index].' LIKE "%'.$_text.'%"';
}

$columns=explode(',', $_key.','.$_value);
$query=' SELECT '.(join(',', array_unique($columns))).' FROM '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$_module.' WHERE '.join(' or ', $_likes).' AND _'.$_module.'_id!='.$_system_id.' ORDER BY '.($_order_by?$_order_by:'1 ASC').' LIMIT 0, 10';
//echo "\$query:".$query."<br />";
$results=create_query($query);	// process select.

echo json_encode($results);
?>