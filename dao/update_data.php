<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include('../function/first_load.php');

//connect();
//begin_transaction();

//commit_transaction();

$module_name_lower=isset($_POST['module_name_lower'])?$_POST['module_name_lower']:(isset($_GET['module_name_lower'])?$_GET['module_name_lower']:'');
$table_name=PENNY_CMS_DB_TABLE_PREFIX.'_'.$module_name_lower;	// define table name.
$primary_key='_'.$module_name_lower.'_id';
$primary_key_id=intval($_POST[$primary_key]);

include('../setting/'.$module_name_lower.'.php');
$columns=array();
$record=array();
foreach ($SETTING as $key=>$column) {
	$columns[]=$column['_field_name'];
	
	if (isset($_POST[$column['_field_name']])) {
		if ('int'==$column['_sql_type']) {
			$record[$column['_field_name']]=intval($_POST[$column['_field_name']]);
		} else {
			$record[$column['_field_name']]=$_POST[$column['_field_name']];
		}
	} else if (0==$primary_key_id) {
		$record[$column['_field_name']]=isset($column['_default_value'])?$column['_default_value']:'';
	}
}

/* check duplicate entity. begin */
$duplicate=FALSE;
$where='';
$_unique_index='';
if ($_unique_index) {
	$where.=' and '.$primary_key.'<>"'.$record[$primary_key].'"';
	
	$_unique_index_array = explode(',', $_unique_index);
	foreach ($_unique_index_array as $value) {
		$where.=' and '.$value.'="'.$record[$value].'"';
	}
	
	$record=create_find('select '.$primary_key.' from '.$table_name.' where '.$primary_key.'>0 '.$where.' order by '.$primary_key.' desc limit 0, 1');
	if ($record) {
		$primary_key_id=$record[$primary_key];
		$duplicate=TRUE;
	}
}
/* check duplicate entity. end */

if (!$duplicate) {
	/* set create/modify. begin */
	$is_create=false;
	$record['_update_date_time']=date('Y-m-d H:i:s');
	if (isset($record['_update_user'])) {
		$record['_update_user']=isset($_SESSION[PENNY_CMS_TAG.'session_admin_user_name'])?$_SESSION[PENNY_CMS_TAG.'session_admin_user_name']:'null';
	}
	if ($record[$primary_key]) {	// modify.
	} else {	//create.
		$is_create = true;
		$record['_create_date_time']=date('Y-m-d H:i:s');
		if (isset($record['_create_user'])) {
			$record['_create_user']=isset($_SESSION[PENNY_CMS_TAG.'session_admin_user_name'])?$_SESSION[PENNY_CMS_TAG.'session_admin_user_name']:'null';
		}
	}
	if (isset($record['_version_num'])) {
		$record['_version_num']=intval($record['_version_num']);
	}
	/* set create/modify. end */
	
	$primary_key_id=save_or_update($table_name, $record, $primary_key);
	
	$record=create_find('select * from '.$table_name.' where '.$primary_key.'='.$primary_key_id.' order by '.$primary_key.' desc limit 0, 1');

	if ($record) {
	} else {	// default value.
//		$record=$default_value;
	}
}

echo json_encode($record);
?>