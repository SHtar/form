<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include('../function/first_load.php');

$module_name_lower=isset($_POST['module_name_lower'])?$_POST['module_name_lower']:(isset($_GET['module_name_lower'])?$_GET['module_name_lower']:'');
$table_name=PENNY_CMS_DB_TABLE_PREFIX.'_'.$module_name_lower;	// define table name.

$primary_key='_'.$module_name_lower.'_id';
$primary_key_id=isset($_POST[$primary_key])?$_POST[$primary_key]:0;

$next_or_prev=isset($_POST['next_or_prev'])?$_POST['next_or_prev']:'';

if ($next_or_prev=='next') {
	$operator='>';
	$order_by='asc';
} else if ($next_or_prev=='prev') {
	$operator='<';
	$order_by='desc';
} else {
	$operator='=';
	$order_by='';
}

include('../setting/'.$module_name_lower.'.php');
$columns=array();
$default_value=array();
foreach ($SETTING as $key=>$column) {
	$columns[]=$column['_field_name'];
	$default_value[$column['_field_name']]=isset($column['_default_value'])?$column['_default_value']:'';
}
$record=create_find('select '.join(',', $columns).' from '.$table_name.' where '.$primary_key.$operator.$primary_key_id.' order by '.$primary_key.' '.$order_by.' limit 0, 1');

if ($record) {
} else {	// default value.
	$record=$default_value;
}

echo json_encode($record);
?>