<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include('../function/first_load.php');
	
$list_file=array();

$query='select * from '.PENNY_CMS_DB_TABLE_PREFIX.'_key_value where _status="true" order by _order_by desc';
$results=create_query($query);
//print_r($results);
if ($results) {
	foreach ($results as $record) {
		$key_value_type=$record['_key_value_type'];
		$key=$record['_key'];
		$value=$record['_value'];
		
		if (!isset($list_file[$key_value_type])) {
			$list_file[$key_value_type]=array();
		}
		
		$list_file[$key_value_type][]=array(
				'key'=>$key
				,
				'value'=>get_name($key)
			);
	}
}

$query='select _key as select_key, _value as select_value, _language as language from '.PENNY_CMS_DB_TABLE_PREFIX.'_resource where _status="true" order by _language';
$results=create_query($query);
$resource_file=array();
if ($results) {
	foreach ($results as $record) {
		$language=$record['language'];
		$key=$record['select_key'];
		$value=$record['select_value'];
		
		if (array_key_exists($language, $resource_file)) {
		} else {
			$resource_file[$language]=array();
		}
		$resource_file[$language][$key]=$value;
	}
}

echo 
'var $LIST='.json_encode($list_file)."\r\n".
'var $RESOURCE='.json_encode($resource_file)."\r\n".
''
;

write_file('../utils/lib/list_data.js', 
'var $LIST='.json_encode($list_file).";\r\n".
"\r\n".
'var $RESOURCE='.json_encode($resource_file).";\r\n".
''
);
?>