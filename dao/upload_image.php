<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php header('Content-Type: text/html; charset=utf-8'); ?>
<?php define('PENNY_CMS_RELATIVE_PATH', '../..');define('PENNY_CMS_RELATIVE_DIR', PENNY_CMS_RELATIVE_PATH.'/'); ?>
<?php $module_key='cms_upload_photo' ?>
<?php include(PENNY_CMS_RELATIVE_DIR.'include/first_load.php'); ?>
<?php include(PENNY_CMS_CMS_DOC_PATH.'/security/admin_user_security.php'); // security check. ?>
<?php

/* get post data. begin */
//$last_action=$HTTP_POST_VARS['last_action'];  // last submit session id.
//$_max_file_size=$_POST['_max_file_size'];
//$_doc_dir=$_POST['_doc_dir'];
//$_source_name=$HTTP_POST_VARS['_source_name'];
//$_upload_files=$HTTP_POST_FILES['_upload_files'];
$_max_file_size=$_POST['MAX_FILE_SIZE'];
$_upload_files=$_FILES['_upload_files'];

$_upload_field_id=$_POST['_upload_field_id'];
$_upload_callback=$_POST['_upload_callback'];
$_upload_image_id=$_POST['_upload_image_id'];
$_upload_parameters=$_POST['_upload_parameters'];

/* get post data. end */
//echo '$_max_file_size : '.$_max_file_size."\r\b<br />\r\b";
/*
echo 'name: '.$_upload_files['name'].'<br>';
echo 'size: '.$_upload_files['size'].'<br>';
echo 'type: '.$_upload_files['type'].'<br>';
echo 'error: '.$_upload_files['error'].'<br>';
echo 'tmp_name: '.$_upload_files['tmp_name'].'<br>';
echo '_max_file_size: '.$_max_file_size.'<br>';
echo '_upload_path: '.$_upload_path.'<br>';
*/

//$upload_dir=$_doc_dir; //$_doc_path.$_source_name;
$upload_uri='/file/img/';
$upload_dir=PENNY_CMS_DOC_PATH.$upload_uri;
// if (!(file_exists($upload_dir) && is_dir($upload_dir))) {
	// mkdir($upload_dir);
// }
$upload_file_error=$_upload_files['error'];
$error_message='';
$script_text='';

if (
(
($_upload_files['type']=='image/gif')
||
($_upload_files['type']=='image/jpeg')
||
($_upload_files['type']=='image/pjpeg')
||
($_upload_files['type']=='image/png')
)
&& ($_upload_files['size']<20000)
) {
	
} else {
	
}

// $save_file_name=md5(time().'_'.$_upload_files['name']);
// if (strpos($_upload_files['name'], '.jpg')) {
	// $save_file_name.='.jpg';
// } else if (strpos($_upload_files['name'], '.png')) {
	// $save_file_name.='.png';
// } else if (strpos($_upload_files['name'], '.gif')) {
	// $save_file_name.='.gif';
// } else {
	// $save_file_name=$_upload_files['name'].'.txt';
// }
$save_file_name=$_upload_files['name'];

/*------ process upload file. begin ------*/
if ($upload_file_error==0) {	// pass upload file.
	if (@move_uploaded_file($_upload_files['tmp_name'], $upload_dir.$save_file_name)) {	// upload succeed.
		$error_message.=get_name('upload_succeed').'<br>';
		
		$script_text='';
//		$script_text.='alert("'.$r.' : '.$i_w.' : '.$img_width.' , '.$i_h.' : '.$img_height.' , '.$n_w.' : '.$n_h.'");';
//		$script_text.='alert("'.$temp_new_img.'");';
		$script_text.='parent.'.$_upload_callback.'("'.$_upload_field_id.'", "'.$upload_uri.$save_file_name.'", "'.$_upload_image_id.'")';
	} else {	// upload error.
		$error_message.=get_name('upload_lost').'<br>';
		$error_message.=$php_errormsg.'<br>';
		$script_text='parent.alert("'.$error_message.'");parent.closeRunMask();';
	}
} else if ($upload_file_error==1){	// can't upload file. over system define size.
	$error_message.=get_name('upload_over_sys');
	$script_text='parent.alert("'.$error_message.'");parent.closeRunMask();';
} else if ($upload_file_error==2){	// can't upload file. over custom define size.
	$error_message.=get_name('upload_over_def').' '.($_max_file_size).'k';
	$script_text='parent.alert("'.$error_message.'");parent.closeRunMask();';
} else if ($upload_file_error==3){	// can't upload file. half-baked file.
	$error_message.=get_name('upload_section');
	$script_text='parent.alert("'.$error_message.'");parent.closeRunMask();';
} else if ($upload_file_error==4){	// can't upload file. none file.
	$error_message.=get_name('upload_no_file');
	$script_text='parent.alert("'.$error_message.'");parent.closeRunMask();';
}
/*------ process upload file. end ------*/
?>
<?php echo $script_text; ?>
<script language='JavaScript'>
<?php echo $script_text; ?>
</script>