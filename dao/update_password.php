<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include('../function/first_load.php');

$_module_name=$_POST['_module_name'];
$_user_id=$_POST['_user_id'];
$_old_password=$_POST['_old_password'];
$_password=$_POST['_password'];

$process_status=false;
$message='';	//  message.
$_user_password='';

$query=
' select _admin_user_id, _admin_user_name, _admin_user_e_mail, _admin_user_password, _admin_user_group, _status from '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$_module_name.
' where _'.$_module_name.'_id='.intval($_user_id).
' and _'.$_module_name.'_password="'.$_old_password.'"'
;
//echo $query;
$record=create_find($query);	// process select.
if ($record) {
	$_user_password=crypt(strtolower($_password), strtolower($record['_'.$_module_name.'_name']));	// encrypt password.
	
	// update sql.
	$query=
			' update '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$_module_name.' set '.
			' _'.$_module_name.'_password=\''.$_user_password.'\''.
			' where _'.$_module_name.'_id='.intval($_user_id);
		;
	create_update($query);
	
	$process_status=true;
}

/* json. begin */
echo json_encode(
array(
'process_status'=>$process_status,
'password'=>$_user_password,
)
);	// print json.
/* json. end */
?>