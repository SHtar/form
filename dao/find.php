<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
$module_name_lower=isset($_POST['module_name_lower'])?$_POST['module_name_lower']:(isset($_GET['module_name_lower'])?$_GET['module_name_lower']:'');
$table_name=PENNY_CMS_DB_TABLE_PREFIX.'_'.$module_name_lower;	// define table name.
$primary_key='_'.$module_name_lower.'_id';
$primary_key_id=isset($_POST[$primary_key])?$_POST[$primary_key]:0;

$identification_code=isset($_POST['identification_code'])?$_POST['identification_code']:NULL;

// Page
$SESSION_TAG=PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_this_page_'.$identification_code;
if (isset($_POST['this_page'])) {
	if (intval($_POST['this_page'])>=0) {
		$this_page=intval($_POST['this_page']);
	} else if (intval($_POST['this_page'])==-1) {
		$this_page=intval($_SESSION[$SESSION_TAG])>=0?intval($_SESSION[$SESSION_TAG]):0;
	} else {
		$this_page=0;
	}
} else {
	$this_page=0;
}
$_SESSION[$SESSION_TAG]=$this_page;

// Show rows.
$show_rows=10;
$SESSION_TAG=PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_show_rows_'.$identification_code;
if (isset($_SESSION[$SESSION_TAG]) && ((integer)$_SESSION[$SESSION_TAG]>0)) {
	$show_rows=$_SESSION[$SESSION_TAG];
}

$SESSION_TAG=PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_where_'.$identification_code;
$where=isset($_SESSION[$SESSION_TAG])?$_SESSION[$SESSION_TAG]:'';	// get where from session.

include('../setting/'.$module_name_lower.'.php');
$columns=array();
$list_names=array();
foreach ($SETTING as $key=>$column) {
	$columns[]=$column['_field_name'];
	if (isset($column['list_name'])) {
		$list_names[$column['_field_name']]=$column['_list_name'];
	}
}

/*------ Set show columns ------*/
$SESSION_TAG=PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_show_columns_'.$identification_code;
if (isset($_SESSION[$SESSION_TAG]) && (count($_SESSION[$SESSION_TAG])>0)) {
	$_show_columns=$_SESSION[$SESSION_TAG];
} else {
	$_show_columns=$columns;
	$_SESSION[$SESSION_TAG]=$_show_columns;
}
//$_show_columns_a=explode(',', $_show_columns);

/* Select count. */
$SESSION_TAG = PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_result_count_'.$identification_code;
if (isset($_POST['this_page']) && ($_POST['this_page']>0)) {
	$result_count=$_SESSION[$SESSION_TAG];
} else {
	$result_count=create_count($table_name, $where);
	$_SESSION[$SESSION_TAG]=$result_count;
}

/* Select data. */
$query=' select '.join(',', $columns).' from '.$table_name.' where 1=1 '.$where.' order by 1 desc limit '.($this_page * $show_rows).', '.$show_rows;
//echo "\$query:".$query."<br />";
$results=create_query($query);	// process select.
$row_count=$results?count($results):0;	// get row count.
if ($row_count>0) {	// have results.
	for ($i=0; $i<$row_count; $i++) {
		$record=$results[$i];
		foreach ($columns as $key=>$column) {
			if (isset($list_names[$column])) {
				$record[$column]=get_name(get_list_item($list_names[$column], $record[$column]));
			} else {
				$record[$column]=htmlspecialchars($record[$column], ENT_QUOTES, 'UTF-8');
			}
		}
	}
}

echo json_encode(
array(
'results'=>$results,
'result_count'=>$result_count,
'show_rows'=>$show_rows,
'this_page'=>$this_page
)
);
?>