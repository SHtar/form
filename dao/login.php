<?php
/*** ***
License
This software is published under the BSD license as listed below.

Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

. Neither the name of the pennycms.com nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
define('PENNY_CMS_RELATIVE_PATH', '../..');define('PENNY_CMS_RELATIVE_DIR', PENNY_CMS_RELATIVE_PATH.'/');
$module_key='cms_login';
//TODO 未知bug
//include(PENNY_CMS_RELATIVE_DIR.'include/first_load.php');
//include(PENNY_CMS_CMS_DOC_PATH.'/security/admin_user_unregister.php'); // unregister info.
include('../function/first_load.php');


?>
<?php
//define(PENNY_CMS_DB_TABLE_PREFIX,"pennycms");
$table_name=PENNY_CMS_DB_TABLE_PREFIX.'_admin_user';	// define table name.

$_admin_user_name =$_POST['_admin_user_name'];
$_admin_user_password = $_POST['_password'];

$error_message = '';	//  error message.

$ref_times = 0;	// auto refresh time.

/*------ process. begin ------*/
/* select data. begin */
// select sql.
$query  = ' select _admin_user_id, _admin_user_name, _admin_user_e_mail, _admin_user_password, _admin_user_group, _status from '.$table_name;
$query .= ' where _admin_user_name = \''.$_admin_user_name.'\'';
$query .= ' and _admin_user_password = \''.crypt($_admin_user_password, $_admin_user_name).'\'';
$query .= ' and _status = \'true\'';


$result = create_find($query);	// process select.
if ($result) {
	$row_admin_user_id = $result['_admin_user_id'];
	$row_admin_user_name = $result['_admin_user_name'];
	$row_admin_user_e_mail = $result['_admin_user_e_mail'];
	$row_admin_user_password = $result['_admin_user_password'];
	$row_admin_user_group = $result['_admin_user_group'];
	$row_status = $result['_status'];

	//开启session
	if(!session_id()){session_start();}
	/* register user info. begin */
//	session_register(PENNY_CMS_TAG.'session_admin_user_id');
	$_SESSION[PENNY_CMS_TAG.'session_admin_user_id'] = $row_admin_user_id;

//	session_register(PENNY_CMS_TAG.'session_admin_user_name');
	$_SESSION[PENNY_CMS_TAG.'session_admin_user_name'] = $row_admin_user_name;

//	session_register(PENNY_CMS_TAG.'session_admin_user_e_mail');
	$_SESSION[PENNY_CMS_TAG.'session_admin_user_e_mail'] = $row_admin_user_e_mail;

//	session_register(PENNY_CMS_TAG.'session_admin_user_group');
	$_SESSION[PENNY_CMS_TAG.'session_admin_user_group'] = $row_admin_user_group;
	/* register user info. end */

	$process_status = true;

} else {
	$process_status = false;
	$error_message .= get_name('login_error');
}
/* select data. end */
/*------ process. end ------*/
//$ref_url = '/cms/index.php?PHPSESSID='.session_id();	// refresh url.

/* json. begin */
$data_json = array(
	'process_status'=>$process_status,
	'message'=>$error_message,
);
echo json_encode($data_json);	// print json.
/* json. end */
?>
