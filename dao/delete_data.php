<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include('../function/first_load.php');

$module_name_lower=isset($_POST['module_name_lower'])?$_POST['module_name_lower']:(isset($_GET['module_name_lower'])?$_GET['module_name_lower']:'');
$table_name=PENNY_CMS_DB_TABLE_PREFIX.'_'.$module_name_lower;	// define table name.
$primary_key='_'.$module_name_lower.'_id';
$primary_key_id=intval($_POST[$primary_key]);

$process_status=false;

/*------ process data. begin ------*/
if ($primary_key_id) {	// if check data error then display error message.
	$record=create_find('select * from '.$table_name.' where '.$primary_key.' in ('.$primary_key_id.')');
	if ($record) {
		$record['_delete_user']=isset($_SESSION[PENNY_CMS_TAG.'session_admin_user_name'])?$_SESSION[PENNY_CMS_TAG.'session_admin_user_name']:'null';
		$record['_delete_date_time']=date('Y-m-d H:i:s');
		
		// delete sql.
		$query='delete from '.$table_name.' where '.$primary_key.' in ('.$primary_key_id.')';
		
		if (create_delete($query)) {	// if delete success.
//			$message.=get_name('data_delete_succeed');
			$process_status=true;
			
			write_file('../data/delete/'.$module_name_lower.'_'.$primary_key_id.'_'.date('Ymd_His').'.php', "<?php\r\n".'$_RECORD='.var_export($record, TRUE)."\r\n?>");
			
			if ('news'==$module_name_lower || 'products'==$module_name_lower || 'merchandise'==$module_name_lower) {
				$file_name=PENNY_CMS_WEB_DOC_PATH.'/'.$module_name_lower.'/'.$primary_key_id.'.html';
				unlink($file_name);
				$index=1;
				while ($index) {
					$file_name=PENNY_CMS_WEB_DOC_PATH.'/'.$module_name_lower.'/'.$primary_key_id.'_'.($index++).'.html';
					if (file_exists($file_name)) {
						unlink($file_name);
					} else {
						$index=0;
					}
				}
			}
		} else {	// if delete error.
//			$message.=get_name('data_delete_error');
		}
	} else {
//		$message.=get_name('data_delete_succeed');
		$process_status=true;
	}
} else {	// delete.
//	$message.=get_name('data_delete_error').get_name('!');
}
/*------ process data. end ------*/

//echo json_encode($DATAS_CACHE);
echo $process_status;
?>