<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
if (!isset($_SESSION[PENNY_CMS_TAG.'session_admin_user_id'])) {
//	break;
}

$module_name_lower=$_POST['module_name_lower'];
$field_name=$_POST['field_name'];
$field_value=$_POST['field_value'];
$operator=isset($_POST['operator'])?$_POST['operator']:'=';
$identification_code=isset($_POST['identification_code'])?$_POST['identification_code']:NULL;

$SESSION_TAG=PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_where_str';
$where_str=isset($_SESSION[$SESSION_TAG])?$_SESSION[$SESSION_TAG]:array();
if ($operator) {
	if (('like'==$operator||'='==$operator) && (strlen($field_value)>0)) {
		$field_name_array=explode(',', $field_name);
		foreach ($field_name_array as $_field_name) {
			$where_str[$_field_name]=$field_value;
		}
	} else {
		$where_str=array();
	}
} else {
	$where_str=array();
}
$_SESSION[$SESSION_TAG]=$where_str;

/*------ Where array. ------*/
$SESSION_TAG=PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_where_array_'.$identification_code;
$where_array=isset($_SESSION[$SESSION_TAG])?$_SESSION[$SESSION_TAG]:array();
if ($operator) {
	if ($operator=='in') {
		if (isset($where_array[$field_name])) {
			$mixed=array_search($field_value, $where_array[$field_name]);
			
			if ($mixed===false) {
				array_push($where_array[$field_name], $field_value);
			} else {
				array_splice($where_array[$field_name], $mixed, 1);
			}
		} else {
			$where_array[$field_name]=array();
			array_push($where_array[$field_name], $field_value);
		}
	}
} else {
	$where_array=array();
}
$_SESSION[$SESSION_TAG]=$where_array;

/*------ Where sql. ------*/
$where='';
if (is_array($where_array) && count($where_array)) {
	foreach ($where_array as $field_name=>$list) {
		if (count($list)>0) {
			$where.=' and '.getArrayStringInSQL($field_name, $list).'';
		}
	}
}
if (is_array($where_str) && count($where_str)) {
	$where_in_array=array();
	foreach ($where_str as $field_name=>$field_value) {
		if ($field_value) {
			$where_in_array[]='('.$field_name.' like "%'.$field_value.'%")';
		}
	}
	if (count($where_in_array)) {
		$where.=' and ('.join(' or ', $where_in_array).')';
	}
}
$SESSION_TAG=PENNY_CMS_TAG.'_cms_'.$module_name_lower.'_where_'.$identification_code;
$_SESSION[$SESSION_TAG]=$where;
?>