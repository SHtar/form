<?php 
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
/* user setting. begin */
if(!defined('PENNY_CMS_SERVER_PROTOCOL')){define('PENNY_CMS_SERVER_PROTOCOL', urldecode('http'));}	// http or https.
if(!defined('PENNY_CMS_TAG')){define('PENNY_CMS_TAG', urldecode('flower'));}	// Pennycms System Tag Name.
if(!defined('PENNY_CMS_DOC_FOLDER')){define('PENNY_CMS_DOC_FOLDER', urldecode('%2Fflower'));}	// system doc directory. sample: '' or '/parent_company' or '/parent_company/filiale'.
if(!defined('PENNY_CMS_URL_FOLDER')){define('PENNY_CMS_URL_FOLDER', urldecode('%2Fflower'));}	// system url directory. sample: '' or '/parent_company' or '/parent_company/filiale'.
if(!defined('PENNY_CMS_CMS_FOLDER')){define('PENNY_CMS_CMS_FOLDER', urldecode('cms'));}	// cms or encrypt_cms or custom others.
if(!defined('PENNY_CMS_TEMPLET_FOLDER')){define('PENNY_CMS_TEMPLET_FOLDER', urldecode('templet'));}	// templet or blank or demo 's templet.
if(!defined('PENNY_CMS_TEMPLATE_FOLDER')){define('PENNY_CMS_TEMPLATE_FOLDER', urldecode('template%2Cmobile'));}	// customer template.
if(!defined('PENNY_CMS_WEB_FOLDER')){define('PENNY_CMS_WEB_FOLDER', urldecode('web%2Cwap'));}	//  web or cache or custom others store static html.
if(!defined('PENNY_CMS_DB_SERVER')){define('PENNY_CMS_DB_SERVER', urldecode('dns.e6.dns-dns.net'));}	// db address. sample: '127.0.0.1' or 'localhost' or 'your.DB.Domain' or 'IP addrsss'.
if(!defined('PENNY_CMS_DB_NAME')){define('PENNY_CMS_DB_NAME', urldecode('sq_flower'));}	// db name.
if(!defined('PENNY_CMS_DB_USER_NAME')){define('PENNY_CMS_DB_USER_NAME', urldecode('sq_flower'));}	// db user name.
if(!defined('PENNY_CMS_DB_PASSWORD')){define('PENNY_CMS_DB_PASSWORD', urldecode('flower15963'));}	// db password.
if(!defined('PENNY_CMS_DB_TABLE_PREFIX')){define('PENNY_CMS_DB_TABLE_PREFIX', urldecode('flower'));}	// table user prefix. sample: '' or 'your_name'.
if(!defined('PENNY_CMS_SYSTEM_LANGUAGE')){define('PENNY_CMS_SYSTEM_LANGUAGE', urldecode('zh_cn'));}	// default language.
if(!defined('PENNY_CMS_DEFAULT_LANGUAGE')){define('PENNY_CMS_DEFAULT_LANGUAGE', urldecode('zh_cn'));}	// default language.
/* user setting. end */
?>