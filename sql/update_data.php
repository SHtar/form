<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
header('Content-Type: text/html; charset=utf-8');
include('../security.php');
include('../function.php');

$module_name_lower=isset($_POST['module_name_lower'])?$_POST['module_name_lower']:(isset($_GET['module_name_lower'])?$_GET['module_name_lower']:'');

$DATAS_CACHE=array();
if (file_exists('../data/'.$module_name_lower.'s_cache.php')) {
	include('../data/'.$module_name_lower.'s_cache.php');
	if (!is_array($DATAS_CACHE)) {
		$DATAS_CACHE=array();
	}
}

$DATA_CACHE=array();
if (file_exists('../setting/'.$module_name_lower.'.php')) {
	include('../setting/'.$module_name_lower.'.php');
	foreach ($SETTING as $key=>$row) {
		$DATA_CACHE[$row[0]]=$_POST[$row[0]];
	}
}

if ($DATA_CACHE['_'.$module_name_lower.'_id'] && isset($DATAS_CACHE[$DATA_CACHE['_'.$module_name_lower.'_id']])) {
	$DATAS_CACHE[$DATA_CACHE['_'.$module_name_lower.'_id']]=$DATA_CACHE;
} else {
	$DATA_CACHE['_'.$module_name_lower.'_id']=time();
	$DATAS_CACHE[$DATA_CACHE['_'.$module_name_lower.'_id']]=$DATA_CACHE;
}

write_file('../data/'.$module_name_lower.'s_cache.php', "<?php\r\n".'$DATAS_CACHE='.var_export($DATAS_CACHE, TRUE)."\r\n?>");
script_nat($DATAS_CACHE);

echo json_encode($DATAS_CACHE);
?>