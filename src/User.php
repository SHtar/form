<?php
/**
 * Created by PhpStorm.
 * User: hack
 * Date: 16/6/29
 * Time: 下午3:10
 */

use Doctrin\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="pennycms_admin_user")
 **/
class User
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    public $_admin_user_id;
    /** @Column(type="string") **/
    public $_admin_user_name;
    /** @Column(type="string") **/
    public $_admin_user_actual_name;
    /** @Column(type="string") **/
    public $_admin_user_password;
    /** @Column(type="text") **/
    public $_admin_user_group;
    /** @Column(type="string") **/
    public $_admin_user_e_mail;
    /** @Column(type="string") **/
    public $_status;
    /** @Column(type="string") **/
    public $_create_user;
    /** @Column(type="string") **/
    public $_create_date_time;
    /** @Column(type="string") **/
    public $_update_user;
    /** @Column(type="string") **/
    public $_update_date_time;
    /** @Column(type="integer") **/
    public $_version_num;

}