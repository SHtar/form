﻿/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/

function $ () {
	return "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
}

function $$ () {
	$node = arguments[1]||document;
	return $node.getElementsByTagName(arguments[0]);
}

String.prototype.lengths = function() {
	return this.replace('/[^\x00-\xff]/ig',"**").length; 
}
String.prototype.trim = function() {
	var str = this,
	whitespace = ' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000';
	for (var i=0, len=str.length; i<len; i++) {
		if (whitespace.indexOf(str.charAt(i))===-1) {
			str = str.substring(i);
			break;
		}
	}
	for (i=str.length-1; i>=0; i--) {
		if (whitespace.indexOf(str.charAt(i))===-1) {
			str = str.substring(0, i+1);
			break;
		}
	}
	
	return whitespace.indexOf(str.charAt(0))===-1 ? str : '';
}

function getRequestParams () {
	var $url=location.search;
	var $request=new Object();
	if ($url.indexOf("?")!=-1) {
		var $str=$url.substr(1);
		var $strs=$str.split("&");
		for (var $index=0; $index<$strs.length; $inde++) {
			var $strss=$strs[$index].split("=");
			$request[$strss[0]]=$strss[1];
		}
	}
	return $request;
}

function getRequestParam () {
//   var $reg=new RegExp("(^|&)"+arguments[0]+"=([^&]*)(&|$)","i");
	var $r=window.location.search.substr(1).match(new RegExp("(^|&)"+arguments[0]+"=([^&]*)(&|$)","i"));
	if ($r!=null) return ($r[2]); return null;
}

// function getKeyCode () {
	// var $event=Pennyutils.events.getEvent(arguments[0]);
	// return $event.keyCode;
// }



var $MASK_TARGET=null;
var $MASK_INNER_TARGET=null;
var $MASK_IS_NEW=true;
function showMask () {
	if ($MASK_TARGET) {
		return;
	}
//	var s = "";
//	s+="\n document.body.clientWidth :"+ document.body.clientWidth ;
//	s+="\n document.body.clientHeight :"+ document.body.clientHeight ;
//	s+="\n document.documentElement.clientWidth :"+document.documentElement.clientWidth;
//	s+="\n document.documentElement.clientHeight :"+document.documentElement.clientHeight;
//	s+="\n document.body.offsetWidth :"+ document.body.offsetWidth ;
//	s+="\n document.body.offsetHeight :"+ document.body.offsetHeight ;
//	s+="\n document.body.scrollWidth :"+ document.body.scrollWidth ;
//	s+="\n document.body.scrollHeight :"+ document.body.scrollHeight ;
//	s+="\n document.body.scrollTop :"+ document.body.scrollTop ;
//	s+="\n document.documentElement.scrollTop :"+ document.documentElement.scrollTop ;
//	s+="\n document.body.scrollLeft :"+ document.body.scrollLeft ;
//	s+="\n window.screenTop :"+ window.screenTop ;
//	s+="\n window.screenLeft :"+ window.screenLeft ;
//	s+="\n window.screen.height :"+ window.screen.height ;
//	s+="\n window.screen.width :"+ window.screen.width ;
//	s+="\n window.screen.availHeight :"+ window.screen.availHeight ;
//	s+="\n window.screen.availWidth :"+ window.screen.availWidth ;
//	s+="\n window.screen.colorDepth :"+ window.screen.colorDepth ;
//	s+="\n window.screen.deviceXDPI :"+ window.screen.deviceXDPI ;
//	alert(s);
	//var sWidth=document.documentElement.clientWidth>document.body.clientWidth?document.documentElement.clientWidth:document.body.clientWidth;
	//var sHeight=document.documentElement.clientHeight>document.body.clientHeight?document.documentElement.clientHeight:document.body.clientHeight;
	//if(navigator.appName=="Microsoft Internet Explorer") {
	//} else {
	//	sHeight+=25;	// fill.
	//}
	//var bgObj=document.createElement("div");
	//$MASK_TARGET = bgObj;
	$MASK_TARGET=document.createElement("div");
	$MASK_TARGET.setAttribute('id','bgDiv');
	$MASK_TARGET.style.background="#000022";
	$MASK_TARGET.style.position="absolute";
	if(navigator.appName=="Microsoft Internet Explorer") {
		$MASK_TARGET.style.filter="alpha(opacity=50)";
	} else {
		$MASK_TARGET.style.MozOpacity=0.5;
		$MASK_TARGET.style.opacity=0.5;
	}
	$MASK_TARGET.style.top=0;
	$MASK_TARGET.style.left=0;
//	$MASK_TARGET.style.width="100%";
//	$MASK_TARGET.style.height=sHeight + "px";
	$MASK_TARGET.style.width=(document.documentElement.clientWidth>document.body.clientWidth?document.documentElement.clientWidth:document.body.clientWidth)+"px";
	$MASK_TARGET.style.height=(document.documentElement.clientHeight>document.body.clientHeight?document.documentElement.clientHeight:document.body.clientHeight)+"px";
	$MASK_TARGET.style.zIndex=10000;
	document.body.appendChild($MASK_TARGET);
	
	if ($MASK_IS_NEW) {
		$MASK_IS_NEW=false;
		window.addOnResizeListener(function(){
				if ($MASK_TARGET) {
					$MASK_TARGET.style.top=0;
					$MASK_TARGET.style.left=0;
					$MASK_TARGET.style.width=(document.documentElement.clientWidth>document.body.clientWidth?document.documentElement.clientWidth:document.body.clientWidth)+"px";
					$MASK_TARGET.style.height=(document.documentElement.clientHeight>document.body.clientHeight?document.documentElement.clientHeight:document.body.clientHeight)+"px";
				}
			});
	}
}
function hideMask () {
	if ($MASK_TARGET) {
		document.body.removeChild($MASK_TARGET);
		$MASK_TARGET=null;
	}
	if ($MASK_INNER_TARGET) {
		document.body.removeChild($MASK_INNER_TARGET);
		$MASK_INNER_TARGET=null;
	}
}

var $MASK_RUN_TARGET=null;
var $MASK_RUN_INNER_TARGET=null;
var $MASK_RUN_IS_NEW=true;
function showRunMask () {
	if ($MASK_RUN_TARGET) {
		return;
	}
	
	//var bgObj=document.createElement("div");
	//$MASK_RUN_TARGET=bgObj;
	$MASK_RUN_TARGET=document.createElement("div");
	$MASK_RUN_TARGET.setAttribute('id','bgDiv');
	$MASK_RUN_TARGET.style.background="#ffffff";
	$MASK_RUN_TARGET.style.position="absolute";
	if(navigator.appName=="Microsoft Internet Explorer") {
		$MASK_RUN_TARGET.style.filter="alpha(opacity=50)";
	} else {
		$MASK_RUN_TARGET.style.MozOpacity=0.5;
		$MASK_RUN_TARGET.style.opacity=0.5;
	}
//	$MASK_RUN_TARGET.style.top=document.documentElement.scrollTop+"px";
	$MASK_RUN_TARGET.style.top=0;
	$MASK_RUN_TARGET.style.left=0;
	$MASK_RUN_TARGET.style.width=(document.documentElement.clientWidth>document.body.clientWidth?document.documentElement.clientWidth:document.body.clientWidth)+"px";
	$MASK_RUN_TARGET.style.height=(document.documentElement.clientHeight>document.body.clientHeight?document.documentElement.clientHeight:document.body.clientHeight)+"px";
	$MASK_RUN_TARGET.style.zIndex=30000;
	$ss="margin:0 auto;";
	
	document.body.appendChild($MASK_RUN_TARGET);
	
//	var loadingObj=document.createElement("div");
//	$MASK_RUN_INNER_TARGET=loadingObj;
	$MASK_RUN_INNER_TARGET=document.createElement("div");
	var img_path=(arguments[0]?arguments[0]:Pennyutils.loadingImg?Pennyutils.loadingImg:"")+"/utils/img/loading.gif";
	$MASK_RUN_INNER_TARGET.innerHTML="<img src=\""+img_path+"\" alt=\"loading\" title=\"loading\" />";
	$MASK_RUN_INNER_TARGET.style.position="absolute";
	$MASK_RUN_INNER_TARGET.style.zIndex=30100;
	document.body.appendChild($MASK_RUN_INNER_TARGET);
	
	$MASK_RUN_INNER_TARGET.style.left=((document.documentElement.clientWidth>$MASK_RUN_INNER_TARGET.offsetWidth?document.documentElement.clientWidth-$MASK_RUN_INNER_TARGET.offsetWidth:0)/2)+"px";
	$MASK_RUN_INNER_TARGET.style.top=((document.documentElement.clientHeight>$MASK_RUN_INNER_TARGET.offsetHeight?document.documentElement.clientHeight-$MASK_RUN_INNER_TARGET.offsetHeight:0)/2+document.documentElement.scrollTop)+"px";
	

	if ($MASK_RUN_IS_NEW) {
		$MASK_RUN_IS_NEW=false;
		
		window.addOnScrollListener(function(){
			if ($MASK_RUN_TARGET) {
		//		$MASK_RUN_TARGET.style.top=document.documentElement.scrollTop+"px";
		//		$MASK_RUN_INNER_TARGET.style.left=((document.documentElement.clientWidth>$MASK_RUN_INNER_TARGET.offsetWidth?document.documentElement.clientWidth-$MASK_RUN_INNER_TARGET.offsetWidth:0)/2)+"px";
		//		$MASK_RUN_INNER_TARGET.style.top=((document.documentElement.clientHeight>$MASK_RUN_INNER_TARGET.offsetHeight?document.documentElement.clientHeight-$MASK_RUN_INNER_TARGET.offsetHeight:0)/2+document.documentElement.scrollTop)+"px";
				$MASK_RUN_INNER_TARGET.style.left=((document.documentElement.clientWidth>$MASK_RUN_INNER_TARGET.offsetWidth?document.documentElement.clientWidth-$MASK_RUN_INNER_TARGET.offsetWidth:0)/2)+"px";
				$MASK_RUN_INNER_TARGET.style.top=((document.documentElement.clientHeight>$MASK_RUN_INNER_TARGET.offsetHeight?document.documentElement.clientHeight-$MASK_RUN_INNER_TARGET.offsetHeight:0)/2+document.documentElement.scrollTop)+"px";
			}
		});
		window.addOnResizeListener(function(){
			if ($MASK_RUN_TARGET) {
				$MASK_RUN_TARGET.style.top=0;
				$MASK_RUN_TARGET.style.left=0;
				$MASK_RUN_TARGET.style.width=(document.documentElement.clientWidth>document.body.clientWidth?document.documentElement.clientWidth:document.body.clientWidth)+"px";
				$MASK_RUN_TARGET.style.height=(document.documentElement.clientHeight>document.body.clientHeight?document.documentElement.clientHeight:document.body.clientHeight)+"px";
				$MASK_RUN_TARGET.style.top=document.documentElement.scrollTop+"px";
				$MASK_RUN_INNER_TARGET.style.left=((document.documentElement.clientWidth>$MASK_RUN_INNER_TARGET.offsetWidth?document.documentElement.clientWidth-$MASK_RUN_INNER_TARGET.offsetWidth:0)/2)+"px";
				$MASK_RUN_INNER_TARGET.style.top=((document.documentElement.clientHeight>$MASK_RUN_INNER_TARGET.offsetHeight?document.documentElement.clientHeight-$MASK_RUN_INNER_TARGET.offsetHeight:0)/2+document.documentElement.scrollTop)+"px";
			}
		});
	}
}
function hideRunMask () {
	if ($MASK_RUN_TARGET) {
		document.body.removeChild($MASK_RUN_TARGET);
		$MASK_RUN_TARGET=null;
	}
	if ($MASK_RUN_INNER_TARGET) {
		document.body.removeChild($MASK_RUN_INNER_TARGET);
		$MASK_RUN_INNER_TARGET=null;
	}
}