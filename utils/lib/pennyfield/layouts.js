/**
 * LayoutsInputField
 */
function getLayoutsInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field=document.createElement("div");
	Pennyutils.setClassName($input_field, "input_border");
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_field_input=document.createElement("input");
	Pennyutils.setIdName($input_field_input, $input_field_id);
	Pennyutils.setClassName($input_field_input, "l input iw"+($_input_width-1)+"s");
	$input_field_input.name=$_field_name;
	
	if (document.uniqueID) {
	} else {
		$input_field_input.type="text";
	}
	$input_field.appendChild($input_field_input);
	
	var $input_field_sub=document.createElement("div");
	Pennyutils.setClassName($input_field_sub,"r icon_upload input_icon");
	Pennyutils.events.addEventHandler($input_field_sub, "click", new Function("showLayoutsEditor('"+$input_field_id+"');"));
	$input_field_sub.innerHTML="&nbsp;";
	$input_field.appendChild($input_field_sub);
	
	var $input_field_sub=document.createElement("div");
	Pennyutils.setClassName($input_field_sub,"c");
	$input_field.appendChild($input_field_sub);
	
	return $input_field;
}
GET_EDITOR_INPUT["layouts"]=getLayoutsInputField;
var $LAYOUTS_WIN=null;
function showLayoutsEditor ($input_field_id) {
	if (null==$LAYOUTS_WIN) {
		$LAYOUTS_WIN=createWindow();
		
		var $layouts_pannel=document.createElement("div");
		
		var $layouts_widget_input=document.createElement("input");
		Pennyutils.setIdName($layouts_widget_input, "__layouts_input__");
		Pennyutils.setClassName($layouts_widget_input, "dn");
		Pennyutils.setCssStyle($layouts_widget_input, "height:0px;");
		$layouts_widget_input.readOnly="readOnly";
		if (document.uniqueID) {
		} else {
			$layouts_widget_input.type="text";
		}
		$layouts_pannel.appendChild($layouts_widget_input);
		
		// Layouts.
		var $layouts_node=document.createElement("div");
		Pennyutils.setIdName($layouts_node, "layouts");
		Pennyutils.setClassName($layouts_node, "b1 m5 p5");
		$layouts_pannel.appendChild($layouts_node);
		
		// Layouts.
		var $layout_items_node=document.createElement("div");
		Pennyutils.setIdName($layout_items_node, "layout_itmes");
		Pennyutils.setClassName($layout_items_node, "b1 m5 p5 tl");
		$layouts_pannel.appendChild($layout_items_node);
		
		// Editor.
		var $layout_prototype_editor_node=document.createElement("div");
		Pennyutils.setIdName($layout_prototype_editor_node, "layout_prototype_editor");
		Pennyutils.setClassName($layout_prototype_editor_node, "b1 m5 p5");
		$layouts_pannel.appendChild($layout_prototype_editor_node);
		
		var $form_name="layout_prototype_form";
		$FORM_CONFIG[$form_name]=[
				{"_label_name":"_level","_field_name":"_level","_edit_field":"text","_gridx":"0","_gridy":"0"}
				,
				{"_label_name":"_subject","_field_name":"_subject","_edit_field":"text","_gridx":"1","_gridy":"0"}
				,
				{"_label_name":"_bound","_field_name":"_bound","_edit_field":"text","_gridx":"0","_gridy":"1"}
				,
				{"_label_name":"_name","_field_name":"_name","_edit_field":"text","_gridx":"1","_gridy":"1"}
				,
				{"_label_name":"_link","_field_name":"_link","_edit_field":"text","_gridx":"2","_gridy":"1"}
				,
				{"_label_name":"_action","_field_name":"_action","_edit_field":"text","_gridx":"3","_gridy":"1"}
				,
				{"_label_name":"_node","_field_name":"_node","_edit_field":"text","_gridx":"0","_gridy":"2"}
				,
				{"_label_name":"_node_id","_field_name":"_node_id","_edit_field":"text","_gridx":"0","_gridy":"2"}
				,
				{"_label_name":"_height","_field_name":"_height","_edit_field":"text","_gridx":"1","_gridy":"2"}
				,
				{"_label_name":"_width","_field_name":"_width","_edit_field":"text","_gridx":"0","_gridy":"2"}
				,
				{"_label_name":"_height","_field_name":"_height","_edit_field":"text","_gridx":"1","_gridy":"2"}
				,
				{"_label_name":"_style","_field_name":"_style","_edit_field":"text","_gridx":"0","_gridy":"2"}
				,
				{"_label_name":"_class","_field_name":"_class","_edit_field":"text","_gridx":"1","_gridy":"2"}
				,
				{"_label_name":"_float","_field_name":"_float","_edit_field":"text","_gridx":"2","_gridy":"2"}
			];
		$layout_prototype_editor_node.appendChild(createForm($form_name));
		
		// Buttons.
		var $layout_prototype_editor_buttons=document.createElement("div");
		// Remove button.
		var $layout_prototype_editor_remove_button=document.createElement("div");
		$layout_prototype_editor_remove_button.innerHTML=getResourceName("remove_layout");;
		Pennyutils.events.addEventHandler($layout_prototype_editor_remove_button, "click", removeLayoutItem);
		Pennyutils.setClassName($layout_prototype_editor_remove_button, "l tc button");
		$layout_prototype_editor_buttons.appendChild($layout_prototype_editor_remove_button);
		
		// Append button.
		var $layout_prototype_editor_append_button=document.createElement("div");
		$layout_prototype_editor_append_button.innerHTML=getResourceName("append_layout");;
		Pennyutils.events.addEventHandler($layout_prototype_editor_append_button, "click", appendLayoutItem);
		Pennyutils.setClassName($layout_prototype_editor_append_button, "l tc button");
		$layout_prototype_editor_buttons.appendChild($layout_prototype_editor_append_button);
		
		// Split button.
		var $layout_prototype_editor_split_button=document.createElement("div");
		$layout_prototype_editor_split_button.innerHTML=getResourceName("split_layout");;
		Pennyutils.events.addEventHandler($layout_prototype_editor_split_button, "click", splitLayoutItem);
		Pennyutils.setClassName($layout_prototype_editor_split_button, "l tc button");
		$layout_prototype_editor_buttons.appendChild($layout_prototype_editor_split_button);
		
		// Apply button.
		var $layout_prototype_editor_apply_button=document.createElement("button");
		$layout_prototype_editor_apply_button.innerHTML=getResourceName("apply_layout");
		Pennyutils.setClassName($layout_prototype_editor_apply_button, "r tc button");
		Pennyutils.events.addEventHandler($layout_prototype_editor_apply_button, "click", applyLayoutItem);
		$layout_prototype_editor_buttons.appendChild($layout_prototype_editor_apply_button);
		// Clear float.
		$layout_prototype_editor_clear_button=document.createElement("div");
		Pennyutils.setClassName($layout_prototype_editor_clear_button, "c");
		$layout_prototype_editor_buttons.appendChild($layout_prototype_editor_clear_button);
		
		$layout_prototype_editor_node.appendChild($layout_prototype_editor_buttons);
		
		// Append to window.
		$LAYOUTS_WIN.childNodes[1].appendChild($layouts_pannel);
		Pennyutils.setCssStyle($LAYOUTS_WIN, "z-index:1000000;");
		
		// Append to body.
		document.body.appendChild($LAYOUTS_WIN);
	}
	
	$("__layouts_input__").value=$input_field_id;
	
	setLayoutsItems();
	
	showWindow($LAYOUTS_WIN);
}

function setLayoutsItems () {
	// Init dom.
	$("layouts").innerHTML="";
	
	// Init value.
	var $layouts_value=$($("__layouts_input__").value).value;
	var $layouts=null;
	if ($layouts_value) {
		try {
			$layouts=JSON.parse($layouts_value);
		} catch ($err) {
			
		}
	}
	if (null==$layouts || $layouts.length==0) {
		// Init default value.
		var $form_name="layout_prototype_form";
		var $layout={};
		for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
			$layout[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]=
					undefined==$FORM_CONFIG[$form_name][$field_index]["_default_value"]
					?
					""
					:
					$FORM_CONFIG[$form_name][$field_index]["_default_value"]
				;
		}
		$layout["_level"]="1";
		$layout["_subject"]="0";
		$layout["_class"]="b1";
		$layout["_style"]="width:300px;height:300px;";
		// .
		$layouts=[];
		$layouts[$layouts.length]=$layout;
		// Write back value to input.
		$($("__layouts_input__").value).value=JSON.stringify($layouts);
		//
		modifyLayoutItem("1");
	}
	
	$("layout_itmes").innerHTML="";
	var $layout_item_node;
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		$layout_item_node=document.createElement("div");
		$layout_item_node.innerHTML=$layouts[$layout_index]["_level"];
		Pennyutils.setClassName($layout_item_node, "l p5 m5 b1");
		Pennyutils.events.addEventHandler($layout_item_node, "click", new Function("modifyLayoutItem('"+$layouts[$layout_index]["_level"]+"');"));
		
		$("layout_itmes").appendChild($layout_item_node);
	}
	$layout_item_node=document.createElement("div");
	Pennyutils.setClassName($layout_item_node, "c");
	$("layout_itmes").appendChild($layout_item_node);
	
	initLayoutsItems($layouts, "0", $("layouts"));
}
function initLayoutsItems ($layouts, $level, $parent_node) {
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		var $layout_node;
		var $layout_inner_node;
		if ($level==$layouts[$layout_index]["_subject"]) {
			$layout_node=document.createElement("div");
			Pennyutils.setIdName($layout_node, "__layouts_node_"+$layouts[$layout_index]["_level"]+"__");
//			Pennyutils.setClassName($layout_node, "w100 p1 m1 tc b1 bgcf");
			Pennyutils.setClassName($layout_node, $layouts[$layout_index]["_class"]);
			Pennyutils.setCssStyle($layout_node, $layouts[$layout_index]["_style"]);
			Pennyutils.events.addEventHandler($layout_node, "click", new Function("Pennyutils.events.getEvent().stopPropagation();modifyLayoutItem('"+$layouts[$layout_index]["_level"]+"');"));
			$parent_node.appendChild($layout_node);
			
			$layout_inner_node=document.createElement("div");
			Pennyutils.setIdName($layout_inner_node, "__layouts_inner_node_"+$layouts[$layout_index]["_level"]+"__");
			Pennyutils.setClassName($layout_inner_node, "w100");
			$layout_node.appendChild($layout_inner_node);
			
			// Insert child.
			initLayoutsItems($layouts, $layouts[$layout_index]["_level"], $("__layouts_inner_node_"+$layouts[$layout_index]["_level"]+"__"));
			// initLayoutsItems($layouts, $layouts[$layout_index]["_level"], $("__layouts_node_"+$layouts[$layout_index]["_level"]+"__"));
			
			//
			var $child_nodes=$("__layouts_inner_node_"+$layouts[$layout_index]["_level"]+"__").childNodes;
			// var $child_nodes=$("__layouts_node_"+$layouts[$layout_index]["_level"]+"__").childNodes;
			var $child_nodes_length=$child_nodes.length;
			
			// 
			if (0==$child_nodes_length) {
				$layout_node.innerHTML=
					// $layouts[$layout_index]["_bound"]?$layouts[$layout_index]["_bound"]:
						// $layouts[$layout_index]["_action"]?$layouts[$layout_index]["_action"]:
							// $layouts[$layout_index]["_level"];
							$layouts[$layout_index]["_name"];
			}
			
			if ($layouts[$layout_index]["_float"] && $child_nodes_length) {
				// var $width=$("__layouts_inner_node_"+$layouts[$layout_index]["_level"]+"__").offsetWidth;
				// // var $width=$("__layouts_node_"+$layouts[$layout_index]["_level"]+"__").offsetWidth;
				// var $child_nodes_width=parseInt($width/$child_nodes_length-6);
// //				alert($width+"/"+$child_nodes_length+" : "+($width/$child_nodes_length)+" , "+$child_nodes_width);
				// for (var $chile_index=0; $chile_index<$child_nodes_length; $chile_index++) {
					// Pennyutils.setCssStyle($child_nodes[$chile_index], "width:"+$child_nodes_width+"px;");
				// }
				
				$layout_node=document.createElement("div");
				Pennyutils.setIdName($layout_node, "__layouts_inner_node_"+$layouts[$layout_index]["_level"]+"_c__");
				// Pennyutils.setIdName($layout_node, "__layouts_node_"+$layouts[$layout_index]["_level"]+"_c__");
				Pennyutils.setClassName($layout_node, "c");
				$("__layouts_inner_node_"+$layouts[$layout_index]["_level"]+"__").appendChild($layout_node);
				// $("__layouts_node_"+$layouts[$layout_index]["_level"]+"__").appendChild($layout_node);
			}
		}
	}
}
function appendLayoutItem () {
	// Init values.
	var $layouts_value=$($("__layouts_input__").value).value;
	var $layouts=null;
	if ($layouts_value) {
		try {
			$layouts=JSON.parse($layouts_value);
		} catch ($err) {
			
		}
	}
	if (null==$layouts) {
		$layouts=[];
	}
	
	// Last layout level.
	var $last_layout_level=0;
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		$last_layout_level=parseInt($layouts[$layout_index]["_level"])>$last_layout_level?parseInt($layouts[$layout_index]["_level"]):$last_layout_level;
	}
	$last_layout_level++;
	
	// Init default value.
	var $form_name="layout_prototype_form";
	var $values={};
	for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
		$values[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]=
				undefined==$FORM_CONFIG[$form_name][$field_index]["_default_value"]
				?
				""
				:
				$FORM_CONFIG[$form_name][$field_index]["_default_value"]
			;
	}
	$values["_level"]=""+$last_layout_level;
	$values["_subject"]=$($form_name+"."+"_level").value;
	$values["_name"]=""+$last_layout_level;
//	$values["_class"]=$($form_name+"."+"_float").value?"l":"";	// float.
	$values["_class"]=($($form_name+"."+"_float").value?"l ":"")+"b1 w100 tc lh200 p5 m5";
//	$values["_style"]="width:100px;height:30px;";
	// Append to array.
	$layouts[$layouts.length]=$values;
	// Write back value to input.
	$($("__layouts_input__").value).value=JSON.stringify($layouts);
	// Set layouts items.
	setLayoutsItems();
}
function splitLayoutItem () {
	// Init values.
	var $layouts_value=$($("__layouts_input__").value).value;
	var $layouts=null;
	if ($layouts_value) {
		try {
			$layouts=JSON.parse($layouts_value);
		} catch ($err) {
			
		}
	}
	if (null==$layouts) {
		$layouts=[];
	}
	
	// Last layout level.
	var $last_layout_level=0;
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		$last_layout_level=parseInt($layouts[$layout_index]["_level"])>$last_layout_level?parseInt($layouts[$layout_index]["_level"]):$last_layout_level;
	}
	$last_layout_level++;
	
	// Init default value.
	var $form_name="layout_prototype_form";
	var $level=$($form_name+"."+"_level").value;
	var $values;
	for (var $index=0; $index<2; $index++) {
		$values={};
		for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
			$values[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]=
					undefined==$FORM_CONFIG[$form_name][$field_index]["_default_value"]
					?
					""
					:
					$FORM_CONFIG[$form_name][$field_index]["_default_value"]
				;
		}
		$values["_level"]=""+$last_layout_level++;
		$values["_subject"]=$level;
		$values["_name"]=""+$last_layout_level;
		$values["_class"]="l b1 lh200 tc p5 m5";
		$values["_style"]="width:100px;";
		// Append to array.
		$layouts[$layouts.length]=$values;
	}
	// Set clear status.
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		if ($layouts[$layout_index]["_level"]==$level) {	// Catch.
			$layouts[$layout_index]["_float"]="true";
			break;
		}
	}
	// Write back value to input.
	$($("__layouts_input__").value).value=JSON.stringify($layouts);
	// Set layouts items.
	setLayoutsItems();
}
function removeLayoutItem () {
	// Init values.
	var $layouts_value=$($("__layouts_input__").value).value;
	var $layouts=null;
	if ($layouts_value) {
		try {
			$layouts=JSON.parse($layouts_value);
		} catch ($err) {
			
		}
	}
	if (null==$layouts) {
		$layouts=[];
	}
	// Remove item.
	var $new_layouts=[];
	var $form_name="layout_prototype_form";
	var $level=$($form_name+"."+"_level").value;
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		if ($layouts[$layout_index]["_level"]==$level || $layouts[$layout_index]["_subject"]==$level ) {	// Skip.
			continue;
		}
		$new_layouts[$new_layouts.length]=$layouts[$layout_index];
	}
	// Write back value to input.
	$($("__layouts_input__").value).value=JSON.stringify($new_layouts);
	// Set layouts items.
	setLayoutsItems();
}
function modifyLayoutItem ($level) {
	// Init values.
	var $layouts_value=$($("__layouts_input__").value).value;
	var $layouts=null;
	if ($layouts_value) {
		try {
			$layouts=JSON.parse($layouts_value);
		} catch ($err) {
			
		}
	}
	if (null==$layouts) {
		$layouts=[];
	}
	
	var $layout=null;
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		if ($layouts[$layout_index]["_level"]==$level) {	// Catch.
			$layout=$layouts[$layout_index];
		}
		Pennyutils.replaceCssStyle($("__layouts_node_"+$layouts[$layout_index]["_level"]+"__"), "background", "background:#fff;");
		Pennyutils.replaceCssStyle($("__layouts_node_"+$layouts[$layout_index]["_level"]+"__"), "border-color", "border-color:#eee;");
	}
	if (null!=$layout) {
		Pennyutils.replaceCssStyle($("__layouts_node_"+$layout["_level"]+"__"), "background", "background:#eee;");
		Pennyutils.replaceCssStyle($("__layouts_node_"+$layout["_level"]+"__"), "border-color", "border-color:#ff0;");
	}
	
	
	// Set input value.
	var $form_name="layout_prototype_form";
	for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
		setInputValue(
				$form_name+"."+$FORM_CONFIG[$form_name][$field_index]["_field_name"]
				,
				null==$level
				?
				undefined==$FORM_CONFIG[$form_name][$field_index]["_default_value"]?"":$FORM_CONFIG[$form_name][$field_index]["_default_value"]
				:
				undefined==$layout[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]?"":$layout[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]
			);
	}
}
function applyLayoutItem () {
	// Init values.
	var $layouts_value=$($("__layouts_input__").value).value;
	var $layouts=null;
	if ($layouts_value) {
		try {
			$layouts=JSON.parse($layouts_value);
		} catch ($err) {
			
		}
	}
	if (null==$layouts) {
		$layouts=[];
	}
	// Update value.
	var $form_name="layout_prototype_form";
	var $level=$($form_name+"."+"_level").value;
	for (var $layout_index=0; $layout_index<$layouts.length; $layout_index++) {
		if ($layouts[$layout_index]["_level"]==$level) {
			for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
				$layouts[$layout_index][$FORM_CONFIG[$form_name][$field_index]["_field_name"]]=$($form_name+"."+$FORM_CONFIG[$form_name][$field_index]["_field_name"]).value;
			}
			break;
		}
	}
	// Write back value to input.
	$($("__layouts_input__").value).value=JSON.stringify($layouts);
	// Set layouts items.
	setLayoutsItems();
}
