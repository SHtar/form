/**
 * PhotosInputField
 */
// function getPhotoInputField ($_form_name, $setting) {
	// return getPhotoInputFields($_form_name, $setting, false);
// }
// GET_EDITOR_INPUT["photo_text"]=getPhotoInputField;
// function getPhotosInputField ($_form_name, $setting) {
	// return getPhotoInputFields($_form_name, $setting, true);
// }
// GET_EDITOR_INPUT["photos_text"]=getPhotosInputField;
function getPhotosInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	Pennyutils.events.addEventHandler($input_node_row, "mouseover", new Function("$PHOTO_ITEMS='__photo_items_"+$input_field_id+"__';"));
	Pennyutils.events.addEventHandler($input_node_row, "mouseout", new Function("$PHOTO_ITEMS=null;hidePhotosItems('__photo_items_"+$input_field_id+"__', '"+$input_field_id+".photo_items');"));
	Pennyutils.events.addEventHandler($input_node_row, "blur", new Function("$PHOTO_ITEMS=null;hidePhotosItems('__photo_items_"+$input_field_id+"__', '"+$input_field_id+".photo_items');"));
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $input_field_id);
	Pennyutils.setClassName($input_node_row_input, "l input iw"+($_input_width-1)+"");
	$input_node_row_input.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	Pennyutils.events.addEventHandler($input_node_row_input, "mouseover", new Function("showPhotosItems('"+$input_field_id+"');"));
	$input_node_row.appendChild($input_node_row_input);
	
	// var $input_node_row_sub=document.createElement("div");
	// Pennyutils.setClassName($input_node_row_sub,"r icon_sub");
	// Pennyutils.events.addEventHandler($input_node_row_sub, "click", new Function("showUploadPhotoFileForm('/file/img', '"+(undefined==$setting["_upload_file_name"]?"":$setting["_upload_file_name"])+"', '"+$input_field_id+"', '"+($is_multi?"true":"false")+"')"));
	// $input_node_row_sub.innerHTML="&nbsp";
	// $input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"c");
	$input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_select_div=document.createElement("div");
	Pennyutils.setIdName($input_node_row_select_div, $input_field_id+".photo_items");
	Pennyutils.setClassName($input_node_row_select_div, "pr tl");
	Pennyutils.setCssStyle($input_node_row_select_div, "height:0px;");
	$input_node_row.appendChild($input_node_row_select_div);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["photos"]=getPhotosInputField;
var $PHOTO_ITEMS_INPUT_FIELD_ID=null;
function showPhotosItems ($input_field_id) {
	$PHOTO_ITEMS_INPUT_FIELD_ID=$input_field_id;
	
	$($input_field_id+".photo_items").innerHTML="";
	
	var $photo_items_widget=document.createElement("div");
	Pennyutils.setClassName($photo_items_widget, "photo_items pa");
	
	var $photo_div_node;
	var $photo_node;
	var $photo_del_node;	
	try {
		var $photo_values=JSON.parse($($input_field_id).value);
		
		for (var $index=0; $index<$photo_values.length; $index++) {
			if ($photo_values[$index]["image"].trim()) {
				$photo_div_node=document.createElement("div");
				$photo_div_node.title=$photo_values[$index]["title"].trim();
				$photo_div_node.alt=$photo_values[$index]["title"].trim();
				Pennyutils.setClassName($photo_div_node, "l photo_items_div");
				
				$photo_node=document.createElement("img");
				$photo_node.src=$photo_values[$index]["image"].trim();
				Pennyutils.setClassName($photo_node, "photo_items_img cp");
				Pennyutils.events.addEventHandler($photo_node, "click", new Function("modifyPhotosEditorFormWinRowProcess("+$index+");"));
				$photo_div_node.appendChild($photo_node);
				
				$photo_del_node=document.createElement("div");
				$photo_del_node.innerHTML=getResourceName("delete");
				Pennyutils.events.addEventHandler($photo_del_node, "click", new Function("removePhotosItems("+$index+");"));
				$photo_div_node.appendChild($photo_del_node);
				
				$photo_items_widget.appendChild($photo_div_node);
				
				$is_show=true;
			}
		}
	} catch ($err) {
		
	}
	
	$photo_div_node=document.createElement("div");
	Pennyutils.setClassName($photo_div_node, "l photo_items_div");
	
	$photo_node=document.createElement("div");
	Pennyutils.setClassName($photo_node, "photo_items_append b1d tc cp");
	$photo_node.innerHTML="+";
	Pennyutils.events.addEventHandler($photo_node, "click", new Function("appendPhotosEditorFormWinRow('"+$input_field_id+"', "+$index+");"));
	$photo_div_node.appendChild($photo_node);
	
	$photo_items_widget.appendChild($photo_div_node);
	
	$photo_div_node=document.createElement("div");
	Pennyutils.setClassName($photo_div_node, "c");
	$photo_items_widget.appendChild($photo_div_node);
	
	$($input_field_id+".photo_items").appendChild($photo_items_widget);
	Pennyutils.showObject($($input_field_id+".photo_items"));
}
function modifyPhotosEditorFormWinRowProcess ($index) {
	createPhotosEditorFormWin();
	
	var $src_value=$($PHOTO_ITEMS_INPUT_FIELD_ID).value;
	var $photo_values;
	if ($src_value) {
		try {
			$photo_values=JSON.parse($src_value);
		} catch ($err) {
			$photo_values=[];
		}
	} else {
		$photo_values=[];
	}
	for (var $key in $photo_values[$index]) {
		if ($("__photo_editor_form__."+$key)) {
			$("__photo_editor_form__."+$key).value=$photo_values[$index][$key];
		}
	}
	if ($("__photo_editor_form__."+"index")) {
		$("__photo_editor_form__."+"index").value=$index;
	}
	if ($("__photo_editor_form__."+"url") && $photo_values[$index]["url"]) {
		$("__photo_editor_form__."+"url").value=JSON.stringify($photo_values[$index]["url"]);
	}
}
function appendPhotosEditorFormWinRow () {
	createPhotosEditorFormWin();
}
function createPhotosEditorFormWin () {
	$FORM_CONFIG["__photo_editor_form__"]=
	[
	{"_label_name":"index","_field_name":"index","_edit_field":"hidden_text","_gridx":0,"_gridy":0}
	,
	{"_label_name":"title","_field_name":"title","_edit_field":"text","_gridx":0,"_gridy":1}
	,
	{"_label_name":"description","_field_name":"description","_edit_field":"text","_gridx":0,"_gridy":2}
	,
	{"_label_name":"image","_field_name":"image","_edit_field":"image","_gridx":0,"_gridy":3}
	,
	{"_label_name":"url","_field_name":"url","_edit_field":"url","_gridx":0,"_gridy":4}
	];
	
	createWinForm("__photo_editor_form__", null, submitPhotosEditorForm);
}
function submitPhotosEditorForm () {
	var $src_value=$($PHOTO_ITEMS_INPUT_FIELD_ID).value;
	var $photo_rows;
	if ($src_value) {
		try {
			$photo_rows=JSON.parse($src_value);
		} catch ($err) {
			$photo_rows=[];
		}
	} else {
		$photo_rows=[];
	}
	var $index=$("__photo_editor_form__.index").value;
	if ($index.length==0) {	// Create or Update.
		$index=$photo_rows.length;
		$("__photo_editor_form__.index").value=$index;
	}
	$photo_rows[$index]={};
	for (var $col_index=0; $col_index<$FORM_CONFIG["__photo_editor_form__"].length; $col_index++) {
		if ($("__photo_editor_form__."+$FORM_CONFIG["__photo_editor_form__"][$col_index]["_field_name"])) {
			$photo_rows[$index][$FORM_CONFIG["__photo_editor_form__"][$col_index]["_field_name"]]=$("__photo_editor_form__."+$FORM_CONFIG["__photo_editor_form__"][$col_index]["_field_name"]).value;
		}
	}
	$photo_rows[$index]["url"]=JSON.parse($photo_rows[$index]["url"]);
	$($PHOTO_ITEMS_INPUT_FIELD_ID).value=JSON.stringify($photo_rows);
	
	showPhotosItems($PHOTO_ITEMS_INPUT_FIELD_ID);
	
	Pennyutils.hideObject($FORM_WIN["__photo_editor_form__"]);
}

function removePhotosItems ($remove_index) {
	var $photos_new_values=[];
	try {
		var $photos_values=JSON.parse($($PHOTO_ITEMS_INPUT_FIELD_ID).value);
		
		for (var $index=0; $index<$photos_values.length; $index++) {
			if ($remove_index!=$index) {
				$photos_new_values[$photos_new_values.length]=$photos_values[$index];
			}
		}
	} catch ($err) {
		
	}
	$($PHOTO_ITEMS_INPUT_FIELD_ID).value=$photos_new_values.length==0?"":JSON.stringify($photos_new_values);
	
	showPhotosItems($PHOTO_ITEMS_INPUT_FIELD_ID);
}
var $PHOTO_ITEMS=null;
function hidePhotosItems ($photo_items, $input_field_id) {
	$timeoutID=setTimeout("if ($PHOTO_ITEMS!='"+$photo_items+"') {$('"+$input_field_id+"').innerHTML='';Pennyutils.hideObject('"+$input_field_id+"');clearTimeout($timeoutID);}", 300);
}