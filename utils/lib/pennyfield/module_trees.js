/**
 * ModuleTreesInputField
 */
function getModuleTreesInputField ($_form_name, $setting) {
	var $_label_name=$setting["_label_name"];
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $module_trees_div=document.createElement("div");
	Pennyutils.setClassName($module_trees_div, "input_border");
	Pennyutils.events.addEventHandler($module_trees_div, "mouseover", new Function("$MODULE_TREES_ITEMS='__module_trees_"+$input_field_id+"__';"));
	Pennyutils.events.addEventHandler($module_trees_div, "mouseout", new Function("$MODULE_TREES_ITEMS=null;hideModuleTreesItems('__module_trees_"+$input_field_id+"__', '__module_trees_"+$setting["_module"]+"__')"));
	Pennyutils.events.addEventHandler($module_trees_div, "blur", new Function("$MODULE_TREES_ITEMS=null;hideModuleTreesItems('__module_trees_"+$input_field_id+"__', '__module_trees_"+$setting["_module"]+"__')"));
	
	var $module_trees_input_node=document.createElement("input");
	Pennyutils.setIdName($module_trees_input_node, $input_field_id);
	Pennyutils.setClassName($module_trees_input_node, "dn");
	$module_trees_input_node.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$module_trees_input_node.type="text";
	}
	$module_trees_div.appendChild($module_trees_input_node);
	
	var $module_trees_module_node=document.createElement("input");
	Pennyutils.setIdName($module_trees_module_node, $input_field_id+".module_trees_module");
	Pennyutils.setClassName($module_trees_module_node, "dn");
	$module_trees_module_node.value=$setting["_module"];
	$module_trees_div.appendChild($module_trees_module_node);
	
	var $module_trees_key_node=document.createElement("input");
	Pennyutils.setIdName($module_trees_key_node, $input_field_id+".module_trees_key");
	Pennyutils.setClassName($module_trees_key_node, "dn");
	$module_trees_key_node.value=$setting["_key"];
	$module_trees_div.appendChild($module_trees_key_node);
	
	var $module_trees_value_node=document.createElement("input");
	Pennyutils.setIdName($module_trees_value_node, $input_field_id+".module_trees_value");
	Pennyutils.setClassName($module_trees_value_node, "dn");
	$module_trees_value_node.value=$setting["_value"];
	$module_trees_div.appendChild($module_trees_value_node);
	
	var $module_trees_parent_node=document.createElement("input");
	Pennyutils.setIdName($module_trees_parent_node, $input_field_id+".module_trees_parent");
	Pennyutils.setClassName($module_trees_parent_node, "dn");
	$module_trees_parent_node.value=$setting["_parent"];
	$module_trees_div.appendChild($module_trees_parent_node);
	
	var $module_trees_items_node=document.createElement("div");
	Pennyutils.setIdName($module_trees_items_node, $input_field_id+".module_trees_items");
	Pennyutils.setClassName($module_trees_items_node, "l tl o input iw"+($_input_width-1)+"");
	$module_trees_items_node.readOnly=true;
	if (document.uniqueID) {
	} else {
		$module_trees_items_node.type="text";
	}
	$module_trees_div.appendChild($module_trees_items_node);
	
	var $module_trees_down_node=document.createElement("div");
	$module_trees_down_node.innerHTML="&nbsp;";
	Pennyutils.events.addEventHandler($module_trees_down_node, "click", new Function("getModuleTreesItems('"+$input_field_id+"')"));
	Pennyutils.setClassName($module_trees_down_node, "r icon_down input_icon");
	$module_trees_div.appendChild($module_trees_down_node);
	
	var $module_trees_clear_node=document.createElement("div");
	Pennyutils.setClassName($module_trees_clear_node, "c");
	$module_trees_div.appendChild($module_trees_clear_node);
	
	var $module_trees_selector_div=document.createElement("div");
	Pennyutils.setIdName($module_trees_selector_div, $input_field_id+".module_trees_selector");
	Pennyutils.setClassName($module_trees_selector_div, "pr bgce tl");
	Pennyutils.setCssStyle($module_trees_selector_div, "height:0px;");
	$module_trees_div.appendChild($module_trees_selector_div);
	
	return $module_trees_div;
}
GET_EDITOR_INPUT["module_trees"]=getModuleTreesInputField;
function getModuleTreesItems ($input_field_id) {
	var $_module=$($input_field_id+".module_trees_module").value;
	if ($("__module_trees_"+$_module+"__")) {	// Secondary.
		$($input_field_id+".module_trees_selector").appendChild($("__module_trees_"+$_module+"__"));
		chooseModuleTreesItemsItem($input_field_id, "");
		Pennyutils.showObject("__module_trees_"+$_module+"__");
	} else {	// First time.
		var $callback=function(){
				var $module_trees_widget=document.createElement("div");
				Pennyutils.setIdName($module_trees_widget, "__module_trees_"+$_module+"__");
				Pennyutils.setClassName($module_trees_widget, "module_trees_selector pa");
				
				var $module_trees_parent_keys_node=document.createElement("div");
				Pennyutils.setIdName($module_trees_parent_keys_node, "__module_trees_parent_keys_"+$_module+"__");
				$module_trees_widget.appendChild($module_trees_parent_keys_node);
				
				var $module_trees_parent_keys_input_node=document.createElement("input");
				Pennyutils.setIdName($module_trees_parent_keys_input_node, "__module_trees_parent_keys_input_"+$_module+"__");
				Pennyutils.setClassName($module_trees_parent_keys_input_node, "dn");
				$module_trees_widget.appendChild($module_trees_parent_keys_input_node);
				
				var $module_trees_items_node=document.createElement("div");
				Pennyutils.setIdName($module_trees_items_node, "__module_trees_items_"+$_module+"__");
				$module_trees_widget.appendChild($module_trees_items_node);
				
				document.body.appendChild($module_trees_widget);
				
				$($input_field_id+".module_trees_selector").appendChild($module_trees_widget);
				chooseModuleTreesItemsItem($input_field_id, "");
				Pennyutils.showObject("__module_trees_"+$_module+"__");
			};
		if (undefined==$MODULE_TREE_DATA[$_module]) {
			var $_key=$($input_field_id+".module_trees_key").value;
			var $_value=$($input_field_id+".module_trees_value").value;
			var $_parent=$($input_field_id+".module_trees_parent").value;
			initModuleTreeData($_module, $_key, $_value, $_parent, $callback);
		} else {
			$callback.call();
		}
	}
}
function chooseModuleTreesItemsItem ($input_field_id, $parent_node_key) {
	var $_module=$($input_field_id+".module_trees_module").value;
	
	// Module tree parent input.
	var $module_trees_parent_keys=$("__module_trees_parent_keys_input_"+$_module+"__").value;
	$module_trees_parent_keys=$module_trees_parent_keys.split(",");
	var $is_catch=false;
	for (var $module_trees_parent_keys_index=0; $module_trees_parent_keys_index<$module_trees_parent_keys.length; $module_trees_parent_keys_index++) {
		if ($module_trees_parent_keys[$module_trees_parent_keys_index]==$parent_node_key) {
			$is_catch=true;
		}
	}
	if (!$is_catch) {
		$module_trees_parent_keys[$module_trees_parent_keys.length]=$parent_node_key;
	}
	$("__module_trees_parent_keys_input_"+$_module+"__").value=$module_trees_parent_keys.join(",");
	
	var $row_node;
	// Module tree parent keys node.
	var $module_trees_parent_keys_node=$("__module_trees_parent_keys_"+$_module+"__");
	// Clear.
	$module_trees_parent_keys_node.innerHTML="";
	// Build.
	for (var $module_trees_parent_keys_index=0; $module_trees_parent_keys_index<$module_trees_parent_keys.length; $module_trees_parent_keys_index++) {
		$row_node=document.createElement("span");
		$row_node.innerHTML=
				undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$module_trees_parent_keys[$module_trees_parent_keys_index]]
				?
				$module_trees_parent_keys[$module_trees_parent_keys_index]
				:
				$MODULE_TREE_DATA[$_module+"_VALUE"][$module_trees_parent_keys[$module_trees_parent_keys_index]]
			;
		Pennyutils.setClassName($row_node, "p5 m5");
		Pennyutils.events.addEventHandler($row_node, "click", new Function("chooseModuleTreesItemsItem('"+$input_field_id+"', '"+$module_trees_parent_keys[$module_trees_parent_keys_index]+"');"));
		
		$module_trees_parent_keys_node.appendChild($row_node);
	}
	
	// Module tree items.
	var $module_trees_items_node=$("__module_trees_items_"+$_module+"__");
	// Clear.
	$module_trees_items_node.innerHTML="";
	// Build.
	var $tree_node_data=$MODULE_TREE_DATA[$_module];
	var $parent_node_key;
	for (var $index=0; $index<$tree_node_data[$parent_node_key].length; $index++) {
		$row_node=document.createElement("div");
		Pennyutils.setClassName($row_node, "l p5 m5 b1d tc");
		Pennyutils.setCssStyle($row_node, "width:40%;");
		$row_node.innerHTML=$tree_node_data[$parent_node_key][$index]["value"];
		
		if (undefined==$MODULE_TREE_DATA[$_module][$tree_node_data[$parent_node_key][$index]["key"]]) {
			Pennyutils.events.addEventHandler($row_node, "dblclick", new Function("setModuleTreesItemsItem('"+$input_field_id+"', '"+$tree_node_data[$parent_node_key][$index]["key"]+"');"));
		} else {
			Pennyutils.events.addEventHandler($row_node, "click", new Function("chooseModuleTreesItemsItem('"+$input_field_id+"', '"+$tree_node_data[$parent_node_key][$index]["key"]+"');"));
		}
		
		$module_trees_items_node.appendChild($row_node);
	}
	$row_node=document.createElement("div");
	Pennyutils.setClassName($row_node, "c");
	$module_trees_items_node.appendChild($row_node);
}
function setModuleTreesItemsItem ($input_field_id, $key) {
	var $keys=$($input_field_id).value;
	$keys=$keys?$keys.split(","):[];
	var $is_catch=false;
	for (var $index=0; $index<$keys.length; $index++) {
		if ($keys[$index]==$key) {
			$is_catch=true;
			break;
		}
	}
	if (!$is_catch) {
		$keys[$keys.length]=$key;
	}
	
	setModuleTreesValues($input_field_id, $keys.join(","));
}
function setModuleTreesValues ($input_field_id, $keys) {
	$($input_field_id).value=$keys;
	
	// init value.
	var $_module=$($input_field_id+".module_trees_module").value;
	var $callback = function () {
		var $tree_value_data=$MODULE_TREE_DATA[$_module+"_VALUE"];
		
		// Clear.
		$($input_field_id+".module_trees_items").innerHTML="";
		// Build.
		var $keys=$($input_field_id).value;
		$keys=$keys?$keys.split(","):[];
		var $row_node;
		var $remove_node;
		for (var $index=0; $index<$keys.length; $index++) {
			$row_node=document.createElement("span");
			Pennyutils.setClassName($row_node, "p1 m1");
			
			var $key=$keys[$index];
			
			// set last node value.
			var $node_names=[undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$key]?$key:$MODULE_TREE_DATA[$_module+"_VALUE"][$key]];
			// set before node value.
			while (undefined!=($key=$MODULE_TREE_DATA[$_module+"_PARENT"][$key])) {
				$node_names[$node_names.length]=undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$key]?$key:$MODULE_TREE_DATA[$_module+"_VALUE"][$key];
			}
			
			// set tree node.
			var $node_names_text=[];
			for (var $node_names_index=$node_names.length-1; $node_names_index>=0; $node_names_index--) {
				// $row_node=document.createElement("span");
				// Pennyutils.setClassName($row_node, "p1 m1");
				// $row_node.appendChild(document.createTextNode($node_names[$index]));
				// $($input_field_id+".module_tree_items").appendChild($row_node);
				$node_names_text[$node_names_text.length]=$node_names[$node_names_index];
			}
			$row_node.title=$node_names_text.join(" ");
			
			// Text
			$row_node.appendChild(document.createTextNode(undefined==$tree_value_data[$keys[$index]]?$keys[$index]:$tree_value_data[$keys[$index]]));
			
			// Remove
			$remove_node=document.createElement("span");
			$remove_node.innerHTML="x";
			Pennyutils.setClassName($remove_node, "p1 m1 cp");
			Pennyutils.events.addEventHandler($remove_node, "click", new Function("removeModuleTreesItemsItem('"+$input_field_id+"', '"+$keys[$index]+"');"));
			$row_node.appendChild($remove_node);
			
			$($input_field_id+".module_trees_items").appendChild($row_node);
		}
	};
	
	if (undefined==$MODULE_TREE_DATA[$_module]) {
		var $_key=$($input_field_id+".module_trees_key").value;
		var $_value=$($input_field_id+".module_trees_value").value;
		var $_parent=$($input_field_id+".module_trees_parent").value;
		initModuleTreeData($_module, $_key, $_value, $_parent, $callback);
	} else {
		$callback.call();
	}
}
function setModuleTreesItemsItem ($input_field_id, $key) {
	var $keys=$($input_field_id).value;
	$keys=$keys?$keys.split(","):[];
	var $is_catch=false;
	for (var $index=0; $index<$keys.length; $index++) {
		if ($keys[$index]==$key) {
			$is_catch=true;
		}
	}
	if (!$is_catch) {
		$keys[$keys.length]=$key;
	}
	
	// Hide selector.
	var $_module=$($input_field_id+".module_trees_module").value;
	Pennyutils.hideObject("__module_trees_"+$_module+"__");
	
	// Set value.
	setModuleTreesValues($input_field_id, $keys.join(","));
}
function removeModuleTreesItemsItem ($input_field_id, $key) {
	var $keys=$($input_field_id).value;
	$keys=$keys?$keys.split(","):[];
	var $new_keys=[];
	var $is_catch=false;
	for (var $index=0; $index<$keys.length; $index++) {
		if ($keys[$index]!=$key) {
			$new_keys[$new_keys.length]=$keys[$index];
		}
	}
	
	setModuleTreesValues($input_field_id, $new_keys.join(","));
}
var $MODULE_TREES_ITEMS=null;
function overModuleTreesItemsItem () {
	var $event=Pennyutils.events.getEvent();
	Pennyutils.replaceCssStyle($event.target, "background", "background:#fefefe;");
}
function outModuleTreesItemsItem () {
	var $event=Pennyutils.events.getEvent();
	Pennyutils.replaceCssStyle($event.target, "background", "");
}
function hideModuleTreesItems () {
	$timeoutID=setTimeout("if ($MODULE_TREES_ITEMS!='"+arguments[0]+"') {Pennyutils.hideObject('"+arguments[1]+"');clearTimeout($timeoutID);}", 300);
}
function showModuleTreesItems () {
	Pennyutils.showObject(arguments[0]);
//	Pennyutils.relateSite(arguments[0]+"_list", arguments[0]+"_field", 'l', 'b');
}