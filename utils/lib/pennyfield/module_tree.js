/**
 * ModuleTreeInputField
 */
function getModuleTreeInputField ($_form_name, $setting) {
	var $_label_name=$setting["_label_name"];
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	
	var $module_tree_div=document.createElement("div");
	Pennyutils.events.addEventHandler($module_tree_div, "mouseover", new Function("$MODULE_TREE_ITEMS='__module_tree_items_"+$input_field_id+"__';"));
	Pennyutils.events.addEventHandler($module_tree_div, "mouseout", new Function("$MODULE_TREE_ITEMS=null;hideModuleTreeItems('__module_tree_items_"+$input_field_id+"__', '__module_tree_"+$setting["_module"]+"__')"));
	Pennyutils.events.addEventHandler($module_tree_div, "blur", new Function("$MODULE_TREE_ITEMS=null;hideModuleTreeItems('__module_tree_items_"+$input_field_id+"__', '__module_tree_"+$setting["_module"]+"__')"));
	
	var $module_tree_input_node=document.createElement("input");
	Pennyutils.setIdName($module_tree_input_node, $input_field_id);
	Pennyutils.setClassName($module_tree_input_node, "dn");
	$module_tree_input_node.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$module_tree_input_node.type="text";
	}
	$module_tree_div.appendChild($module_tree_input_node);
	
	var $module_tree_module_node=document.createElement("input");
	Pennyutils.setIdName($module_tree_module_node, $input_field_id+".module_tree_module");
	Pennyutils.setClassName($module_tree_module_node, "dn");
	$module_tree_module_node.value=$setting["_module"];
	$module_tree_div.appendChild($module_tree_module_node);
	
	var $module_tree_key_node=document.createElement("input");
	Pennyutils.setIdName($module_tree_key_node, $input_field_id+".module_tree_key");
	Pennyutils.setClassName($module_tree_key_node, "dn");
	$module_tree_key_node.value=$setting["_key"];
	$module_tree_div.appendChild($module_tree_key_node);
	
	var $module_tree_value_node=document.createElement("input");
	Pennyutils.setIdName($module_tree_value_node, $input_field_id+".module_tree_value");
	Pennyutils.setClassName($module_tree_value_node, "dn");
	$module_tree_value_node.value=$setting["_value"];
	$module_tree_div.appendChild($module_tree_value_node);
	
	var $module_tree_parent_node=document.createElement("input");
	Pennyutils.setIdName($module_tree_parent_node, $input_field_id+".module_tree_parent");
	Pennyutils.setClassName($module_tree_parent_node, "dn");
	$module_tree_parent_node.value=$setting["_parent"];
	$module_tree_div.appendChild($module_tree_parent_node);
	
	var $module_tree_choose_node=document.createElement("input");
	Pennyutils.setIdName($module_tree_choose_node, $input_field_id+".module_tree_choose");
	Pennyutils.setClassName($module_tree_choose_node, "dn");
	$module_tree_choose_node.value=$setting["_choose"];
	$module_tree_div.appendChild($module_tree_choose_node);
	
	var $module_tree_items_node=document.createElement("div");
	Pennyutils.setIdName($module_tree_items_node, $input_field_id+".module_tree_items");
	Pennyutils.setClassName($module_tree_items_node, "l tl o input iw"+($_input_width-1)+"");
	$module_tree_items_node.readOnly=true;
	if (document.uniqueID) {
	} else {
		$module_tree_items_node.type="text";
	}
	$module_tree_div.appendChild($module_tree_items_node);
	
	var $module_tree_down_node=document.createElement("div");
	$module_tree_down_node.innerHTML="&nbsp;";
	Pennyutils.events.addEventHandler($module_tree_down_node, "click", new Function("getModuleTreeItems('"+$input_field_id+"')"));
	Pennyutils.setClassName($module_tree_down_node, "r icon_down input_icon");
	$module_tree_div.appendChild($module_tree_down_node);
	
	var $module_tree_clear_node=document.createElement("div");
	Pennyutils.setClassName($module_tree_clear_node, "c");
	$module_tree_div.appendChild($module_tree_clear_node);
	
	var $module_tree_selector_div=document.createElement("div");
	Pennyutils.setIdName($module_tree_selector_div, $input_field_id+".module_tree_selector_div");
	Pennyutils.setClassName($module_tree_selector_div, "pr bgce tl");
	Pennyutils.setCssStyle($module_tree_selector_div, "height:0px;");
	$module_tree_div.appendChild($module_tree_selector_div);
	
	$input_node_row.appendChild($module_tree_div);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["module_tree"]=getModuleTreeInputField;
function getModuleTreeItems ($input_field_id) {
	var $_module=$($input_field_id+".module_tree_module").value;
	if ($("__module_tree_"+$_module+"__")) {	// Secondary.
		$($input_field_id+".module_tree_selector_div").appendChild($("__module_tree_"+$_module+"__"));
		chooseModuleTreeItemsItem($input_field_id, "");
		Pennyutils.showObject("__module_tree_"+$_module+"__");
	} else {	// First time.
		var $callback=function(){
				var $module_trees_widget=document.createElement("div");
				Pennyutils.setIdName($module_trees_widget, "__module_tree_"+$_module+"__");
				Pennyutils.setClassName($module_trees_widget, "module_tree_selector pa");
				
				var $module_tree_parent_keys_node=document.createElement("div");
				Pennyutils.setIdName($module_tree_parent_keys_node, "__module_tree_parent_keys_"+$_module+"__");
				$module_trees_widget.appendChild($module_tree_parent_keys_node);
				
				var $module_tree_parent_keys_input_node=document.createElement("input");
				Pennyutils.setIdName($module_tree_parent_keys_input_node, "__module_tree_parent_keys_input_"+$_module+"__");
				Pennyutils.setClassName($module_tree_parent_keys_input_node, "dn");
				$module_trees_widget.appendChild($module_tree_parent_keys_input_node);
				
				var $module_tree_items_node=document.createElement("div");
				Pennyutils.setIdName($module_tree_items_node, "__module_tree_items_"+$_module+"__");
				$module_trees_widget.appendChild($module_tree_items_node);
				
				document.body.appendChild($module_trees_widget);
				
				$($input_field_id+".module_tree_selector_div").appendChild($("__module_tree_"+$_module+"__"));
				chooseModuleTreeItemsItem($input_field_id, "");
				Pennyutils.showObject("__module_tree_"+$_module+"__");
			};
		if (undefined==$MODULE_TREE_DATA[$_module]) {
			var $_key=$($input_field_id+".module_tree_key").value;
			var $_value=$($input_field_id+".module_tree_value").value;
			var $_parent=$($input_field_id+".module_tree_parent").value;
			initModuleTreeData($_module, $_key, $_value, $_parent, $callback);
		} else {
			$callback.call();
		}
	}
}
function chooseModuleTreeItemsItem ($input_field_id, $parent_node_key) {
	var $_module=$($input_field_id+".module_tree_module").value;
	
	// Module tree parent input.
	var $module_tree_parent_keys=$("__module_tree_parent_keys_input_"+$_module+"__").value;
	$module_tree_parent_keys=$module_tree_parent_keys.split(",");
	var $new_module_tree_parent_keys=[];
	var $is_catch=false;
	for (var $module_tree_parent_keys_index=0; $module_tree_parent_keys_index<$module_tree_parent_keys.length; $module_tree_parent_keys_index++) {
		if ($module_tree_parent_keys[$module_tree_parent_keys_index]==$parent_node_key) {
			$is_catch=true;
			for (; $module_tree_parent_keys_index<$module_tree_parent_keys.length; $module_tree_parent_keys_index++) {
				$module_tree_parent_keys.pop();
			}
			break;
		}
	}
	if (!$is_catch) {
		$module_tree_parent_keys[$module_tree_parent_keys.length]=$parent_node_key;
	}
	$("__module_tree_parent_keys_input_"+$_module+"__").value=$module_tree_parent_keys.join(",");
	
	var $row_node;
	// Module tree parent keys node.
	var $module_tree_parent_keys_node=$("__module_tree_parent_keys_"+$_module+"__");
	// Clear.
	$module_tree_parent_keys_node.innerHTML="";
	// Build.
	for (var $module_tree_parent_keys_index=0; $module_tree_parent_keys_index<$module_tree_parent_keys.length; $module_tree_parent_keys_index++) {
		$row_node=document.createElement("span");
		$row_node.innerHTML=
				undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$module_tree_parent_keys[$module_tree_parent_keys_index]]
				?
				$module_tree_parent_keys[$module_tree_parent_keys_index]
				:
				$MODULE_TREE_DATA[$_module+"_VALUE"][$module_tree_parent_keys[$module_tree_parent_keys_index]]
			;
		Pennyutils.setClassName($row_node, "p1 m1");
		Pennyutils.events.addEventHandler($row_node, "click", new Function("chooseModuleTreeItemsItem('"+$input_field_id+"', '"+$module_tree_parent_keys[$module_tree_parent_keys_index]+"');"));
		$module_tree_parent_keys_node.appendChild($row_node);
	}
	
	// Module tree items.
	var $module_tree_items_node=$("__module_tree_items_"+$_module+"__");
	// Clear.
	$module_tree_items_node.innerHTML="";
	// Build.
	var $tree_node_data=$MODULE_TREE_DATA[$_module];
	var $parent_node_key;
	var $row_label_node;
	for (var $index=0; $index<$tree_node_data[$parent_node_key].length; $index++) {
		$row_node=document.createElement("div");
		
		$row_label_node=document.createElement("span");
		Pennyutils.setClassName($row_label_node, "p5 m5 tr");
		Pennyutils.setClassName($row_node, "l p5 m5 b1d tc w40%");
//		Pennyutils.setCssStyle($row_node, "width:40%;");
		
		$row_label_node.appendChild(document.createTextNode($tree_node_data[$parent_node_key][$index]["value"]));
		if (undefined==$MODULE_TREE_DATA[$_module][$tree_node_data[$parent_node_key][$index]["key"]]) {
			Pennyutils.events.addEventHandler($row_label_node, "dblclick", new Function("setModuleTreeItemsItem('"+$input_field_id+"', '"+$tree_node_data[$parent_node_key][$index]["key"]+"');"));
		} else {
			Pennyutils.events.addEventHandler($row_label_node, "click", new Function("chooseModuleTreeItemsItem('"+$input_field_id+"', '"+$tree_node_data[$parent_node_key][$index]["key"]+"');"));
		}
		$row_node.appendChild($row_label_node);
		
		$row_label_node=document.createElement("span");
		$row_label_node.appendChild(document.createTextNode("Y"));
		Pennyutils.events.addEventHandler($row_label_node, "click", new Function("setModuleTreeItemsItem('"+$input_field_id+"', '"+$tree_node_data[$parent_node_key][$index]["key"]+"');"));
		Pennyutils.setClassName($row_label_node, "p5 m5");
		$row_node.appendChild($row_label_node);
		
		$module_tree_items_node.appendChild($row_node);
	}
	$row_node=document.createElement("div");
	Pennyutils.setClassName($row_node, "c");
	$module_tree_items_node.appendChild($row_node);
}
function setModuleTreeItemsItem ($input_field_id, $key) {
	// Hide selector.
	var $_module=$($input_field_id+".module_tree_module").value;
	Pennyutils.hideObject("__module_tree_"+$_module+"__");
	
	// Set value.
	setModuleTreeValues($input_field_id, $key);
	
	// Set relate.
	var $_module=$($input_field_id+".module_tree_module").value;
	var $_choose=$($input_field_id+".module_tree_choose").value;
	if ($_choose) {
		var $json_objcet=JSON.parse($_choose);
		
		var $_map=undefined==$json_objcet._map?"":$json_objcet._map;
		var $_sep=undefined==$json_objcet._sep?"":$json_objcet._sep;
		var $_pre=undefined==$json_objcet._pre?"":$json_objcet._pre;
		var $_app=undefined==$json_objcet._app?"":$json_objcet._app;
		
		var $_set=undefined==$json_objcet._set?"":$json_objcet._set;
		var $_dot=undefined==$json_objcet._dot?"":$json_objcet._dot;
		var $_add=undefined==$json_objcet._add?"":$json_objcet._add;
		var $_fro=undefined==$json_objcet._fro?"":$json_objcet._fro;
		
		var $_form_name=$input_field_id.split(".")[0];
		if ($_map) {
			$_map=$_map.split(",");
			
			var $_map_value=[];
			if ($_pre) {
				$_pre=$_pre.split(",");
				for (var $_pre_index=0; $_pre_index<$_pre.length; $_pre_index++) {
					$_map_value[$_map_value.length]=$($_form_name+"."+$_pre[$_pre_index]).value;
				}
			}
			$_map_value[$_map_value.length]=$key;
			if ($_app) {
				$_app=$_app.split(",");
				for (var $_app_index=0; $_app_index<$_app.length; $_app_index++) {
					$_map_value[$_map_value.length]=$($_form_name+"."+$_app[$_app_index]).value;
				}
			}
			$_map_value=$_map_value.join($_sep);
			for (var $_map_index=0; $_map_index<$_map.length; $_map_index++) {
				setInputValue($_form_name+"."+$_map[$_map_index], $_map_value);
			}
		}
		if ($_set) {
			// set last node value.
			var $node_names=[];
			$node_names[$node_names.length]=[undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$key]?$key:$MODULE_TREE_DATA[$_module+"_VALUE"][$key]];
			// set before node value.
			while (undefined!=($key=$MODULE_TREE_DATA[$_module+"_PARENT"][$key])) {
				$node_names[$node_names.length]=undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$key]?$key:$MODULE_TREE_DATA[$_module+"_VALUE"][$key];
			}
			
			var $_set_value=[];
			if ($_fro) {
				$_fro=$_fro.split(",");
				for (var $_fro_index=0; $_fro_index<$_fro.length; $_fro_index++) {
					$_set_value[$_set_value.length]=$($_form_name+"."+$_fro[$_fro_index]).value;
				}
			}
			$_set_value[$_set_value.length]=$node_names.reverse().join("");
			if ($_add) {
				$_add=$_add.split(",");
				for (var $_add_index=0; $_add_index<$_add.length; $_add_index++) {
					$_set_value[$_set_value.length]=$($_form_name+"."+$_add[$_add_index]).value;
				}
			}
			$_set_value=$_set_value.join($_dot);
			
			$_set=$_set.split(",");
			for (var $_set_index=0; $_set_index<$_set.length; $_set_index++) {
				setInputValue($_form_name+"."+$_set[$_set_index], $_set_value);
			}
		}
	}
}
function setModuleTreeValues ($input_field_id, $key) {
	// Set value.
	$($input_field_id).value=$key;
	
	// Init module tree value.
	var $_module=$($input_field_id+".module_tree_module").value;
	var $callback=function(){
		var $tree_value_data=$MODULE_TREE_DATA[$_module+"_VALUE"];
		
		// clear.
		$($input_field_id+".module_tree_items").innerHTML="";
		
		// set last node value.
		var $node_names=[undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$key]?$key:$MODULE_TREE_DATA[$_module+"_VALUE"][$key]];
		// set before node value.
		while (undefined!=($key=$MODULE_TREE_DATA[$_module+"_PARENT"][$key])) {
			$node_names[$node_names.length]=undefined==$MODULE_TREE_DATA[$_module+"_VALUE"][$key]?$key:$MODULE_TREE_DATA[$_module+"_VALUE"][$key];
		}
		
		// set tree node.
		var $row_node;
		for (var $index=$node_names.length-1; $index>=0; $index--) {
			$row_node=document.createElement("span");
			Pennyutils.setClassName($row_node, "p1 m1");
			$row_node.appendChild(document.createTextNode($node_names[$index]));
			$($input_field_id+".module_tree_items").appendChild($row_node);
		}
		
		$($input_field_id+".module_tree_items").title=$($input_field_id+".module_tree_items").innerText;
		
		hideModuleTreeItems();
	};
	
	if (undefined==$MODULE_TREE_DATA[$_module]) {
		var $_key=$($input_field_id+".module_tree_key").value;
		var $_value=$($input_field_id+".module_tree_value").value;
		var $_parent=$($input_field_id+".module_tree_parent").value;
		initModuleTreeData($_module, $_key, $_value, $_parent, $callback);
	} else {
		$callback.call();
	}
}
var $MODULE_TREE_ITEMS=null;
function overModuleTreeItemsItem () {
	var $event=Pennyutils.events.getEvent();
	Pennyutils.replaceCssStyle($event.target, "background", "background:#fefefe;");
}
function outModuleTreeItemsItem () {
	var $event=Pennyutils.events.getEvent();
	Pennyutils.replaceCssStyle($event.target, "background", "");
}
function hideModuleTreeItems () {
	$timeoutID=setTimeout("if ($MODULE_TREE_ITEMS!='"+arguments[0]+"') {Pennyutils.hideObject('"+arguments[1]+"');clearTimeout($timeoutID);}", 300);
}
function showModuleTreeItems () {
	Pennyutils.showObject(arguments[0]);
}

// Init module tree data.
var $MODULE_TREE_DATA={};
function initModuleTreeData ($_module, $_key, $_value, $_parent, $callback) {
	if (undefined==$MODULE_TREE_DATA[$_module]) {
		new net.ContentLoader("dao/get_module_tree.php", function(){
			var $json_obj=JSON.parse(this.request.responseText);
//			$MODULE_TREE_DATA[$_module]=$json_obj;
			var $key_value_data={};
			var $parent_data={};
			var $tree_node_data={"":[]};
			for (var $row_index=0; $row_index<$json_obj.length; $row_index++) {
				if (undefined==$tree_node_data[$json_obj[$row_index][$_parent]]) {
					$tree_node_data[$json_obj[$row_index][$_parent]]=[];
				}
				$tree_node_data[$json_obj[$row_index][$_parent]][$tree_node_data[$json_obj[$row_index][$_parent]].length]={
					"key":$json_obj[$row_index][$_key]
					, 
					"value":$json_obj[$row_index][$_value]
				}
				$key_value_data[$json_obj[$row_index][$_key]]=$json_obj[$row_index][$_value];
				$parent_data[$json_obj[$row_index][$_key]]=$json_obj[$row_index][$_parent];
			}
			$MODULE_TREE_DATA[$_module]=$tree_node_data;
			$MODULE_TREE_DATA[$_module+"_VALUE"]=$key_value_data;
			$MODULE_TREE_DATA[$_module+"_PARENT"]=$parent_data;
			
			if ($callback) {
				$callback.call();
			}
		}, 
		"&_module="+encodeURIComponent($_module),
		true);
		
		return [];
	} else {
		return $MODULE_TREE_DATA[$_module];
	}
}