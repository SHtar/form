/**
 * AudioInputField
 */
function getAudioInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"l icon_play input_icon");
//	Pennyutils.events.addEventHandler($input_node_row_sub, "click", new Function("playAudioFile('"+$_form_name+"."+$_field_name+"')"));
	Pennyutils.events.addEventHandler($input_node_row_sub, "click", function(){
		var $_file_path=$($_form_name+"."+$_field_name).value;
		if ($_file_path) {
			if ($("audio_update_callback")) {
				$("audio_update_callback").parentNode.removeChild($("audio_update_callback"));
			}
			
			var $audio_node=document.createElement('audio');
			Pennyutils.setIdName($audio_node, "audio_update_callback");
			$audio_node.controls="controls";
			$audio_node.autoplay="autoplay";
			
			var $source_node=document.createElement('source');
			$source_node.src=$_file_path;
			$source_node.type="audio/mpeg";
			$audio_node.appendChild($source_node);
			
			document.body.appendChild($audio_node);
		}
	});
	$input_node_row_sub.innerHTML="&nbsp;";
	$input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $_form_name+"."+$_field_name);
	Pennyutils.setClassName($input_node_row_input, "l input iw"+($_input_width-1)+"s");
	$input_node_row_input.name=$_field_name;
	
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	$input_node_row.appendChild($input_node_row_input);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"r icon_upload input_icon");
	Pennyutils.events.addEventHandler($input_node_row_sub, "click", new Function("showUploadAudioFileForm('/file/audio', '"+(undefined==$setting["_upload_file_name"]?"":$setting["_upload_file_name"])+"', '"+$_form_name+"."+$_field_name+"')"));
	$input_node_row_sub.innerHTML="&nbsp;";
	$input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"c");
	$input_node_row.appendChild($input_node_row_sub);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["audio"]=getAudioInputField;
function showUploadAudioFileForm ($_upload_save_file_path, $_upload_save_file_name, $_upload_field_id) {
	showUploadFileForm("audio", "dao/upload_file.php", $_upload_save_file_path, $_upload_save_file_name, $_upload_field_id, "updateAudioCallback", null, null);
}
function updateAudioCallback ($_upload_field_id, $_file_path) {
	$($_upload_field_id).value=$_file_path;
	
	playAudioFile($_upload_field_id);
	
	hideRunMask();
}
function playAudioFile ($_upload_field_id) {
	var $_file_path=$($_upload_field_id).value;
	if ($_file_path) {
		if ($("audio_update_callback")) {
			$("audio_update_callback").parentNode.removeChild($("audio_update_callback"));
		}
		
		var $audio_node=document.createElement("embed");
		Pennyutils.setIdName($audio_node, "audio_update_callback");
		Pennyutils.setClassName($audio_node, "dn");
		$audio_node.src=$($_upload_field_id).value;
		$audio_node.autostart=true;
		
		// var $audio_node=document.createElement('audio');
		// Pennyutils.setIdName($audio_node, "audio_update_callback");
		// $audio_node.controls="controls";
		// $audio_node.autoplay="autoplay";
// 		
		// var $source_node=document.createElement('source');
		// $source_node.src=$($_upload_field_id).value;
		// $source_node.type="audio/mpeg";
		// $audio_node.appendChild($source_node);
		
		document.body.appendChild($audio_node);
	}
}