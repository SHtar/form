/**
 * CheckboxInputField
 */
function getCheckboxInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row=document.createElement("div");
//	Pennyutils.setClassName($input_node_row, "input_border");
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $input_field_id);
	Pennyutils.setClassName($input_node_row_input, "dn");
	$input_node_row_input.name=$_field_name;
	$input_node_row.appendChild($input_node_row_input);
	
	var $input_node_row_checkbox_div=document.createElement("div");
	Pennyutils.setIdName($input_node_row_checkbox_div, $input_field_id+".__checkbox_items__");
	Pennyutils.setClassName($input_node_row_checkbox_div, "iw"+$_input_width+"");
	
	var $list=getList($setting["_list_name"]);
	var $checkbox_widget_item;
	for (var $index=0; $index<$list.length; $index++) {
		$checkbox_widget_item=document.createElement("div");
		Pennyutils.setIdName($checkbox_widget_item, $input_field_id+".__checkbox_"+$list[$index]["key"]+"_item__");
		Pennyutils.setClassName($checkbox_widget_item, "l checkbox");
		$checkbox_widget_item.innerHTML=getResourceName($list[$index]["key"]);
		Pennyutils.events.addEventHandler($checkbox_widget_item, "click", new Function("selectCheckboxValues('"+$input_field_id+"', '"+$list[$index]["key"]+"');"));
		$input_node_row_checkbox_div.appendChild($checkbox_widget_item);
	}
	$checkbox_widget_item=document.createElement("div");
	Pennyutils.setClassName($checkbox_widget_item, "c");
	$input_node_row_checkbox_div.appendChild($checkbox_widget_item);
	$input_node_row.appendChild($input_node_row_checkbox_div);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["checkbox"]=getCheckboxInputField;
function selectCheckboxValues ($id, $key) {
	var $values=$($id).value;
	$values=$values?$values.split(","):[];
	var $new_values=[];
	var $is_catch=false;
	for (var $index=0; $index<$values.length; $index++) {
		if ($values[$index]==$key) {
			$is_catch=true;
		} else {
			$new_values[$new_values.length]=$values[$index];
		}
	}
	if (!$is_catch) {
		$new_values[$new_values.length]=$key;
	}
	
	setCheckboxValues($id, $new_values.join(","));
}
function setCheckboxValues ($id, $values) {
	$($id).value=$values;
	$values=$values?$values.split(","):[];
	var $child_nodes=$($id+".__checkbox_items__").childNodes;
	for (var $index=0; $index<$child_nodes.length-1; $index++) {
		Pennyutils.replaceClassName($child_nodes[$index], "checkboxed", "checkbox");	// Clear.
	}
	for (var $index=0; $index<$values.length; $index++) {
		Pennyutils.replaceClassName($($id+".__checkbox_"+$values[$index]+"_item__"), "checkbox", "checkboxed");	// Selected.
	}
}