/**
 * RadioField
 */
function getRadioInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_node_row=document.createElement("div");
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $_form_name+"."+$_field_name);
	Pennyutils.setClassName($input_node_row_input, "dn");
	$input_node_row_input.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="hidden";
	}
	$input_node_row.appendChild($input_node_row_input);
	
	var $input_node_row_radio_div=document.createElement("div");
	Pennyutils.setIdName($input_node_row_radio_div, $_form_name+"."+$_field_name+".__radio_items__");
	Pennyutils.setClassName($input_node_row_radio_div, "iw"+$_input_width+"");
	
	var $list=getList($setting["_list_name"]);
	var $radio_widget_item;
	for (var $index=0; $index<$list.length; $index++) {
		$radio_widget_item=document.createElement("div");
		Pennyutils.setIdName($radio_widget_item, $_form_name+"."+$_field_name+".__radio_"+$list[$index]["key"]+"_item__");
		Pennyutils.setClassName($radio_widget_item, "l radio");
		$radio_widget_item.innerHTML=getResourceName($list[$index]["key"]);
		Pennyutils.events.addEventHandler($radio_widget_item, "click", new Function("setRadioValues('"+$_form_name+"."+$_field_name+"', '"+$list[$index]["key"]+"');"));
		$input_node_row_radio_div.appendChild($radio_widget_item);
	}
	$radio_widget_item=document.createElement("div");
	Pennyutils.setClassName($radio_widget_item, "c");
	$input_node_row_radio_div.appendChild($radio_widget_item);
	
	$input_node_row.appendChild($input_node_row_radio_div);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["radio"]=getRadioInputField;
function setRadioValues ($id, $key) {
	$($id).value=$key;
	var $child_nodes=$($id+".__radio_items__").childNodes;
	for (var $index=0; $index<$child_nodes.length-1; $index++) {
		Pennyutils.replaceClassName($child_nodes[$index], "radioed", "radio");	// Clear.
	}
	Pennyutils.replaceClassName($($id+".__radio_"+$key+"_item__"), "radio", "radioed");	// Selected.
}