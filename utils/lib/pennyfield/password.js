/**
 * HiddenInputField
 */
function getPasswordInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	
	var $id_node_row_input=document.createElement("input");
	Pennyutils.setIdName($id_node_row_input, $input_field_id);
	Pennyutils.setClassName($id_node_row_input, "l input iw"+($_input_width-1));
	if (document.uniqueID) {
	} else {
		$id_node_row_input.type="password";
	}
	$id_node_row_input.name=$_field_name;
	$input_node_row.appendChild($id_node_row_input);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"c");
	$input_node_row.appendChild($input_node_row_sub);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["password"]=getPasswordInputField;