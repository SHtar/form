/**
 * HiddenInputField
 */
function getModifyPasswordInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "tc");
	
	var $button_node=document.createElement("button");
	Pennyutils.setClassName($button_node, "button tc input");
	$button_node.innerHTML=getResourceName("modify_password");
	Pennyutils.events.addEventHandler($button_node, "click", new Function("modifyPasswordProcess('"+$setting["_module_name"]+"', $('"+$_form_name+"."+$setting["_system_id"]+"').value, $('"+$input_field_id+"').value, '"+$input_field_id+"');"));
	$input_node_row.appendChild($button_node);
	
	var $id_node_row_input=document.createElement("input");
	Pennyutils.setIdName($id_node_row_input, $input_field_id);
	Pennyutils.setClassName($id_node_row_input, "dn");
	if (document.uniqueID) {
	} else {
		$id_node_row_input.type="password";
	}
	$id_node_row_input.name=$_field_name;
	$input_node_row.appendChild($id_node_row_input);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["modify_password"]=getModifyPasswordInputField;
function modifyPasswordProcess ($_module_name, $_user_id, $_old_password, $_password_field_id) {
	$FORM_CONFIG["__modify_password_editor_form__"]=
	[
	{"_label_name":"_module_name","_field_name":"_module_name","_edit_field":"hidden","_gridx":0,"_gridy":0}
	,
	{"_label_name":"_user_id","_field_name":"_user_id","_edit_field":"hidden","_gridx":1,"_gridy":0}
	,
	{"_label_name":"_password_field_id","_field_name":"_password_field_id","_edit_field":"hidden","_gridx":2,"_gridy":0}
	,
	{"_label_name":"_old_password","_field_name":"_old_password","_edit_field":"hidden","_gridx":3,"_gridy":0}
	,
	{"_label_name":"_password","_field_name":"_password","_edit_field":"password","_gridx":0,"_gridy":1}
	,
	{"_label_name":"_repassword","_field_name":"_repassword","_edit_field":"password","_gridx":0,"_gridy":2}
	];
	
	if ($_user_id) {
		createWinForm("__modify_password_editor_form__", null, submitModifyPasswordEditorForm);
		
		if ($("__modify_password_editor_form__."+"_module_name")) {
			$("__modify_password_editor_form__."+"_module_name").value=$_module_name;
		}
		if ($("__modify_password_editor_form__."+"_user_id")) {
			$("__modify_password_editor_form__."+"_user_id").value=$_user_id;
		}
		if ($("__modify_password_editor_form__."+"_old_password")) {
			$("__modify_password_editor_form__."+"_old_password").value=$_old_password;
		}
		if ($("__modify_password_editor_form__."+"_password_field_id")) {
			$("__modify_password_editor_form__."+"_password_field_id").value=$_password_field_id;
		}
	}
}
function submitModifyPasswordEditorForm () {
	if ($("__modify_password_editor_form__."+"_password").value!=$("__modify_password_editor_form__."+"_repassword").value) {
		alert(getResourceName("validate_password_error")+getResourceName("!"));
		return;
	}
	
	new net.ContentLoader("dao/update_password.php", function(){
		try {
			var $json_obj=JSON.parse(this.request.responseText);
			
			$($("__modify_password_editor_form__."+"_password_field_id").value).value=$json_obj.password;
			
			Pennyutils.hideObject($FORM_WIN["__modify_password_editor_form__"]);
		} catch ($err) {
			alert("Error connect!\nerr:"+$err+"\nerr.message:"+$err.message+"\n"+this.request.responseText);
		}
	}, 
	"&_module_name="+encodeURIComponent($("__modify_password_editor_form__."+"_module_name").value)
	+
	"&_user_id="+encodeURIComponent($("__modify_password_editor_form__."+"_user_id").value)
	+
	"&_password="+encodeURIComponent($("__modify_password_editor_form__."+"_password").value)
	+
	"&_old_password="+encodeURIComponent($("__modify_password_editor_form__."+"_old_password").value)
	, true);
}