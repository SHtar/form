/**
 * ImageInputField
 */
function getImageInputField ($_form_name, $setting) {
	return getImageInputFields($_form_name, $setting, false);
}
GET_EDITOR_INPUT["image"]=getImageInputField;
function getImagesInputField ($_form_name, $setting) {
	return getImageInputFields($_form_name, $setting, true);
}
GET_EDITOR_INPUT["images"]=getImagesInputField;
function getImageInputFields ($_form_name, $setting, $is_multi) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $_module_name=undefined==$setting["_module_name"]?"":$setting["_module_name"];
	var $_system_id=undefined==$setting["_system_id"]?"":$setting["_system_id"];
	var $_save_file_prefix=undefined==$setting["_save_file_prefix"]?"":$setting["_save_file_prefix"];
	var $_save_file_subfix=undefined==$setting["_save_file_subfix"]?"":$setting["_save_file_subfix"];
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	Pennyutils.events.addEventHandler($input_node_row, "mouseover", new Function("$IMAGE_ITEMS='__image_items_"+$input_field_id+"__';"));
	Pennyutils.events.addEventHandler($input_node_row, "mouseout", new Function("$IMAGE_ITEMS=null;hideImageItems('__image_items_"+$input_field_id+"__', '"+$input_field_id+".image_items');"));
	Pennyutils.events.addEventHandler($input_node_row, "blur", new Function("$IMAGE_ITEMS=null;hideImageItems('__image_items_"+$input_field_id+"__', '"+$input_field_id+".image_items');"));
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $input_field_id);
	Pennyutils.setClassName($input_node_row_input, "l input iw"+($_input_width-1)+"");
	$input_node_row_input.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	Pennyutils.events.addEventHandler($input_node_row_input, "mouseover", new Function("showImageItems('"+$input_field_id+"',"+($is_multi?"true":"false")+");"));
	$input_node_row.appendChild($input_node_row_input);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"r icon_sub");
	Pennyutils.events.addEventHandler($input_node_row_sub, "click", new Function("showUploadImageFileForm('/file/img', '"+(undefined==$setting["_upload_file_name"]?"":$setting["_upload_file_name"])+"', '"+$input_field_id+"', "+($is_multi?"true":"false")+", '"+$_module_name+"', '"+($_system_id?$_form_name+"."+$_system_id:"")+"', '"+$_save_file_prefix+"', '"+$_save_file_subfix+"')"));
	$input_node_row_sub.innerHTML="&nbsp";
	$input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"c");
	$input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_select_div=document.createElement("div");
	Pennyutils.setIdName($input_node_row_select_div, $input_field_id+".image_items");
	Pennyutils.setClassName($input_node_row_select_div, "pr tl");
	Pennyutils.setCssStyle($input_node_row_select_div, "height:0px;");
	$input_node_row.appendChild($input_node_row_select_div);
	
	return $input_node_row;
}
function showUploadImageFileForm ($_upload_save_file_path, $_upload_save_file_name, $_upload_field_id, $is_multi, $_module_name, $_system_id, $_save_file_prefix, $_save_file_subfix) {
	showUploadFileForm("image", "dao/upload_file.php", $_upload_save_file_path, $_upload_save_file_name, $_upload_field_id, $is_multi?"updateImagesCallback":"updateImageCallback", null, null, $_module_name, $_system_id, $_save_file_prefix, $_save_file_subfix);
}
function updateImageCallback ($_upload_field_id, $_file_path) {
	$($_upload_field_id).value=$_file_path;
	
	hideRunMask();
	
	showImageItems($_upload_field_id, false);
}
function updateImagesCallback ($_upload_field_id, $_file_path) {
	var $images_paths=$($_upload_field_id).value.split(";");
	var $images_new_paths=[];
	for (var $index=0; $index<$images_paths.length; $index++) {
		if ($images_paths[$index].trim()) {
			$images_new_paths[$images_new_paths.length]=$images_paths[$index];
		}
	}
	$images_new_paths[$images_new_paths.length]=$_file_path;
	
	$($_upload_field_id).value=$images_new_paths.join(";");;
	
	hideRunMask();
	
	showImageItems($_upload_field_id, true);
}
function showImageItems ($input_field_id, $is_multi) {
	$($input_field_id+".image_items").innerHTML="";
	
	var $image_items_widget=document.createElement("div");
	Pennyutils.setClassName($image_items_widget, $is_multi?"image_items pa":"image_item pa");
	
	var $is_show=false;
	
	var $images_paths=$($input_field_id).value.split(";");
	var $image_div_node;
	var $image_node;
	var $image_del_node;
	for (var $index=0; $index<$images_paths.length; $index++) {
		if ($images_paths[$index].trim()) {
			$image_div_node=document.createElement("div");
			$image_div_node.title=$images_paths[$index];
			$image_div_node.alt=$images_paths[$index];
			Pennyutils.setClassName($image_div_node, "l image_item_div");
			
			$image_node=document.createElement("img");
			Pennyutils.setClassName($image_node, "image_item_img");
			$image_node.src=$images_paths[$index];
			$image_div_node.appendChild($image_node);
			
			$image_del_node=document.createElement("div");
			$image_del_node.innerHTML=getResourceName("delete");
			Pennyutils.events.addEventHandler($image_del_node, "click", new Function("removeImageItems('"+$input_field_id+"', "+$index+", "+($is_multi?"true":"false")+");"));
			$image_div_node.appendChild($image_del_node);
			
			$image_items_widget.appendChild($image_div_node);
			
			$is_show=true;
		}
	}
	$image_div_node=document.createElement("div");
	Pennyutils.setClassName($image_div_node, "c");
	$image_items_widget.appendChild($image_div_node);
	
	if ($is_show) {
		$($input_field_id+".image_items").appendChild($image_items_widget);
		Pennyutils.showObject($($input_field_id+".image_items"));
	}
}
function removeImageItems ($input_field_id, $remove_index, $is_multi) {
	var $images_paths=$($input_field_id).value.split(";");
	var $images_new_paths=[];
	for (var $index=0; $index<$images_paths.length; $index++) {
		if ($remove_index!=$index) {
			$images_new_paths[$images_new_paths.length]=$images_paths[$index];
		}
	}
	$($input_field_id).value=$images_new_paths.join(";");
	Pennyutils.hideObject($input_field_id+".image_items");
	showImageItems($input_field_id, $is_multi);
}
var $IMAGE_ITEMS=null;
function hideImageItems ($image_items, $input_field_id) {
	$timeoutID=setTimeout("if ($IMAGE_ITEMS!='"+$image_items+"') {$('"+$input_field_id+"').innerHTML='';Pennyutils.hideObject('"+$input_field_id+"');clearTimeout($timeoutID);}", 300);
}