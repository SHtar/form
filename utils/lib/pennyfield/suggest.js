/**
 * Suggest
 */
function getSuggestInputField ($_form_name, $setting) {
	var $_label_name=$setting["_label_name"];
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $suggest_div=document.createElement("div");
	Pennyutils.setClassName($suggest_div, "input_border");
	
	Pennyutils.events.addEventHandler($suggest_div, "mouseover", new Function("$SUGGEST_ITEMS='__suggest_items_"+$input_field_id+"__';"));
	Pennyutils.events.addEventHandler($suggest_div, "mouseout", new Function("$SUGGEST_ITEMS=null;hideSuggestItems('__suggest_items_"+$input_field_id+"__', '"+$input_field_id+".suggest_list')"));
	Pennyutils.events.addEventHandler($suggest_div, "blur", new Function("$SUGGEST_ITEMS=null;hideSuggestItems('__suggest_items_"+$input_field_id+"__', '"+$input_field_id+".suggest_list')"));
	
	var $input_field=document.createElement("input");
	Pennyutils.setIdName($input_field, $input_field_id);
	Pennyutils.setClassName($input_field, "input iw"+$_input_width+"s");
	$input_field.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$input_field.type="text";
	}
	Pennyutils.events.addEventHandler($input_field, "keyup", new Function("showSuggest('"+$_form_name+"', '"+$_field_name+"', '"+$setting["_module"]+"', '"+$setting["_key"]+"', '"+(undefined==$setting["_map"]?"":$setting["_map"])+"', '"+$setting["_value"]+"', '"+$setting["_equal"]+"', '"+$setting["_like"]+"', '"+$setting["_order_by"]+"', '"+$setting["_type"]+"');"));
	$suggest_div.appendChild($input_field);
	
	// var $suggest_list_node=document.createElement("div");
	// Pennyutils.setIdName($suggest_list_node, $input_field_id+".suggest_list");
	// Pennyutils.setClassName($suggest_list_node, "pa suggest_list dn");
	// $suggest_div.appendChild($suggest_list_node);
	
	var $suggest_list_node_div=document.createElement("div");
	Pennyutils.setIdName($suggest_list_node_div, $input_field_id+".suggest_selector_div");
	Pennyutils.setClassName($suggest_list_node_div, "pr bgce tl");
	Pennyutils.setCssStyle($suggest_list_node_div, "height:0px;");
	$suggest_div.appendChild($suggest_list_node_div);
	
	var $suggest_list_node=document.createElement("div");
	Pennyutils.setIdName($suggest_list_node, $input_field_id+".suggest_list");
	Pennyutils.setClassName($suggest_list_node, "pa suggest_list dn");
	$suggest_list_node_div.appendChild($suggest_list_node);
	
	return $suggest_div;
}
GET_EDITOR_INPUT["suggest"]=getSuggestInputField;
function showSuggest ($_form_name, $_field_name, $_module, $_key, $_map, $_value, $_equal, $_like, $_order_by, $_type) {
	var $_text=$($_form_name+"."+$_field_name).value;
	var $_system_id=$($_form_name+"._"+$_module+"_id")?$($_form_name+"._"+$_module+"_id").value:"";
//	Pennyutils.showObject($_form_name+"."+$_field_name+".suggest_selector_div");
	new net.ContentLoader("dao/suggest.php", function(){
//		alert("showSuggest:\r\n"+this.request.responseText);
		var $json_obj=JSON.parse(this.request.responseText);
		// $DATA_COLUMNS=$json_obj;
		var $suggest_list_node_key=$_form_name+"."+$_field_name+".suggest_list";
		var $suggest_list_node=$($suggest_list_node_key);
		$suggest_list_node.innerHTML="";
		
		var $row_node;
		var $_value_texts;
		var $_actions;
		$_key=$_key.split(",");
		$_map=$_map?$_map.split(","):$_key;
		$_value=$_value.split(",");
		for (var $row_index=0; $row_index<$json_obj.length; $row_index++) {
			$_value_texts=[];
			for (var $_value_index=0; $_value_index<$_value.length; $_value_index++) {
				$_value_texts[$_value_texts.length]=$json_obj[$row_index][$_value[$_value_index]];
			}
			$row_node=document.createElement("div");
			$row_node.innerHTML=$_value_texts.join(" ");
			Pennyutils.setIdName($row_node, "suggest_items_"+$_form_name+"_"+$_field_name+"_"+$_module+"_"+$row_index+"_item_node");
			Pennyutils.setClassName($row_node, "suggest_item");
			Pennyutils.events.addEventHandler($row_node, "mouseover", new Function("Pennyutils.replaceCssStyle($('suggest_items_"+$_form_name+"_"+$_field_name+"_"+$_module+"_"+$row_index+"_item_node'), 'background', 'background:#fefefe;');"));
			Pennyutils.events.addEventHandler($row_node, "mouseout", new Function("Pennyutils.replaceCssStyle($('suggest_items_"+$_form_name+"_"+$_field_name+"_"+$_module+"_"+$row_index+"_item_node'), 'background', '');"));
			if ("0"==$_type) {
				Pennyutils.events.addEventHandler($row_node, "click", new Function("loadDataAction('"+$json_obj[$row_index][$_key]+"');Pennyutils.hideObject('"+$suggest_list_node_key+"');"));
			} else {
				$_actions=[];
				for (var $_map_index=0; $_map_index<$_map.length; $_map_index++) {
					$_actions[$_actions.length]="setInputValue('"+$_form_name+"."+$_map[$_map_index]+"','"+$json_obj[$row_index][$_key[$_map_index]].replace(/'/g, "&apos;")+"'.replace(/&apos;/g, \"'\"));";	// '=&apos; "=&quot;
				}
				$_actions[$_actions.length]="Pennyutils.hideObject('"+$suggest_list_node_key+"');";
				Pennyutils.events.addEventHandler($row_node, "click", new Function($_actions.join("")));
			}
			
			$suggest_list_node.appendChild($row_node);
		}
		
		Pennyutils.showObject($suggest_list_node);
	}, 
	"&_module="+encodeURIComponent($_module)+
	"&_key="+encodeURIComponent($_key)+
	"&_value="+encodeURIComponent($_value)+
	"&_equal="+encodeURIComponent($_equal)+
	"&_like="+encodeURIComponent($_like)+
	"&_order_by="+encodeURIComponent($_order_by)+
	"&_text="+encodeURIComponent($_text)+
	"&_system_id="+encodeURIComponent($_system_id),
	true);
}
var $SUGGEST_ITEMS=null;
function hideSuggestItems () {
	$timeoutID=setTimeout("if ($SUGGEST_ITEMS!='"+arguments[0]+"') {Pennyutils.hideObject('"+arguments[1]+"');clearTimeout($timeoutID);}", 300);
}