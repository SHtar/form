/**
 * HiddenInputField
 */
function getHiddenInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $id_node_row_input=document.createElement("input");
	Pennyutils.setIdName($id_node_row_input, $input_field_id);
	if (document.uniqueID) {
	} else {
		$id_node_row_input.type="hidden";
	}
	$id_node_row_input.name=$_field_name;
	
	return $id_node_row_input;
}
GET_EDITOR_INPUT["hidden"]=getHiddenInputField;