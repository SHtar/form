/**
 * UrlInputField
 */
function getUrlInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];

	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $input_field_id);
	Pennyutils.setClassName($input_node_row_input, "l input_border input iw"+$_input_width+"ss");
	$input_node_row_input.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	$input_node_row.appendChild($input_node_row_input);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"l input_border icon_sub");
//		Pennyutils.events.addEventHandler($input_node_row_sub, "click", new Function("showURLForm('"+$input_field_id+"')"));
	Pennyutils.events.addEventHandler($input_node_row_sub, "click", function(){
		var $form_name="url_form";
		$FORM_CONFIG[$form_name]=
		[
		{"_label_name":"_field","_field_name":"_field","_edit_field":"hidden"}
		,
		{"_label_name":"_type","_field_name":"_type","_edit_field":"select","_list_name":"url_type"}
		,
		{"_label_name":"_value","_field_name":"_value","_edit_field":"text"}
		];
		createWinForm(
			$form_name, 
			function(){
				var $value=$($input_field_id).value;
				if ($value) {
					try {
						var $json=JSON.parse($value);
						for (var $key in $json) {
							if ($($form_name+"."+$key)) {
								$($form_name+"."+$key).value=$json[$key];
							}
						}
					} catch ($err) {
						
					}
				}
			}, 
			function(){
				alert($input_field_id);
				$value={};
				for (var $col_index=0; $col_index<$FORM_CONFIG[$form_name].length; $col_index++) {
					if ($($form_name+"."+$FORM_CONFIG[$form_name][$col_index]["_field_name"])) {
						$value[$FORM_CONFIG[$form_name][$col_index]["_field_name"]]=$($form_name+"."+$FORM_CONFIG[$form_name][$col_index]["_field_name"]).value;
					}
				}
				$($input_field_id).value=JSON.stringify($value);
				Pennyutils.hideObject($FORM_WIN[$form_name]);
			}
		);
	});
	$input_node_row_sub.innerHTML="&nbsp;";
	$input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"c");
	$input_node_row.appendChild($input_node_row_sub);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["url"]=getUrlInputField;