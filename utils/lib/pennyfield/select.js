/**
 * SelectInputField
 */
function getSelectInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	var $list_name=$setting["_list_name"];
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	Pennyutils.events.addEventHandler($input_node_row, "mouseover", new Function("$SELECT_ITEMS='__select_items_"+$input_field_id+"__';"));
	Pennyutils.events.addEventHandler($input_node_row, "mouseout", new Function("$SELECT_ITEMS=null;hideSelectItems('__select_items_"+$input_field_id+"__','__select_items_"+$list_name+"__')"));
	Pennyutils.events.addEventHandler($input_node_row, "blur", new Function("$SELECT_ITEMS=null;hideSelectItems('__select_items_"+$input_field_id+"__','__select_items_"+$list_name+"__')"));
	
	var $input_node=document.createElement("input");
	Pennyutils.setIdName($input_node, $input_field_id);
	Pennyutils.setClassName($input_node, "dn");
	$input_node.value=$input_field_id;
	$input_node.name=$_field_name;
	$input_node_row.appendChild($input_node);
	
	var $list_name_node=document.createElement("input");
	Pennyutils.setIdName($list_name_node, $input_field_id+".select_name");
	Pennyutils.setClassName($list_name_node, "dn");
	$list_name_node.value=$list_name;
	$input_node_row.appendChild($list_name_node);
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $input_field_id+".value");
	Pennyutils.setClassName($input_node_row_input, "l input iw"+($_input_width-1)+"");
	$input_node_row_input.readOnly=true;
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	Pennyutils.events.addEventHandler($input_node_row_input, "click", new Function("getSelectItems('"+$input_field_id+"', '"+$list_name+"')"));
	
	$input_node_row.appendChild($input_node_row_input);
	
	var $select_down_node=document.createElement("div");
	$select_down_node.innerHTML="&nbsp;";
	Pennyutils.setClassName($select_down_node, "r icon_down input_icon");
	Pennyutils.events.addEventHandler($select_down_node, "click", new Function("getSelectItems('"+$input_field_id+"', '"+$list_name+"')"));
	$input_node_row.appendChild($select_down_node);
	
	var $select_clear_node=document.createElement("div");
	Pennyutils.setClassName($select_clear_node, "c");
	$input_node_row.appendChild($select_clear_node);
	
	var $input_node_row_select_div=document.createElement("div");
	Pennyutils.setIdName($input_node_row_select_div, $input_field_id+".select_items");
	Pennyutils.setClassName($input_node_row_select_div, "pr tl");
	Pennyutils.setCssStyle($input_node_row_select_div, "height:0px;");
	$input_node_row.appendChild($input_node_row_select_div);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["select"]=getSelectInputField;
function getSelectItems ($input_field_id, $list_name) {
	if ($("__select_items_"+$list_name+"__")) {	// 
		$select_widget=$("__select_items_"+$list_name+"__");
	} else {	// First time.
		$select_widget=document.createElement("div");
		Pennyutils.setIdName($select_widget, "__select_items_"+$list_name+"__");
		Pennyutils.setClassName($select_widget, "select_items pa");
		
		var $select_widget_input=document.createElement("input");
		Pennyutils.setIdName($select_widget_input, "__select_items_"+$list_name+"_input__");
		Pennyutils.setClassName($select_widget_input, "dn");
		Pennyutils.setCssStyle($select_widget_input, "height:0px;");
		$select_widget_input.readOnly="readOnly";
		if (document.uniqueID) {
		} else {
			$select_widget_input.type="text";
		}
		$select_widget.appendChild($select_widget_input);
		
		var $list=getList($list_name);
		var $select_widget_item_node;
		var $select_widget_item_text_node;
		for (var $index=0; $index<$list.length; $index++) {
			$select_widget_item_node=document.createElement("div");
			Pennyutils.setClassName($select_widget_item_node, "select_item");
			Pennyutils.setIdName($select_widget_item_node, "select_items_"+$list_name+"_"+$list[$index]["key"]+"_item_node");
			Pennyutils.events.addEventHandler($select_widget_item_node, "mouseover", new Function("Pennyutils.replaceCssStyle($('select_items_"+$list_name+"_"+$list[$index]["key"]+"_item_node'), 'background', 'background:#fefefe;');"));
			Pennyutils.events.addEventHandler($select_widget_item_node, "mouseout", new Function("Pennyutils.replaceCssStyle($('select_items_"+$list_name+"_"+$list[$index]["key"]+"_item_node'), 'background', '');"));
			Pennyutils.events.addEventHandler($select_widget_item_node, "click", new Function("chooseSelectItemsItem('"+$list_name+"','"+$list[$index]["key"]+"','"+$list[$index]["key"]+"')"));
			
			$select_widget_item_text_node=document.createElement("div");
			Pennyutils.setClassName($select_widget_item_text_node, "select_item_text");
			$select_widget_item_text_node.innerHTML=getResourceName($list[$index]["key"]);
			$select_widget_item_node.appendChild($select_widget_item_text_node);
			
			$select_widget.appendChild($select_widget_item_node);
		}
		
		document.body.appendChild($select_widget);
	}
	
	$("__select_items_"+$list_name+"_input__").value=$input_field_id;
	
	$($input_field_id+".select_items").appendChild($select_widget);	
	Pennyutils.showObject("__select_items_"+$list_name+"__");
}
function setSelectValues ($id, $key) {
	$($id).value=$key;
	var $value="";
	var $list=getList($($id+".select_name").value);
	var $temp_key=","+$key+",";
	for (var $index=0; $index<$list.length; $index++) {
		if ($key==$list[$index]["key"]) {
			$value=getResourceName($list[$index]["key"]);
			break;
		}
	}
	$($id+".value").value=$value;
}
function chooseSelectItemsItem ($list_name, $key, $value) {
	var $input_field_id=$("__select_items_"+$list_name+"_input__").value;
	$($input_field_id).value=$key;
	$($input_field_id+".value").value=$value;
	Pennyutils.hideObject("__select_items_"+$list_name+"__");
}
var $SELECT_ITEMS=null;
function hideSelectItems () {
	$timeoutID=setTimeout("if ($SELECT_ITEMS!='"+arguments[0]+"') {Pennyutils.hideObject('"+arguments[1]+"');clearTimeout($timeoutID);}", 300);
}
function showSelectItems () {
	Pennyutils.showObject(arguments[0]);
//	Pennyutils.relateSite(arguments[0]+"_list", arguments[0]+"_field", 'l', 'b');
}