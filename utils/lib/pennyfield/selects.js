/**
 * SelectsInputField
 */
function getSelectsInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	var $list_name=$setting["_list_name"];
	
	var $selects_div=document.createElement("div");
	Pennyutils.setClassName($selects_div, "input_border");
	Pennyutils.events.addEventHandler($selects_div, "mouseover", new Function("$SELECTS_ITEMS='__selects_items_"+$input_field_id+"__';"));
	Pennyutils.events.addEventHandler($selects_div, "mouseout", new Function("$SELECTS_ITEMS=null;hideSelectsItems('__selects_items_"+$input_field_id+"__','__selects_items_"+$list_name+"__')"));
	Pennyutils.events.addEventHandler($selects_div, "blur", new Function("$SELECTS_ITEMS=null;hideSelectsItems('__selects_items_"+$input_field_id+"__','__selects_items_"+$list_name+"__')"));
	Pennyutils.events.addEventHandler($selects_div, "click", new Function("getSelectsItems('"+$input_field_id+"', '"+$list_name+"')"));
	
	var $selects_input_node=document.createElement("input");
	Pennyutils.setIdName($selects_input_node, $input_field_id);
	Pennyutils.setClassName($selects_input_node, "dn");
	$selects_input_node.value=$input_field_id;
	$selects_input_node.name=$_field_name;
	$selects_div.appendChild($selects_input_node);
	
	var $selects_name_node=document.createElement("input");
	Pennyutils.setIdName($selects_name_node, $input_field_id+".selects_name");
	Pennyutils.setClassName($selects_name_node, "dn");
	$selects_name_node.value=$list_name;
	$selects_div.appendChild($selects_name_node);
	
	var $selects_value_node=document.createElement("input");
	Pennyutils.setIdName($selects_value_node, $input_field_id+".value");
	Pennyutils.setClassName($selects_value_node, "l input iw"+($_input_width)+"s");
	$selects_value_node.readOnly=true;
	if (document.uniqueID) {
	} else {
		$selects_value_node.type="text";
	}
//	Pennyutils.events.addEventHandler($selects_value_node, "click", new Function("getSelectsItems('"+$input_field_id+"', '"+$list_name+"')"));
	$selects_div.appendChild($selects_value_node);
	
	var $selects_down_node=document.createElement("div");
	$selects_down_node.innerHTML="&nbsp;";
	Pennyutils.setClassName($selects_down_node, "r icon_down input_icon");
//	Pennyutils.events.addEventHandler($selects_down_node, "click", new Function("getSelectsItems('"+$input_field_id+"', '"+$list_name+"')"));
	$selects_div.appendChild($selects_down_node);
	
	var $selects_clear_node=document.createElement("div");
	Pennyutils.setClassName($selects_clear_node, "c");
	$selects_div.appendChild($selects_clear_node);
	
	var $selects_list_div=document.createElement("div");
	Pennyutils.setIdName($selects_list_div, $input_field_id+".selects_items");
	Pennyutils.setClassName($selects_list_div, "pr tl");
	Pennyutils.setCssStyle($selects_list_div, "height:0px;");
	$selects_div.appendChild($selects_list_div);
	

//	Pennyutils.setClassName($selects_list_div, "pa selects_items dn");
//	Pennyutils.setCssStyle($selects_list_div,"position:absolute;text-align:left;height:0;");
	
//	Pennyutils.events.addEventHandler($selects_list_div, "mouseover", new Function("$SELECTS_ITEMS='__selects_items_"+$list_name+"__';"));
//	Pennyutils.events.addEventHandler($selects_list_div, "mouseout", new Function("$SELECTS_ITEMS=null;hideSelectsItems('__selects_items_"+$list_name+"__')"));
	
	
	return $selects_div;
}
GET_EDITOR_INPUT["selects"]=getSelectsInputField;
function getSelectsItems ($input_field_id, $list_name) {
	var $selects_widget;
	if ($("__selects_items_"+$list_name+"__")) {	// Secondary use.
		$selects_widget=$("__selects_items_"+$list_name+"__");
	} else {	// First time.
		$selects_widget=document.createElement("div");
		Pennyutils.setIdName($selects_widget, "__selects_items_"+$list_name+"__");
		Pennyutils.setClassName($selects_widget, "select_items pa");
		
		var $selects_widget_input=document.createElement("input");
		Pennyutils.setIdName($selects_widget_input, "__selects_items_"+$list_name+"_input__");
		Pennyutils.setClassName($selects_widget_input, "dn");
		Pennyutils.setCssStyle($selects_widget_input, "height:0px;");
		$selects_widget_input.readOnly="readOnly";
		if (document.uniqueID) {
		} else {
			$selects_widget_input.type="text";
		}
		$selects_widget.appendChild($selects_widget_input);
		
		var $list=getList($list_name);
		var $selects_widget_item_node;
		var $selects_widget_item_text_node;
		for (var $index=0; $index<$list.length; $index++) {
			$selects_widget_item_node=document.createElement("div");
			Pennyutils.setClassName($selects_widget_item_node, "selects_item");
			Pennyutils.setIdName($selects_widget_item_node, "selects_items_"+$list_name+"_"+$list[$index]["key"]+"_item_node");
			// Pennyutils.setClassName($selects_widget_item_node, "checkbox b1 tl p5 p5");
			// Pennyutils.setCssStyle($selects_widget_item_node, "height:20px;");
			// $selects_widget_item_node.innerHTML=$list[$index]["value"];
			Pennyutils.events.addEventHandler($selects_widget_item_node, "click", new Function("chooseSelectsItemsItem('"+$list_name+"','"+$list[$index]["key"]+"');"));
			Pennyutils.events.addEventHandler($selects_widget_item_node, "mouseover", new Function("Pennyutils.replaceCssStyle($('selects_items_"+$list_name+"_"+$list[$index]["key"]+"_item_node'), 'background', 'background:#fefefe;');"));
			Pennyutils.events.addEventHandler($selects_widget_item_node, "mouseout", new Function("Pennyutils.replaceCssStyle($('selects_items_"+$list_name+"_"+$list[$index]["key"]+"_item_node'), 'background', '');"));
			
			$selects_widget_item_text_node=document.createElement("div");
			Pennyutils.setClassName($selects_widget_item_text_node, "selects_item_text");
			$selects_widget_item_node.appendChild($selects_widget_item_text_node);
			
			$selects_widget_item_checkbox_node=document.createElement("div");
			Pennyutils.setIdName($selects_widget_item_checkbox_node, "selects_items_"+$list_name+"_"+$list[$index]["key"]+"_item");
			Pennyutils.setClassName($selects_widget_item_checkbox_node, "checkbox");
			$selects_widget_item_checkbox_node.innerHTML=getResourceName($list[$index]["key"]);
			$selects_widget_item_text_node.appendChild($selects_widget_item_checkbox_node);
			
			$selects_widget.appendChild($selects_widget_item_node);
		}
		
		document.body.appendChild($selects_widget);
	}
	
	$("__selects_items_"+$list_name+"_input__").value=$input_field_id;
	
	$($input_field_id+".selects_items").appendChild($selects_widget);
	
	Pennyutils.showObject("__selects_items_"+$list_name+"__");
	// Pennyutils.setCssStyle($selects_widget, "background:#f0f0f0;");
	
	setSelectsValuesItems($list_name, $($input_field_id).value);
}
function setSelectsValues ($id, $key) {
	$($id).value=$key;
	var $value=[];
	var $list=getList($($id+".selects_name").value);
	var $temp_key=","+$key+",";
	for (var $index=0; $index<$list.length; $index++) {
		if ($temp_key.indexOf($list[$index]["key"])>=0) {
			$value[$value.length]=getResourceName($list[$index]["key"]);
		}
	}
	$($id+".value").value=$value.join(",");
}
function chooseSelectsItemsItem ($list_name, $key) {
	var $old_key=$($("__selects_items_"+$list_name+"_input__").value).value.split(",");
	var $new_key=[];
	var $is_catch=false;
	for (var $index=0; $index<$old_key.length; $index++) {
		if ($key==$old_key[$index]) {
			$is_catch=true;
		} else {
			$new_key[$new_key.length]=$old_key[$index];
		}
	}
	if (!$is_catch) {
		$new_key[$new_key.length]=$key;
	}
	setSelectsValues($("__selects_items_"+$list_name+"_input__").value, $new_key.join(","));
	setSelectsValuesItems($list_name, $new_key.join(","));
}
function setSelectsValuesItems ($list_name, $key) {
	// Clear.
	var $list=getList($list_name);
	for (var $index=0; $index<$list.length; $index++) {
		Pennyutils.replaceClassName($("selects_items_"+$list_name+"_"+$list[$index]["key"]+"_item"), "checkboxed", "checkbox");
	}
	
	// Selected.
	var $keys=$key.split(",");
	var $node;
	for (var $index=0; $index<$keys.length; $index++) {
		$node=$("selects_items_"+$list_name+"_"+$keys[$index]+"_item");
		if ($node) {
			Pennyutils.replaceClassName($node, "checkbox", "checkboxed");
		}
	}
}
var $SELECTS_ITEMS=null;
function hideSelectsItems () {
	$timeoutID=setTimeout("if ($SELECTS_ITEMS!='"+arguments[0]+"') {Pennyutils.hideObject('"+arguments[1]+"');clearTimeout($timeoutID);}", 300);
}
function showSelectsItems () {
	Pennyutils.showObject(arguments[0]);
}