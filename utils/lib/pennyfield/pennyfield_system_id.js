/**
 * SystemIdInputField
 */
function getSystemIdInputField ($_form_name, $setting) {
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $_form_name+"."+$_field_name);
	Pennyutils.setClassName($input_node_row_input, "input c4 iw"+$_input_width+"s");
	$input_node_row_input.name=$_field_name;
	$input_node_row_input.readOnly=true;
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	
	return $input_node_row_input;
}
