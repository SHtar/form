/**
 * TextInputField
 */
function getTextInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	
	var $input_field=document.createElement("input");
	Pennyutils.setIdName($input_field, $_form_name+"."+$_field_name);
	Pennyutils.setClassName($input_field, "input iw"+$_input_width+"s");
	$input_field.name=$_field_name;
	if (document.uniqueID) {
	} else {
		$input_field.type="text";
	}
	
	$input_node_row.appendChild($input_field);
	return $input_node_row;
}
GET_EDITOR_INPUT["text"]=getTextInputField;