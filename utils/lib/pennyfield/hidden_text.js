/**
 * HiddenTextInputField
 */
function getHiddenTextInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $input_field_id);
	Pennyutils.setClassName($input_node_row_input, "input c9 iw"+$_input_width+"s");
	$input_node_row_input.name=$_field_name;
	$input_node_row_input.readOnly=true;
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	return $input_node_row_input;
}
GET_EDITOR_INPUT["hidden_text"]=getHiddenTextInputField;