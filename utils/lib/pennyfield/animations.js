/**
 * AnimationInputField
 */
function getAnimationsInputField ($_form_name, $setting) {
	var $_field_name=$setting["_field_name"];
	var $_input_width=undefined==$setting["_input_width"]||$setting["_input_width"].length==0?"3":$setting["_input_width"];
	
	var $input_node_row=document.createElement("div");
	Pennyutils.setClassName($input_node_row, "input_border");
	
	var $input_field_id=$_form_name+"."+$_field_name;
	
	var $input_node_row_input=document.createElement("input");
	Pennyutils.setIdName($input_node_row_input, $input_field_id);
	Pennyutils.setClassName($input_node_row_input, "l input iw"+($_input_width-1)+"s");
	$input_node_row_input.name=$_field_name;
	
	if (document.uniqueID) {
	} else {
		$input_node_row_input.type="text";
	}
	$input_node_row.appendChild($input_node_row_input);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"r icon_upload input_icon");
	Pennyutils.events.addEventHandler($input_node_row_sub, "click", new Function("showAnimationsEditor('"+$input_field_id+"');"));
	$input_node_row_sub.innerHTML="&nbsp;";
	$input_node_row.appendChild($input_node_row_sub);
	
	var $input_node_row_sub=document.createElement("div");
	Pennyutils.setClassName($input_node_row_sub,"c");
	$input_node_row.appendChild($input_node_row_sub);
	
	return $input_node_row;
}
GET_EDITOR_INPUT["animations"]=getAnimationsInputField;
var $ANIMATIONS_WIN=null;
function showAnimationsEditor ($input_field_id) {
	if (null==$ANIMATIONS_WIN) {
		$ANIMATIONS_WIN=createWindow();
		
		var $animations_pannel=document.createElement("div");
		
		var $animations_widget_input=document.createElement("input");
		Pennyutils.setIdName($animations_widget_input, "__animations_input__");
		Pennyutils.setClassName($animations_widget_input, "dn");
		Pennyutils.setCssStyle($animations_widget_input, "height:0px;");
		$animations_widget_input.readOnly="readOnly";
		if (document.uniqueID) {
		} else {
			$animations_widget_input.type="text";
		}
		$animations_pannel.appendChild($animations_widget_input);
		
		
		
		// Animations.
		var $animations_node=document.createElement("div");
		Pennyutils.setIdName($animations_node, "animations");
		$animations_pannel.appendChild($animations_node);
		
		// Editor.
		var $animation_prototype_editor_node=document.createElement("div");
		Pennyutils.setIdName($animation_prototype_editor_node, "animation_prototype_editor");
		Pennyutils.setClassName($animation_prototype_editor_node, "b1 m5 p5");
		$animations_pannel.appendChild($animation_prototype_editor_node);
		
		var $form_name="animation_prototype_form";
		$FORM_CONFIG[$form_name]=[
				{"_label_name":"index","_field_name":"index","_edit_field":"text","_gridx":"0","_gridy":"0"}
				,
				{"_label_name":"name","_field_name":"name","_edit_field":"text","_gridx":"0","_gridy":"0"}
				,
				{"_label_name":"loop","_field_name":"loop","_edit_field":"text","_gridx":"0","_gridy":"0"}
				,
				{"_label_name":"timeout","_field_name":"timeout","_edit_field":"text","_gridx":"1","_gridy":"0"}
				,
				{"_label_name":"delay","_field_name":"delay","_edit_field":"text","_gridx":"2","_gridy":"0"}
			];
		$animation_prototype_editor_node.appendChild(createForm($form_name));
		
		// Buttons.
		var $animation_prototype_editor_buttons=document.createElement("div");
		// Remove button.
		var $animation_prototype_editor_remove_button=document.createElement("div");
		$animation_prototype_editor_remove_button.innerHTML=getResourceName("remove_animation");;
		Pennyutils.events.addEventHandler($animation_prototype_editor_remove_button, "click", removeAnimationItem);
		Pennyutils.setClassName($animation_prototype_editor_remove_button, "l tc button");
		$animation_prototype_editor_buttons.appendChild($animation_prototype_editor_remove_button);
		// Apply button.
		var $animation_prototype_editor_apply_button=document.createElement("button");
		$animation_prototype_editor_apply_button.innerHTML=getResourceName("apply_animation");
		Pennyutils.setClassName($animation_prototype_editor_apply_button, "r tc button");
		Pennyutils.events.addEventHandler($animation_prototype_editor_apply_button, "click", 
			function(){
				// Update memery value.
				var $form_name="animation_prototype_form";
				for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
					$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX][$FORM_CONFIG[$form_name][$field_index]["_field_name"]]=$($form_name+"."+$FORM_CONFIG[$form_name][$field_index]["_field_name"]).value;
				}
				// Update input value.
//				$($("__animations_input__").value).value=JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]);
				setInputValue($("__animations_input__").value, JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]));
				
				Pennyutils.hideObject($ANIMATIONS_WIN);
			});
		$animation_prototype_editor_buttons.appendChild($animation_prototype_editor_apply_button);
		// Clear float.
		$animation_prototype_editor_clear_button=document.createElement("div");
		Pennyutils.setClassName($animation_prototype_editor_clear_button, "c");
		$animation_prototype_editor_buttons.appendChild($animation_prototype_editor_clear_button);
		
		$animation_prototype_editor_node.appendChild($animation_prototype_editor_buttons);
		
		// Animation motions.
		var $animation_prototype_motions_node=document.createElement("div");
		Pennyutils.setIdName($animation_prototype_motions_node, "animation_prototype_motions");
		$animations_pannel.appendChild($animation_prototype_motions_node);
		
		// Editor.
		var $animation_prototype_motion_prototype_editor_node=document.createElement("div");
		Pennyutils.setIdName($animation_prototype_motion_prototype_editor_node, "animation_prototype_motion_prototype_editor");
		Pennyutils.setClassName($animation_prototype_motion_prototype_editor_node, "b1 m5 p5");
		$animations_pannel.appendChild($animation_prototype_motion_prototype_editor_node);
		
		var $form_name="animation_prototype_motion_prototype_form";
		$FORM_CONFIG[$form_name]=[
				{"_label_name":"_name","_field_name":"_name","_edit_field":"text"}
				,
				{"_label_name":"_property","_field_name":"_property","_edit_field":"text"}
				,
				{"_label_name":"_start_value","_field_name":"_start_value","_edit_field":"text"}
				,
				{"_label_name":"_close_value","_field_name":"_close_value","_edit_field":"text"}
				,
				{"_label_name":"_unit","_field_name":"_unit","_edit_field":"text"}
				,
				{"_label_name":"_step_value","_field_name":"_step_value","_edit_field":"text"}
				,
				{"_label_name":"_accelerat","_field_name":"_accelerat","_edit_field":"text"}
				,
				{"_label_name":"_revert","_field_name":"_revert","_edit_field":"text"}
				,
				{"_label_name":"_millisec","_field_name":"_millisec","_edit_field":"text"}
			];
		$animation_prototype_motion_prototype_editor_node.appendChild(createForm($form_name));
		
		// Buttons.
		var $animation_prototype_motion_prototype_editor_buttons=document.createElement("div");
		// Remove button.
		var $animation_prototype_motion_prototype_editor_remove_button=document.createElement("button");
		$animation_prototype_motion_prototype_editor_remove_button.innerHTML=getResourceName("remove_animation_motion");
		Pennyutils.setClassName($animation_prototype_motion_prototype_editor_remove_button, "l tc button");
		Pennyutils.events.addEventHandler($animation_prototype_motion_prototype_editor_remove_button, "click", removeAnimationMotionItem);
		$animation_prototype_motion_prototype_editor_buttons.appendChild($animation_prototype_motion_prototype_editor_remove_button);
		// Apply button.
		var $animation_prototype_motion_prototype_editor_apply_button=document.createElement("button");
		$animation_prototype_motion_prototype_editor_apply_button.innerHTML=getResourceName("apply_animation_motion");
		Pennyutils.setClassName($animation_prototype_motion_prototype_editor_apply_button, "r tc button");
		Pennyutils.events.addEventHandler($animation_prototype_motion_prototype_editor_apply_button, "click", applyAnimationMotion);
		$animation_prototype_motion_prototype_editor_buttons.appendChild($animation_prototype_motion_prototype_editor_apply_button);
		// Clear float.
		var $animation_prototype_motion_prototype_editor_clear_button=document.createElement("div");
		Pennyutils.setClassName($animation_prototype_motion_prototype_editor_clear_button, "c");
		$animation_prototype_motion_prototype_editor_buttons.appendChild($animation_prototype_motion_prototype_editor_clear_button);
		
		$animation_prototype_motion_prototype_editor_node.appendChild($animation_prototype_motion_prototype_editor_buttons);
		
		$animations_pannel.appendChild($animation_prototype_motion_prototype_editor_node);
		
		// Append to window.
		$ANIMATIONS_WIN.childNodes[1].appendChild($animations_pannel);
		Pennyutils.setCssStyle($ANIMATIONS_WIN, "z-index:1000000;");
		
		// Append to body.
		document.body.appendChild($ANIMATIONS_WIN);
	}
	
	$("__animations_input__").value=$input_field_id;
	
	setAnimationItems();
	
	showWindow($ANIMATIONS_WIN);
}
function setAnimationItems () {
	// Init dom.
	$("animations").innerHTML="";
	$("animation_prototype_motions").innerHTML="";
	
	// Init animations values.
//	var $animations_value=$($input_field_id).value;
	var $animations_value=$($TYPESET_PROTOTYPE_FORM_NAME+"."+"_animations").value;
	var $animations=null;
	if ($animations_value) {
		try {
			$animations=JSON.parse($animations_value);
		} catch ($err) {
			
		}
	}
	if (null==$animations) {
		$animations=[];
	}
	
	// Init animations item.
	var $animations_items_button;
	if ($animations.length>0) {
		for (var $index=0; $index<$animations.length; $index++) {
			$animations_items_button=getAnimationItemButton($index, $animations[$index]);
			Pennyutils.setClassName($animations_items_button, "l p5");
			$("animations").appendChild($animations_items_button);
		}
		modifyAnimationItem(0);
		Pennyutils.showObject("animation_prototype_editor");
		Pennyutils.showObject("animation_prototype_motions");
	} else {
		modifyAnimationItem(null);
		Pennyutils.hideObject("animation_prototype_editor");
		Pennyutils.hideObject("animation_prototype_motions");
	}
	$animations_items_button=document.createElement("div");
	Pennyutils.setClassName($animations_items_button, "l p5");
	
	var $animations_items_text_button=document.createElement("div");
	$animations_items_text_button.innerHTML="+";
	Pennyutils.setClassName($animations_items_text_button, "b1d f12 p5");
	Pennyutils.events.addEventHandler($animations_items_text_button, "click", selectAddAnimation);
	$animations_items_button.appendChild($animations_items_text_button);
	
	$("animations").appendChild($animations_items_button);
	
	$animations_items_button=document.createElement("div");
	Pennyutils.setClassName($animations_items_button, "c");
	$("animations").appendChild($animations_items_button);
}
function getAnimationItemButton ($animations_item_index, $animations) {
	var $animations_item=document.createElement("div");
	Pennyutils.setIdName($animations_item, "animations_item_"+$animations_item_index);
	Pennyutils.events.addEventHandler($animations_item, "click", new Function("modifyAnimationItem('"+$animations_item_index+"')"));
	
	var $animations_input=document.createElement("input");
	Pennyutils.setIdName($animations_input, "animations_input_"+$animations_item_index);
	Pennyutils.setClassName($animations_input, "dn");
	$animations_input.value=JSON.stringify($animations);	// Object to json text.
	$animations_item.appendChild($animations_input);
	
	var $animations_name_item=document.createElement("div");
	$animations_name_item.appendChild(document.createTextNode(($animations_item_index+1)+"."+$animations["name"]));
	Pennyutils.setClassName($animations_name_item, "b1d f12 p5");
	$animations_item.appendChild($animations_name_item);
	
	// var $modify_animations_button=document.createElement("div");
	// $modify_animations_button.innerHTML=" ";
	// Pennyutils.setClassName($modify_animations_button, " l icon_modify");
	// Pennyutils.events.addEventHandler($modify_animations_button, "click", new Function("modifyAnimationItem('"+$animations_item_index+"')"));
	// $animations_item.appendChild($modify_animations_button);
	
	// var $animations_clear=document.createElement("div");
	// Pennyutils.setClassName($animations_clear, " c");
	// $animations_item.appendChild($animations_clear);
	
	return $animations_item;
}
function removeAnimationItem () {
	var $animations=[];
	for (var $index=0; $index<$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"].length; $index++) {
		if ($index==$ANIMATION_ITEM_INDEX) {
			continue;
		}
		$animations[$animations.length]=$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$index];
	}
	$ANIMATION_ITEM_INDEX=null;
	$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]=$animations;
	
//	$($TYPESET_PROTOTYPE_FORM_NAME+"."+"_animations").value=JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]);
	setInputValue($TYPESET_PROTOTYPE_FORM_NAME+"."+"_animations", JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]));
	
	setAnimationItems();
}
var $ANIMATION_ITEM_INDEX=null;
function modifyAnimationItem ($animations_item_index) {
	$ANIMATION_ITEM_INDEX=$animations_item_index;
	// Init json.
	var $json;
	if (null==$ANIMATION_ITEM_INDEX) {
		$json=[];
	} else {
		$json=$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX];
	}
	// Set input value.
	var $form_name="animation_prototype_form";
	for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
		setInputValue(
				$form_name+"."+$FORM_CONFIG[$form_name][$field_index]["_field_name"]
				,
				null==$animations_item_index
				?
				undefined==$FORM_CONFIG[$form_name][$field_index]["_default_value"]?"":$FORM_CONFIG[$form_name][$field_index]["_default_value"]
				:
				undefined==$json[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]?"":$json[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]
			);
	}
	// Set animation item motions.
	setAnimationItemMotions();
}

var $ANIMATIONS_TYPE_WIN=null;
function selectAddAnimation () {
	if (null==$ANIMATIONS_TYPE_WIN) {
		var $ANIMATION_TYPES=[
				["A00","A10"]
				,
				
				["B000","B010","B020","B030"]
				,
				["B040","B050","B060","B070"]
				,
				["B101","B111","B121","B131"]
				,
				["B141","B151","B161","B171"]
				,
	
				["C000","C010","C020","C030"]
				,
				["C040","C050","C060","C070"]
				,
				["C101","C111","C121","C131"]
				,
				["C141","C151","C161","C171"]
				,
				
				["D000","D100","D001","D101"]
			];
		var $animations_type_node=document.createElement("div");
		var $animations_type_row_node;
		var $animations_type_col_node;
		for (var $row_index=0; $row_index<$ANIMATION_TYPES.length; $row_index++) {
			$animations_type_row_node=document.createElement("div");
			for (var $col_index=0; $col_index<$ANIMATION_TYPES[$row_index].length; $col_index++) {
				$animations_type_col_node=document.createElement("div");
				$animations_type_col_node.innerHTML=getResourceName($ANIMATION_TYPES[$row_index][$col_index]);
				Pennyutils.setClassName($animations_type_col_node, "l cp p5 m5 b1d");
				Pennyutils.events.addEventHandler($animations_type_col_node, "click", new Function("insertAnimation('"+$ANIMATION_TYPES[$row_index][$col_index]+"');Pennyutils.hideObject($ANIMATIONS_TYPE_WIN);"));
				$animations_type_row_node.appendChild($animations_type_col_node);
			}
			$animations_type_col_node=document.createElement("div");
			Pennyutils.setClassName($animations_type_col_node, "c");
			$animations_type_row_node.appendChild($animations_type_col_node);
			
			$animations_type_node.appendChild($animations_type_row_node);
		}
		
		$ANIMATIONS_TYPE_WIN=createWindow();
		
		$ANIMATIONS_TYPE_WIN.childNodes[1].appendChild($animations_type_node);
		Pennyutils.setCssStyle($ANIMATIONS_TYPE_WIN, "z-index:1000000;");
		
		document.body.appendChild($ANIMATIONS_TYPE_WIN);
	}
	
	showWindow($ANIMATIONS_TYPE_WIN);
}
//function insertAnimation ($type, $direction, $tone, $index) {
function insertAnimation ($type, $index) {
	
	function getSiteMotion ($site, $order) {
		var $_name;
		var $_property;
		var $_start_value;
		var $_close_value;
		
		if (1==$site) {	// Right outer.
			$_name="right_outer";
			$_property="left";
			$_start_value="parent_width+this_width";
			$_close_value="this_left";
		} else if (2==$site) {	// Bottom outer.
			$_name="bottom_outer";
			$_property="top";
			$_start_value="parent_height+this_height";
			$_close_value="this_top";
		} else if (3==$site) {	// Left outer.
			$_name="left_outer";
			$_property="left";
			$_start_value="0-this_width";
			$_close_value="this_left";
		} else if (4==$site) {	// Top inner.
			$_name="top_inner"
			$_property="top";
			$_start_value="0";
			$_close_value="this_top";
		} else if (5==$site) {	// Right inner.
			$_name="right_inner";
			$_property="left";
			$_start_value="parent_width-this_width";
			$_close_value="this_left";
		} else if (6==$site) {	// Bottom inner.
			$_name="buttom_inner"
			$_property="top";
			$_start_value="parent_height-this_height";
			$_close_value="this_top";
		} else if (7==$site) {	// Left inner.
			$_name="left_inner";
			$_property="left";
			$_start_value="0";
			$_close_value="this_left";
		} else {	// Top outer.
			$_name="top_outer";
			$_property="top";
			$_start_value="0-this_height";
			$_close_value="this_top";
		}
		
		return {
				_name:getResourceName($_name),
				_property:$_property, 
				_start_value:$order?$_close_value:$_start_value, 
				_close_value:$order?$_start_value:$_close_value, 
				_unit:"px", 
				_step_value:"Math.abs(("+$_start_value+")-("+$_close_value+"))/10", 
				_accelerat:true, 
				_revert:true, 
				_millisec:50
			};
	}
	
	var $animation={name:getResourceName($type), loop:1, timeout:0, motion:[]};
	var $is_alpha=parseInt($type.charAt(1));
	if (!isNaN($is_alpha)) {
		$animation["motion"][$animation["motion"].length]={
				_name:getResourceName("opacity"),
				_property:"opacity", 
				_start_value:$is_alpha?0:100, 
				_close_value:$is_alpha?100:0, 
				_unit:"", 
				_step_value:10, 
				_accelerat:true, 
				_revert:true, 
				_millisec:50
			};
	}
	
	if ("B"==$type.charAt(0)) {
		var $site=parseInt($type.charAt(2));
		$animation["motion"][$animation["motion"].length]=getSiteMotion($site, parseInt($type.charAt(3)));
	}
	
	if ("C"==$type.charAt(0)) {
		var $site=parseInt($type.charAt(2));
		$animation["motion"][$animation["motion"].length]=getSiteMotion($site, parseInt($type.charAt(3)));
		$animation["motion"][$animation["motion"].length]=getSiteMotion($site==7?0:$site+1, parseInt($type.charAt(3)));
	}
	
	if ("D"==$type.charAt(0)) {
		$animation["motion"][$animation["motion"].length]={
				_name:getResourceName("left"),
				_property:"left", 
				_start_value:parseInt($type.charAt(3))?"this_left+this_width/2":"this_left", 
				_close_value:parseInt($type.charAt(3))?"this_left":"this_left+this_width/2", 
				_unit:"px", 
				_step_value:5, 
				_accelerat:true, 
				_revert:true, 
				_millisec:50
			};
		$animation["motion"][$animation["motion"].length]={
				_name:getResourceName("width"),
				_property:"width", 
				_start_value:parseInt($type.charAt(3))?0:"this_width", 
				_close_value:parseInt($type.charAt(3))?"this_width":0, 
				_unit:"px", 
				_step_value:10, 
				_accelerat:true, 
				_revert:true, 
				_millisec:50
			};
		$animation["motion"][$animation["motion"].length]={
				_name:getResourceName("top"),
				_property:"top", 
				_start_value:parseInt($type.charAt(3))?"this_top+this_height/2":"this_top", 
				_close_value:parseInt($type.charAt(3))?"this_top":"this_top+this_height/2", 
				_unit:"px", 
				_step_value:5, 
				_accelerat:true, 
				_revert:true, 
				_millisec:50
			};
		$animation["motion"][$animation["motion"].length]={
				_name:getResourceName("height"),
				_property:"height", 
				_start_value:parseInt($type.charAt(3))?0:"this_height", 
				_close_value:parseInt($type.charAt(3))?"this_height":0, 
				_unit:"px", 
				_step_value:10, 
				_accelerat:true, 
				_revert:true, 
				_millisec:50
			};
	}
	
	// var $animation;
	// if ("A"==$type.indexOf(0)) {
		// $animation={
			// loop:1,
			// timeout:0,
			// motion:
				// [
					// {
						// _property:"opacity", 
						// _start_value:$type.indexOf(1)==0?0:100, 
						// _close_value:$type.indexOf(1)==0?100:0, 
						// _unit:"", 
						// _step_value:10, 
						// _accelerat:$type.indexOf(3)?true:false;, 
						// _revert:true, 
						// _millisec:50
					// }
				// ]
			// };
	// }
// 	
	// if ("B"==$type.indexOf(0)) {
		// var $_property;
		// var $_start_value;
		// var $_close_value;
// 		
		// if (1==$type.indexOf(2)) {	// Right outer.
			// $_property="left";
			// $_start_value="parent_left+parent_width+this_width";
			// $_close_value="this_left";
		// } else if (2==$type.indexOf(2)) {	// Top outer.
			// $_property="top";
			// $_start_value="parent_top-this_height";
			// $_close_value="this_top";
		// } else if (3==$type.indexOf(2)) {	// Bottom outer.
			// $_property="top";
			// $_start_value="parent_top+parent_height+this_height";
			// $_close_value="this_top";
		// } else if (4==$type.indexOf(2)) {	// Left inner.
			// $_property="left";
			// $_start_value="parent_left";
			// $_close_value="this_left";
		// } else if (5==$type.indexOf(2)) {	// Right inner.
			// $_property="left";
			// $_start_value="parent_left+parent_width-this_width";
			// $_close_value="this_left";
		// } else if (6==$type.indexOf(2)) {	// Top inner.
			// $_property="top";
			// $_start_value="parent_top";
			// $_close_value="this_top";
		// } else if (7==$type.indexOf(2)) {	// Bottom inner.
			// $_property="top";
			// $_start_value="parent_top+parent_height-this_height";
			// $_close_value="this_top";
		// } else {	// Left outer.
			// $_property="left";
			// $_start_value="parent_left-this_width";
			// $_close_value="this_left";
		// }
// 		
		// $animation={
			// loop:1,
			// timeout:0,
			// motion:
				// [
					// {
						// _property:$_property, 
						// _start_value:$_start_value, 
						// _close_value:$_close_value, 
						// _unit:"", 
						// _step_value:10, 
						// _accelerat:true, 
						// _revert:true, 
						// _millisec:50
					// }
				// ]
			// };
		// if ($type.indexOf(1)) {
			// $animation["motion"][$animation["motion"].length]=
					// {
						// _property:"opacity", 
						// _start_value:0, 
						// _close_value:100, 
						// _unit:"", 
						// _step_value:10, 
						// _accelerat:true, 
						// _revert:true, 
						// _millisec:50
					// }
				// ;
		// }
	// }
// 	
	// if ("d"==$type.indexOf(0)) {
		// $animation={
			// loop:1,
			// timeout:0,
			// motion:
				// [
					// {
						// _property:"left", 
						// _start_value:100, 
						// _close_value:0, 
						// _unit:"", 
						// _step_value:10, 
						// _accelerat:true, 
						// _revert:true, 
						// _millisec:50
					// }
				// ]
			// };
		// if ($type.indexOf(1)) {
			// $animation["motion"][$animation["motion"].length]=
					// {
						// _property:"opacity", 
						// _start_value:0, 
						// _close_value:100, 
						// _unit:"", 
						// _step_value:10, 
						// _accelerat:true, 
						// _revert:true, 
						// _millisec:50
					// }
				// ;
		// }
	// }
	
	if (undefined==$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"] || null==$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]) {
		$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]=new Array;
	}
	
	// Insert to animations.
	var $animations=$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"];
	$index=undefined==$index?$animations.length:$index;
	if ($index>=$animations.length) {	// Add to last.
		$animations[$animations.length]=$animation;
		$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]=$animations;
	} else {	// Insert to before.
		var $new_animations=[];
		for (var $animations_index=0; $animations_index<$animations.length; $animations_index++) {
			if ($index==$animations_index) {
				$new_animations[$new_animations.length]=$animations[$animations];
			} else {
				$new_animations[$new_animations.length]=$animation;
			}
		}
		$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]=$new_animations;
	}
//	$($TYPESET_PROTOTYPE_FORM_NAME+"."+"_animations").value=JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]);
	setInputValue($TYPESET_PROTOTYPE_FORM_NAME+"."+"_animations", JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]));
	setAnimationItems();
}


// Animation motions
function setAnimationItemMotions () {
	$("animation_prototype_motions").innerHTML="";
	
	var $json;
	if (null==$ANIMATION_ITEM_INDEX) {
		$json=[];
	} else {
		$json=$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX];
	}
	
	var $animations_item_list=document.createElement("div");
	var $animations_item_node;
	var $animations_item_text_node;
	if (undefined==$json || undefined==$json["motion"] || 0==$json["motion"].length) {
		modifyAnimationItemMotion(null);
	} else {
		for (var $animation_motion_index=0; $animation_motion_index<$json["motion"].length; $animation_motion_index++) {
			$animations_item_node=document.createElement("div");
			Pennyutils.setClassName($animations_item_node, "l p5");
			
			$animations_item_text_node=document.createElement("div");
			$animations_item_text_node.innerHTML=($animation_motion_index+1)+"."+$json["motion"][$animation_motion_index]["_name"];
			Pennyutils.setClassName($animations_item_text_node, "p5 b1d tc");
			Pennyutils.events.addEventHandler($animations_item_text_node, "click", new Function("modifyAnimationItemMotion('"+$animation_motion_index+"')"));
			$animations_item_node.appendChild($animations_item_text_node);
			
			$animations_item_list.appendChild($animations_item_node);
		}
		// // Set first animation item motion.
		// modifyAnimationItemMotion(0);
		// Pennyutils.showObject("animation_prototype_motion_prototype_editor");
	}
	Pennyutils.hideObject("animation_prototype_motion_prototype_editor");
	
	$animations_item_node=document.createElement("div");
	Pennyutils.setClassName($animations_item_node, "l p5");
	
	$animations_item_text_node=document.createElement("div");
	$animations_item_text_node.innerHTML="+";
	Pennyutils.events.addEventHandler($animations_item_text_node, "click", insertAnimationItemMotion);
	Pennyutils.setClassName($animations_item_text_node, "p5 b1d tc");
	$animations_item_node.appendChild($animations_item_text_node);
	
	$animations_item_list.appendChild($animations_item_node);
	
	$animations_item_node=document.createElement("div");
	Pennyutils.setClassName($animations_item_node, "c");
	$animations_item_list.appendChild($animations_item_node);
	
	$("animation_prototype_motions").appendChild($animations_item_list);
}
var $ANIMATION_STEP_INDEX=null;
function modifyAnimationItemMotion ($animation_motion_index) {
	$ANIMATION_STEP_INDEX=$animation_motion_index;
	
	var $form_name="animation_prototype_motion_prototype_form";
	
	for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
		setInputValue(
				$form_name+"."+$FORM_CONFIG[$form_name][$field_index]["_field_name"]
				,
				null==$animation_motion_index
				?
				undefined==$FORM_CONFIG[$form_name][$field_index]["_default_value"]?"":$FORM_CONFIG[$form_name][$field_index]["_default_value"]
				:
				undefined==$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"][$ANIMATION_STEP_INDEX][$FORM_CONFIG[$form_name][$field_index]["_field_name"]]?"":$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"][$ANIMATION_STEP_INDEX][$FORM_CONFIG[$form_name][$field_index]["_field_name"]]
			);
	}
	
	if (null!=$animation_motion_index) {
		Pennyutils.showObject("animation_prototype_motion_prototype_editor");
	}
}
function removeAnimationMotionItem () {
	$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"]
	
	var $animation_motions=[];
	for (var $motion_index=0; $motion_index<$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"].length; $motion_index++) {
		if ($motion_index==$ANIMATION_STEP_INDEX) {
			continue;
		}
		$animation_motions[$animation_motions.length]=$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"][$motion_index];
	}
	$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"]=$animation_motion;
	
//	$($TYPESET_PROTOTYPE_FORM_NAME+"."+"_animations").value=JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]);
//	setInputValue($TYPESET_PROTOTYPE_FORM_NAME+"."+"_animations", JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]));

	setAnimationItemMotions();
}
function applyAnimationMotion () {
	// Update memery value.
	var $form_name="animation_prototype_motion_prototype_form";
	var $animation_values=new Object;
	for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
		$animation_values[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]=$($form_name+"."+$FORM_CONFIG[$form_name][$field_index]["_field_name"]).value;
	}
	$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"][$ANIMATION_STEP_INDEX]=$animation_values;
	// // Update input value.
	// $($("__animations_input__").value).value=JSON.stringify($MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]);
	
	Pennyutils.hideObject("animation_prototype_motion_prototype_editor");
}
function insertAnimationItemMotion ($index) {
	var $form_name="animation_prototype_motion_prototype_form";
	var $animation_values=new Object;
	for (var $field_index=0; $field_index<$FORM_CONFIG[$form_name].length; $field_index++) {
		$animation_values[$FORM_CONFIG[$form_name][$field_index]["_field_name"]]=undefined==$FORM_CONFIG[$form_name][$field_index]["_default_value"]?"":$FORM_CONFIG[$form_name][$field_index]["_default_value"];
	}
	
	var $motion=$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"][$ANIMATION_ITEM_INDEX]["motion"];
	$index=undefined==$index?$motion.length:$index;
	if ($index>=$motion.length) {	// Add to last.
		$motion[$motion.length]=$animation_values;
		$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]["motion"]=$motion;
	} else {	// Insert to before.
		var $new_motion=[];
		for (var $motion_index=0; $motion_index<$motion.length; $motion_index++) {
			if ($index==$motion_index) {
				$new_motion[$new_motion.length]=$motion[$motion];
			} else {
				$new_motion[$new_motion.length]=$animation_values;
			}
		}
		$MAGAZINES[$MAGAZINE_NAME][$PAGE_INDEX][$TYPESET_INDEX]["_animations"]["motion"]=$new_motion;
	}
	
	setAnimationItemMotions();	
}