/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/

/***
Pennyutils Object.
***/
var Pennyutils=new Object;

Pennyutils.setCssStyle = function () {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	$element.setAttribute("style", arguments[1]);
	$element.style.cssText=arguments[1];
};
Pennyutils.getCssStyle = function() {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	return $element.currentStyle?$element.currentStyle[arguments[1]]:document.defaultView.getComputedStyle($element,null).getPropertyValue($property);
};
Pennyutils.replaceCssStyle = function () {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	var $css_text_buffer=new Array;
	
	var $replace_css_key_array=arguments[1].toLowerCase().split(" ");
	var $replace_css_key_obj=new Object;
	for (var $id=0; $id<$replace_css_key_array.length; $id++) {
		if ($replace_css_key_array[$id]) {
			$replace_css_key_obj[$replace_css_key_array[$id].toLowerCase()]=$replace_css_key_array[$id];
		}
	}
	
	var $css_text_array=$element.style.cssText.split(";");
	var $css_value_array;
	for (var $id=0; $id<$css_text_array.length; $id++) {
		if ($css_text_array[$id].trim().length>0) {
			if ($replace_css_key_obj[$css_text_array[$id].split(":")[0].trim().toLowerCase()]) {
			} else {
				$css_text_buffer[$css_text_buffer.length]=$css_text_array[$id];
			}
		}
	}
	$css_text_buffer[$css_text_buffer.length]=arguments[2];
	$element.style.cssText=$css_text_buffer.join(";");
	$element.setAttribute("style", $element.style.cssText);
};

Pennyutils.setClassName = function () {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	if(document.uniqueID) {	// IE.
		if (document.documentMode==undefined) {	// IE6
			$element.setAttribute("className", arguments[1]);
		}
		if (document.documentMode==7) {	// IE7
			$element.setAttribute("className", arguments[1]);
		}
		$element.setAttribute("class", arguments[1]);
	} else {
		$element.setAttribute("class", arguments[1]);
	}
};
Pennyutils.getClassName = function () {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	if(document.uniqueID) {	// IE.
		if (document.documentMode==undefined) {	// IE6
			return $element.getAttribute("className");
		}
		if (document.documentMode==7) {	// IE7
			return $element.getAttribute("className");
		}
		return $element.getAttribute("class");
	} else {
		return $element.getAttribute("class");
	}
};
Pennyutils.replaceClassName = function () {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	
	var $class_names=new Array;
	
	// replace class name.
	var $repace_class_names=arguments[1].split(" ");
	var $repace_class_names_obj=new Object;
//	for (var $id in $repace_class_names) {
	for (var $id=0; $id<$repace_class_names.length; $id++) {
		if ($repace_class_names[$id]) {
			$repace_class_names_obj[$repace_class_names[$id]]=$repace_class_names[$id];
		}
	}
	
	// old class name.
	var $old_class_names=Pennyutils.getClassName($element).split(" ");
//	for (var $id in $old_class_names) {
	for (var $id=0; $id<$old_class_names.length; $id++) {
		if ($repace_class_names_obj[$old_class_names[$id]]) {
			
		} else if ($old_class_names[$id]) {
			$class_names[$class_names.length]=$old_class_names[$id];
		}
	}
	
	// new class name.
	var $new_class_names=arguments[2].split(" ");
	var $new_class_names_obj=new Object;
//	for (var $id in $new_class_names) {
	for (var $id=0; $id<$new_class_names.length; $id++) {
		if ($new_class_names[$id]) {
			$class_names[$class_names.length]=$new_class_names[$id];
		}
	}
	
	// 
	Pennyutils.setClassName($element, $class_names.join(" "));
};

Pennyutils.setIdName = function ($element, $id_name) {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	if(document.uniqueID) {		// IE.
		$element.id=$id_name;
	} else {
		$element.setAttribute("id", $id_name);
	}
};
Pennyutils.getIdName = function ($element) {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	if(document.uniqueID) {		// IE.
		return $element.id;
	} else {
		return $element.getAttribute("id");
	}
};

Pennyutils.showHideObject = function () {
	$obj="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$obj) return;
	if ("visible"==$obj.style.visibility) {
		Pennyutils.hideObject($obj);
	} else {
		Pennyutils.showObject($obj, arguments[1], arguments[2], arguments[3]);
	}
}

Pennyutils.showObject = function () {
	$obj="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$obj) return;
	$obj.style.visibility="visible";
	Pennyutils.replaceCssStyle($obj, "display", "display:inline");
	if (arguments[1]) {	// position.
/*
	var s = "";
	s+="\n document.body.clientWidth :"+ document.body.clientWidth ;
	s+="\n document.body.clientHeight :"+ document.body.clientHeight ;
	s+="\n document.documentElement.clientWidth :"+document.documentElement.clientWidth;
	s+="\n document.documentElement.clientHeight :"+document.documentElement.clientHeight;
	s+="\n document.body.offsetWidth :"+ document.body.offsetWidth ;
	s+="\n document.body.offsetHeight :"+ document.body.offsetHeight ;
	s+="\n document.body.scrollWidth :"+ document.body.scrollWidth ;
	s+="\n document.body.scrollHeight :"+ document.body.scrollHeight ;
	s+="\n document.body.scrollTop :"+ document.body.scrollTop ;
	s+="\n document.documentElement.scrollTop :"+ document.documentElement.scrollTop ;
	s+="\n document.body.scrollLeft :"+ document.body.scrollLeft ;
	s+="\n window.screenTop :"+ window.screenTop ;
	s+="\n window.screenLeft :"+ window.screenLeft ;
	s+="\n window.screen.height :"+ window.screen.height ;
	s+="\n window.screen.width :"+ window.screen.width ;
	s+="\n window.screen.availHeight :"+ window.screen.availHeight ;
	s+="\n window.screen.availWidth :"+ window.screen.availWidth ;
	s+="\n window.screen.colorDepth :"+ window.screen.colorDepth ;
	s+="\n window.screen.deviceXDPI :"+ window.screen.deviceXDPI ;
	alert(s);
*/
		$obj.style.left=arguments[2]?arguments[2]:(document.documentElement.clientWidth>$obj.offsetWidth?document.documentElement.clientWidth-$obj.offsetWidth:0)/2+"px";
		$obj.style.top=arguments[3]?arguments[3]:(document.documentElement.clientHeight>$obj.offsetHeight?document.documentElement.clientHeight-$obj.offsetHeight:0)/2+document.documentElement.scrollTop+document.body.scrollTop+"px";
	}
};

Pennyutils.hideObject = function () {
	$obj="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$obj) return;
//	$obj.setAttribute("style", "display:none");
//	$obj.style.cssText = "display:none";
	$obj.style.visibility="hidden";
	Pennyutils.replaceCssStyle($obj, "display", "display:none");
};

Pennyutils.showObjectSite = function () {
	$obj="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	$obj.style.left=arguments[1]?arguments[1]:(document.documentElement.clientWidth>$obj.offsetWidth?document.documentElement.clientWidth-$obj.offsetWidth:0)/2+"px";
	$obj.style.top=arguments[2]?arguments[2]:(document.documentElement.clientHeight>$obj.offsetHeight?document.documentElement.clientHeight-$obj.offsetHeight:0)/2+document.documentElement.scrollTop+document.body.scrollTop+"px";
};

// [0] reference
// [1] target
Pennyutils.relateSite = function () {
	if (arguments[0]) {
		var $src_object="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
		var $select_pos_x=0;
		var $select_pos_y=0;
		while ($src_object!=null) {
			$select_pos_x+=$src_object.offsetLeft;
			$select_pos_y+=$src_object.offsetTop;
			$src_object=$src_object.offsetParent;
		}
		/*
		var $parent_pos_x = 0;
		var $parent_pos_y = 0;
		$parent_obj = arguments[2]?"string"==typeof arguments[2]?document.getElementById(arguments[2]):arguments[2]:null;
		if ($parent_obj) {
			$src_object = $parent_obj;
			while ($src_object!=null) {
				$parent_pos_x += $src_object.offsetLeft;
				$parent_pos_y += $src_object.offsetTop;
//				alert($src_object.id);
				$src_object = $src_object.offsetParent;
			}
		}
		
		var $select_pos_x = $(arguments[0]).offsetLeft;
		var $select_pos_y = $(arguments[0]).offsetTop;
		*/
//		alert($(arguments[0]).clientWidth+":"+$select_pos_x+":"+$parent_pos_x+":"+$select_pos_y+":"+$parent_pos_y);
//		alert($select_pos_y+":"+$(arguments[0]).clientHeight+"::"+($select_pos_y-$(arguments[0]).clientHeight));
//		alert($(arguments[0]).clientWidth+":"+$select_pos_x+":"+$select_pos_y);
//		alert($(arguments[0]).clientWidth+":"+$(arguments[0]).offsetWidth+":"+$(arguments[0]).width+"  "+$(arguments[0]).clientHeight+":"+$(arguments[0]).offsetHeight+":"+$(arguments[0]).height);
		$target_object="string"==typeof arguments[1]?document.getElementById(arguments[1]):arguments[1];
		$_left=($src_object.offsetWidth+$select_pos_x);
//		alert($select_pos_x+":"+$select_pos_y);
		if (arguments[2]) {
			switch (arguments[2]) {
				case 'l':
					$_left=0;
					break
				case 'c':
					$_left=(document.documentElement.clientWidth>$target_object.offsetWidth?document.documentElement.clientWidth-$target_object.offsetWidth:0)/2;
					break
				case 'r':
					$_left=(document.documentElement.clientWidth>$target_object.offsetWidth?document.documentElement.clientWidth-$target_object.offsetWidth:0);
					break
				case 'tl':
					$_left=$select_pos_x;
					break
				case 'tc':
					$_left=($src_object.offsetWidth-$target_object.offsetWidth+$select_pos_x)/2;
					break
				case 'tr':
					$_left=$src_object.offsetWidth-$target_object.offsetWidth+$select_pos_x;
					break
				case 'ttl':
					$_left=$select_pos_x-$target_object.offsetWidth;
					break
				case 'ttr':
					$_left=$select_pos_x+$target_object.offsetWidth;
					break
				default:
					$_left+=arguments[2];
			}
		}
		$_top=$select_pos_y;
		if (arguments[3]) {
			switch (arguments[3]) {
				case 't':
					$_top=0;
					break
				case 'c':
					$_top=(document.documentElement.clientHeight>$target_object.offsetHeight?document.documentElement.clientHeight-$target_object.offsetHeight:0)/2+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'b':
					$_top=(document.documentElement.clientHeight>$target_object.offsetHeight?document.documentElement.clientHeight-$target_object.offsetHeight:0)+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'tt':
					$_top=$select_pos_y;
					break
				case 'tc':
					$_top=($target_object.offsetHeight-$target_object.offsetHeight+$select_pos_y)/2+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'tb':
					$_top=$target_object.offsetHeight-$target_object.offsetHeight+$select_pos_y+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'ttt':
					$_top=$select_pos_y+document.documentElement.scrollTop+document.body.scrollTop-$target_object.offsetHeight;
					break
				case 'ttb':
					$_top=$target_object.offsetHeight+$select_pos_y+document.documentElement.scrollTop+document.body.scrollTop;
					break
				default:
					$_top+=arguments[3];
			}
		}
		$target_object.style.left=$_left+"px";
		$target_object.style.top=$_top+"px";
	}
}

/* Pennyutils.events. begin. */
Pennyutils.events=new Object;
Pennyutils.events.addEventHandler = function ($target, $event_type, $function_handler) {
	if ($target.addEventListener) {
		$target.addEventListener($event_type, $function_handler, false);
	} else if ($target.attachEvent) {
		$target.attachEvent("on"+$event_type, $function_handler);
	} else {
		$target["on"+$event_type]=$function_handler;
	}
};

Pennyutils.events.removeEventHandler = function ($target, $event_type, $function_handler) {
	if ($target.removeEventListener) {
		$target.removeEventListener($event_type, $function_handler, false);
	} else if ($target.detachEvent) {
		$target.detachEvent("on"+$event_type, $function_handler);
	} else { 
		$target["on"+$event_type] = null;
	}
};

Pennyutils.events.formatEvent = function ($event) {
	if ("undefined"==typeof $event.charCode) {
		$event.charCode=("keypress"==$event.type)?$event.keyCode:0;
		$event.isChar=($event.charCode>0);
	}
	
	if ($event.srcElement && !$event.target) {
		$event.eventPhase=2;
		$event.pageX=$event.clientX+document.body.scrollLeft;
		$event.pageY=$event.clientY+document.body.scrollTop;
		
		if (!$event.preventDefault) {
			$event.preventDefault = function () {
				this.returnValue=false;
			};
		}
		
		if ("mouseout"==$event.type) {
			$event.relatedTarget=$event.toElement;
		} else if ("mouseover"==$event.type) {
			$event.relatedTarget=$event.fromElement;
		}
		
		if (!$event.stopPropagation) {
			$event.stopPropagation = function () {
				this.cancelBubble=true;
			};
		}
		
		$event.target=$event.srcElement;
		$event.time=(new Date).getTime();
	}
	
	return $event;
};

Pennyutils.events.getEvent = function() {
	if (window.event) {
		return this.formatEvent(window.event);
	} else {
		return Pennyutils.events.getEvent.caller.arguments[0];
	}
};
/* Pennyutils.events. end. */

/* Drag. begin */
Pennyutils.drag=new Object;
Pennyutils.drag=function(){
	this.$element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	this.ondragstart=arguments[1]?arguments[1]:function(){};
	this.ondraging=arguments[2]?arguments[2]:function(){};
	this.ondragend=arguments[3]?arguments[3]:function(){};
	this.$event;
	this.offsetX=0;
	this.offsetY=0;
	
	var $drag_obj=this;	
	var mouseDown=function(){
		$drag_obj.$event=arguments[0]||window.event;
		$drag_obj.offsetX=typeof $drag_obj.$element.layerX=="undefined"?$drag_obj.$element.offsetLeft:$drag_obj.$element.layerX;
		$drag_obj.offsetY=typeof $drag_obj.$element.layerY=="undefined"?$drag_obj.$element.offsetTop:$drag_obj.$element.layerY;
		$drag_obj.offsetX=$drag_obj.$event.clientX-parseInt($drag_obj.offsetX);
		$drag_obj.offsetY=$drag_obj.$event.clientY-parseInt($drag_obj.offsetY);
		this.offsetX=$drag_obj.offsetX;
		this.offsetY=$drag_obj.offsetY;
		
		$drag_obj.attachEventHandlers();	// Drag start.
		$drag_obj.ondragstart.call(this);
	};
	
	if (this.$element.addEventListener) {
		this.$element.addEventListener("mousedown", mouseDown, false);
	} else if(this.$element.attachEvent) {
		this.$element.attachEvent("onmousedown", mouseDown);
	} else {
		this.$element["onmousedown"]=mouseDown;
	}
};
Pennyutils.drag.prototype.attachEventHandlers=function(){
	var $drag_obj=this;
	this.mouseMove=function(){	// start drag
		var $event=arguments[0]||window.event;
		// ...
		var $left=$event.clientX-$drag_obj.offsetX;
		var $top=$event.clientY-$drag_obj.offsetY;
		if (document.uniqueID) {
			$drag_obj.$element.style.left=$left;
			$drag_obj.$element.style.top=$top;
		} else {
			$drag_obj.$element.style.setProperty("left", $left+"px", "");
			$drag_obj.$element.style.setProperty("top", $top+"px", "");
		}
		// ...
		$drag_obj.ondraging.call(this);
	};
	$drag_obj.mouseUp=function(){
		$drag_obj.detachEventHandlers();	// stop drag/drop
		// ... 
		$drag_obj.ondragend.call(this);
	};
	if (document.addEventListener) {
		document.addEventListener("mousemove", this.mouseMove, false);
		document.addEventListener("mouseup", this.mouseUp, false);
	} else if (document.body.attachEvent) {
		document.body.attachEvent("onmousemove", this.mouseMove);
		document.body.attachEvent("onmouseup", this.mouseUp);
	} else {
		document.body["onmousemove"]=this.mouseMove;
		document.body["onmouseup"]=this.mouseUp;
	}
};
Pennyutils.drag.prototype.detachEventHandlers=function(){
	if (document.removeEventListener) {
		document.removeEventListener("mousemove", this.mouseMove, false);
		document.removeEventListener("mouseup", this.mouseUp, false);
	} else if(document.body.detachEvent) {
		document.body.detachEvent("onmousemove", this.mouseMove);
		document.body.detachEvent("onmouseup", this.mouseUp);
	} else {
		document.body["onmousemove"]=null;
		document.body["onmouseup"]=null;
	}
};
/* Drag. end */


Pennyutils.showPhoto = function () {
	var $show_img=arguments[0];
//	$show_img.style.opacity=0;
//	$show_img.style.filter='alpha(opacity=0)';
	$show_img.src.onload=new Function("Pennyutils.setOpacity(this, 100, 5, 1)");
	$show_img.src=arguments[1];
	var $img_width=arguments[2];
	var $img_height=arguments[3];
	Pennyutils.resizeImage($show_img, $img_width, $img_height);
	$show_img.style.left=($img_width-$show_img.width)/2+"px";
	$show_img.style.top=($img_height-$show_img.height)/2+"px";
	$show_img.style.opacity=0;
	$show_img.style.filter='alpha(opacity=0)';
	Pennyutils.setOpacity($show_img, 100, 10, 1);
}
Pennyutils.setOpacity = function () {
	$element=arguments[0];
	$alpha=arguments[1];
	$target=arguments[2];
	$destine=arguments[3]?arguments[3]:1;
	$element='object'==typeof $element?$element:$($element);
	var $opacity=$element.style.opacity||Pennyutils.getCssStyle($element, 'opacity');
//	var $destine=$alpha>$opacity*100?1:-1;
	window.status=$opacity+","+arguments[1]+","+arguments[2]+","+$destine;
	$element.style.opacity=$opacity;
//	clearInterval($element.$interval_id);
	$element.$interval_id=setInterval(function(){Pennyutils.tween($element, $alpha, $destine, $target)}, 20);
}
Pennyutils.tween = function () {
	$element=arguments[0];
	$alpha=arguments[1];
	$destine=arguments[2];
	$target=arguments[3];
	$element='object'==typeof $element?$element:$($element);
	var $opacity=Math.round($element.style.opacity*100);
//	window.status=$destine+"::"+$opacity+">="+$alpha;
	if ($destine<0) {
		if($opacity<=$alpha){
			clearInterval($element.$interval_id);
		}
	} else {
		if($opacity>=$alpha){
			clearInterval($element.$interval_id);
		}
	}
	
	var $now_opacity=$opacity+Math.ceil(Math.abs($alpha-$opacity)/$target)*$destine;
	$element.style.opacity=$now_opacity/100;
	$element.style.filter='alpha(opacity='+$now_opacity+')';
//	window.status=$now_opacity;
};

Pennyutils.removeAllChild = function () {
	var $element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$element) return;
	for (var index=$element.childNodes.length-1;index>=0;index--) {
		$element.removeChild($element.childNodes[index]);
	}
};