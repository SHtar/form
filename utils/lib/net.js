﻿/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
var net=new Object();
net.READY_STATE_UNINITIALIZED=0;
net.READY_STATE_LOADING=1;
net.READY_STATE_LOADED=2;
net.READY_STATE_INTERACTIVE=3;
net.READY_STATE_COMPLETE=4;
net.ContentLoader=function (url, onload, params, synch, alerterror, onerror, method, content_type) {
	net.currentLoader=this;
	
	this.url=url;
	this.onload=onload;
	this.params=params;
	this.synch=synch;
	this.method=method;
	this.content_type=content_type;
	this.onerror=(onerror)?onerror:this.defaultError;
	this.alerterror=alerterror?true:false;
	
	this.request=null;
	
//	this.loadXMLDoc(url, method, params, content_type);
	this.loadXMLDoc();
}
//net.ContentLoader.prototype.loadXMLDoc=function (url, method, params, content_type) {
net.ContentLoader.prototype.loadXMLDoc=function () {
	if (!this.method) {
		this.method="POST";
	} else {
		this.method=this.method.toUpperCase();
	}
	if (!this.content_type && "POST"==this.method){
		this.content_type="application/x-www-form-urlencoded";
	}
	if (window.XMLHttpRequest) {
		this.request=new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		this.request=new ActiveXObject("Microsoft.XMLHTTP");
		if (!this.req) {
			this.request=new ActiveXObject("Msxml2.XMLHTTP");
		}
	}
	if (this.params) {
		if (this.params.indexOf("&")!=0) {
			this.params="&"+this.params;
		}
	} else {
		this.params="";
	}
	if (this.request){
		try{
			var loader=this;
			
			this.request.open(this.method, this.url, this.synch);
			this.request.onreadystatechange=function () {
				net.ContentLoader.onReadyState.call(loader);
			};
			if (this.content_type) {
				this.request.setRequestHeader("Content-Type", this.content_type);
			}
			this.request.send(this.params);
			if (isFirefox) {
				net.ContentLoader.onReadyState.call(loader);
			}
		} catch ($err) {
//			this.onerror.call(this);
			this.onerror.call(loader);
		}
	}
}
net.ContentLoader.onReadyState=function () {
	if (net.READY_STATE_COMPLETE==this.request.readyState) {
		if (200===this.request.status || 0===this.request.status) {
			this.onload.call(this);
		} else {
			this.onerror.call(this);
		}
	}
}
net.ContentLoader.prototype.defaultError=function(){
	window.status="Error fetching data!";
//	alert(this.alerterror);
	if (this.alerterror) {
		alert("net.ContentLoader:Error fetching data!"+"\n\nreadyState:"+this.request.readyState+"\nstatus: "+this.request.status+"\nheaders: "+this.request.getAllResponseHeaders()+"\nText: "+this.request.responseText);
	}
}