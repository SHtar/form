/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/

function $() {
	return "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
}

function $$() {
	$node = arguments[1]||document;
	return $node.getElementsByTagName(arguments[0]);
}

String.prototype.lengths = function() {
	return this.replace('/[^\x00-\xff]/ig',"**").length; 
}
String.prototype.trim = function() {
	var str = this,
	whitespace = ' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000';
	for (var i=0, len=str.length; i<len; i++) {
		if (whitespace.indexOf(str.charAt(i))===-1) {
			str = str.substring(i);
			break;
		}
	}
	for (i=str.length-1; i>=0; i--) {
		if (whitespace.indexOf(str.charAt(i))===-1) {
			str = str.substring(0, i+1);
			break;
		}
	}
	
	return whitespace.indexOf(str.charAt(0))===-1 ? str : '';
}

function StringBuffer () {
	this.__strings__ = new Array;
}

StringBuffer.prototype.append = function () {
	this.__strings__.push(arguments[0]);
}

StringBuffer.prototype.toString = function () {
	return this.__strings__.join("");
}

/***
Pennyutils Object.
***/
var Pennyutils = new Object;

Pennyutils.generateDate = function ($obj) {
	var $dateText = $obj.value;
	
	if ($dateText.length==8) {	// String length 8 character.
		if ($dateText.indexOf("-") < 1) {	// String text have "-"
			$obj.value = $dateText.substr(0, 4)+"-"+$dateText.substr(4, 2)+"-"+$dateText.substr(6, 2)
		}
	}
}

Pennyutils.isDate = function ($obj) {
	if ($obj==null) {
		return;
	}
	var $value = $obj.value;
	if (($value == null)||($value.length == 0)||($value == "0000-00-00")) {
		return false;
	}
	var r = $value.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
	if(r==null) {
		obj.select();
		obj.focus();
		return false;
	}
	var d = new Date(r[1], r[3]-1, r[4]);
	
	if (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]) {
		return true;
	} else {
		obj.select();
		obj.focus();
		return false;
	}
}


Pennyutils.getFitSize = function () {
	$img=arguments[0];
	$set_width=arguments[1]>0?arguments[1]:1;
	$set_height=arguments[2]>0?arguments[2]:1;
	$model=arguments[3];
	
	$width_rate = $set_width / $img.width;
	$height_rate = $set_height / $img.height;
	
	if ($model==1) {
		$rate = $width_rate > $height_rate ? $width_rate : $height_rate; // get max rate.
	} else if ($model==2) {
		$rate = $width_rate;
	} else if ($model==3) {
		$rate = $height_rate;
	} else {
		$rate = $width_rate < $height_rate ? $width_rate : $height_rate; // get min rate.
	}
	
	$re_width = $img.width * $rate;
	$re_height = $img.height * $rate;
	
	var $size = new Object();
	
	$size.width = $re_width;
	$size.height = $re_height;
	
	return $size;
}

Pennyutils.resizeImage = function () {
	$img=arguments[0];
	$set_width=arguments[1]>0?arguments[1]:1;
	$set_height=arguments[2]>0?arguments[2]:1;
	$model=arguments[3];
//	alert($img.src);
	var $size = Pennyutils.getFitSize($img, $set_width, $set_height, $model);
	
	$img.width = $size.width==0?$set_width:$size.width;
	$img.height = $size.height==0?$set_height:$size.height;
	
	return $size;
}

Pennyutils.setCssStyle = function () {
	$item = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$item) return;
	$item.setAttribute("style", arguments[1]);
	$item.style.cssText = arguments[1];
}

Pennyutils.setClassName = function () {
	$item = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$item) return;
	if(document.uniqueID) {		// IE.
		if (document.documentMode==undefined) {	// IE6
			$item.setAttribute("className", arguments[1]);
		}
		if (document.documentMode==7) {	// IE7
			$item.setAttribute("className", arguments[1]);
		}
		$item.setAttribute("class", arguments[1]);
	} else {
		$item.setAttribute("class", arguments[1]);
	}
}

Pennyutils.getClassName = function ($item) {
	if (!$item) return;
	if(document.uniqueID) {		// IE.
		if (document.documentMode==undefined) {	// IE6
			return $item.getAttribute("className");
		}
		if (document.documentMode==7) {	// IE7
			return $item.getAttribute("className");
		}
		return $item.getAttribute("class");
	} else {
		return $item.getAttribute("class");
	}
}

Pennyutils.setIdName = function ($item, $id_name) {
	if (!$item) return;
	if(document.uniqueID) {		// IE.
		$item.id = $id_name;
	} else {
		$item.setAttribute("id", $id_name);
	}
}

Pennyutils.getIdName = function ($item) {
	if (!$item) return;
	if(document.uniqueID) {		// IE.
		return $item.id;
	} else {
		return $item.getAttribute("id");
	}
}

Pennyutils.noDocSelect = function () {
	if(document.uniqueID) {		// IE.
		return false;
	} else  {
		$("body").style["-moz-user-select"]="none";
	}
}

var $selectObj;
var $timeoutID;
Pennyutils.hideSelectObject=function() {
	$timeoutID=setTimeout("if ($selectObj!='"+arguments[0]+"') {Pennyutils.hideObject('"+arguments[0]+"_list');clearTimeout($timeoutID);}", 300);
}
Pennyutils.showSelectObject=function() {
	Pennyutils.showObject(arguments[0]+"_list");
	Pennyutils.relateSite(arguments[0]+"_list", arguments[0]+"_field", 'l', 'b');
}
Pennyutils.keySelectObject=function() {
//	alert(arguments[1].keyCode);
	if (arguments[1].keyCode==40){	// Down
		var $cssText = $(arguments[0]+"_list").style.cssText.toLowerCase();
		if ($cssText.indexOf("display:none;")>=0||$cssText.indexOf("display: none;")>=0) {
			Pennyutils.showObject(arguments[0]+"_list");
			Pennyutils.relateSite(arguments[0]+"_list", arguments[0]+"_field", 'l', 'b');
		} else {
			arguments[1].keyCode=9;
		}
	} else if (arguments[1].keyCode==27){	// Esc
		Pennyutils.hideObject(arguments[0]+"_list");
	}
}
Pennyutils.keySelectOption=function() {
//	alert(arguments[2].keyCode);
	if(arguments[2].keyCode==13){	// Enter
		Pennyutils.input.setSelectRadioValue(arguments[0], arguments[1]);
		Pennyutils.hideObject(arguments[0]+"_select_list");
		$(arguments[0]+"_select_field").focus();
	} else if(arguments[2].keyCode==32){	// Space
		Pennyutils.input.setSelectRadioValue(arguments[0], arguments[1]);
	} else if (arguments[2].keyCode==27){	// Esc
		Pennyutils.hideObject(arguments[0]+"_select_list");
		$(arguments[0]+"_select_field").focus();
	} else if (arguments[2].keyCode==38){	// Up
		arguments[2].keyCode=9;
	} else if (arguments[1].keyCode==40){	// Down
		arguments[2].keyCode=9;
	}
}

Pennyutils.showHideSelectObject = function ($input_obj, $select_obj, $parent_obj) {
//	alert($input_obj+":"+$select_obj);
	$input_obj = "string"==typeof $input_obj?document.getElementById($input_obj):$input_obj;
	$select_obj = "string"==typeof $select_obj?document.getElementById($select_obj):$select_obj;
	if (!$input_obj||!$select_obj) return;
	

//	var s = "";
//	s+="\n $input_obj.clientWidth :"+ $input_obj.clientWidth ;
//	s+="\n $input_obj.clientHeight :"+ $input_obj.clientHeight ;
//	s+="\n $input_obj.offsetWidth :"+ $input_obj.offsetWidth ;
//	s+="\n $input_obj.offsetHeight :"+ $input_obj.offsetHeight ;
//	s+="\n $input_obj.scrollWidth :"+ $input_obj.scrollWidth ;
//	s+="\n $input_obj.scrollHeight :"+ $input_obj.scrollHeight ;
//	s+="\n $input_obj.scrollTop :"+ $input_obj.scrollTop ;
//	s+="\n $input_obj.scrollLeft :"+ $input_obj.scrollLeft ;
	
//	s+="\n $input_obj.offsetLeft :"+ $input_obj.offsetLeft;
//	s+="\n $input_obj.offsetTop :"+ $input_obj.offsetTop;
	
//	s+="\n $select_obj.offsetLeft :"+ $select_obj.offsetLeft;
//	s+="\n $select_obj.offsetTop :"+ $select_obj.offsetTop;
	
//	s+="\n $parent_obj.offsetLeft :"+ $parent_obj.offsetLeft;
//	s+="\n $parent_obj.offsetTop :"+ $parent_obj.offsetTop;

//	alert(s);

	
	var $cssText = $select_obj.style.cssText.toLowerCase();
	if ($cssText.indexOf("display:none;")>=0||$cssText.indexOf("display: none;")>=0) {
//		Pennyutils.replaceCSSText($obj, "display", "display:inline");
//		$select_obj.style.visibility="visible";
//		$input_obj.offsetHeight;
//		$input_obj.offsetWidth;
		var $temp_element;
		var $select_pos_x = 0;
		var $select_pos_y = 0;
		$temp_element = $input_obj;
		while ($temp_element!=null) {
			$select_pos_x += $temp_element.offsetLeft;
			$select_pos_y += $temp_element.offsetTop;
//			alert($temp_element.id+"\n"+$select_pos_x+";"+$select_pos_y+","+$temp_element.offsetLeft+","+$temp_element.offsetTop);
//			alert($temp_element.id);
			$temp_element = $temp_element.offsetParent;
		}
//		alert($select_pos_x+","+$select_pos_y);
		var $parent_pos_x = 0;
		var $parent_pos_y = 0;
		$parent_obj = "string"==typeof $parent_obj?document.getElementById($parent_obj):$parent_obj;
		if ($parent_obj) {
			$temp_element = $parent_obj;
			while ($temp_element!=null) {
				$parent_pos_x += $temp_element.offsetLeft;
				$parent_pos_y += $temp_element.offsetTop;
//				alert($temp_element.id);
				$temp_element = $temp_element.offsetParent;
			}
		}
//		alert($parent_pos_x+","+$parent_pos_y);
/* Needless count offsetParent.offsetLeft/offsetTop!!!
		var $select_pos_x = $input_obj.offsetLeft;
		var $select_pos_y = $input_obj.offsetTop;
*/
//		alert($select_pos_x+","+$select_pos_y+":"+$parent_pos_x+","+$parent_pos_y);

//		$select_obj.style.left = ($select_pos_x-$parent_pos_x)+"px";
//		$select_obj.style.top = ($select_pos_y-$parent_pos_y) + $input_obj.offsetHeight+"px";

		$select_obj.style.left = ($select_pos_x-$parent_pos_x)+"px";
		$select_obj.style.top = ($select_pos_y-$parent_pos_y) + $input_obj.offsetHeight+"px";

//		$select_obj.style.left = "0px";
//		$select_obj.style.top = "0px";

		$select_obj.style.display = "block";
		Pennyutils.replaceCSSText($select_obj, "display", "display:inline");
		$select_obj.style.visibility="visible";
//		window.status = $select_pos_x+":"+$select_pos_y+":"+$parent_pos_x+":"+$parent_pos_y;
		if ($select_obj.offsetWidth < $input_obj.offsetWidth) {	// must visible then $select_obj cat give offsetWidth.
			$select_obj.style.width = $input_obj.offsetWidth;
		}
	} else {
		Pennyutils.replaceCSSText($select_obj, "display", "display:none");
		$select_obj.style.visibility="hidden";
	}
}

Pennyutils.showHideUnderObject = function () {
	var $input_obj = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	var $select_obj = "string"==typeof arguments[1]?document.getElementById(arguments[1]):arguments[1];
	var $offset_x = Number(arguments[2])?Number(arguments[2]):0;
	var $offset_y = Number(arguments[3])?Number(arguments[3]):0;
	if (!$input_obj||!$select_obj) return;
	
	var $cssText = $select_obj.style.cssText.toLowerCase();
	if ($cssText.indexOf("display:none;")>=0||$cssText.indexOf("display: none;")>=0) {
		var $temp_element;
		var $select_pos_x = 0;
		var $select_pos_y = 0;
		$temp_element = $input_obj;
		while ($temp_element!=null) {
			$select_pos_x += $temp_element.offsetLeft;
			$select_pos_y += $temp_element.offsetTop;
			$temp_element = $temp_element.offsetParent;
		}
		
		$select_obj.style.left = ($select_pos_x+$offset_x)+"px";
		$select_obj.style.top = ($select_pos_y+$offset_y+$input_obj.offsetHeight)+"px";
		
		$select_obj.style.display = "block";
		Pennyutils.replaceCSSText($select_obj, "display", "display:inline");
		$select_obj.style.visibility="visible";

//		if ($select_obj.offsetWidth < $input_obj.offsetWidth) {	// must visible then $select_obj cat give offsetWidth.
//			$select_obj.style.width = $input_obj.offsetWidth;
//		}
	} else {
		Pennyutils.replaceCSSText($select_obj, "display", "display:none");
		$select_obj.style.visibility="hidden";
	}
}

Pennyutils.showHideObject = function ($obj) {
	$obj = "string"==typeof $obj?document.getElementById($obj):$obj;
	if (!$obj) return;
	var $cssText = $obj.style.cssText.toLowerCase();
//alert($cssText+"\n"+$cssText.indexOf("display:none;"));
	if ($cssText.indexOf("display:none;")>=0||$cssText.indexOf("display: none;")>=0) {
		Pennyutils.replaceCSSText($obj, "display", "display:inline");
		$obj.style.visibility="visible";
//alert("inline:"+$obj.style.cssText.toLowerCase());
	} else {
		Pennyutils.replaceCSSText($obj, "display", "display:none");
		$obj.style.visibility="hidden";
//alert("none:"+$obj.style.cssText.toLowerCase());
	}
}

Pennyutils.showObject = function () {
	$obj = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$obj) return;
	$obj.style.visibility="visible";
	Pennyutils.replaceCSSText($obj, "display", "display:inline");
	if (arguments[1]) {	// position.
/*
	var s = "";
	s+="\n document.body.clientWidth :"+ document.body.clientWidth ;
	s+="\n document.body.clientHeight :"+ document.body.clientHeight ;
	s+="\n document.documentElement.clientWidth :"+document.documentElement.clientWidth;
	s+="\n document.documentElement.clientHeight :"+document.documentElement.clientHeight;
	s+="\n document.body.offsetWidth :"+ document.body.offsetWidth ;
	s+="\n document.body.offsetHeight :"+ document.body.offsetHeight ;
	s+="\n document.body.scrollWidth :"+ document.body.scrollWidth ;
	s+="\n document.body.scrollHeight :"+ document.body.scrollHeight ;
	s+="\n document.body.scrollTop :"+ document.body.scrollTop ;
	s+="\n document.documentElement.scrollTop :"+ document.documentElement.scrollTop ;
	s+="\n document.body.scrollLeft :"+ document.body.scrollLeft ;
	s+="\n window.screenTop :"+ window.screenTop ;
	s+="\n window.screenLeft :"+ window.screenLeft ;
	s+="\n window.screen.height :"+ window.screen.height ;
	s+="\n window.screen.width :"+ window.screen.width ;
	s+="\n window.screen.availHeight :"+ window.screen.availHeight ;
	s+="\n window.screen.availWidth :"+ window.screen.availWidth ;
	s+="\n window.screen.colorDepth :"+ window.screen.colorDepth ;
	s+="\n window.screen.deviceXDPI :"+ window.screen.deviceXDPI ;
	alert(s);
*/
		$obj.style.left=arguments[2]?arguments[2]:(document.documentElement.clientWidth>$obj.offsetWidth?document.documentElement.clientWidth-$obj.offsetWidth:0)/2+"px";
		$obj.style.top=arguments[3]?arguments[3]:(document.documentElement.clientHeight>$obj.offsetHeight?document.documentElement.clientHeight-$obj.offsetHeight:0)/2+document.documentElement.scrollTop+document.body.scrollTop+"px";
	}
}

Pennyutils.showObjectSite = function () {
	$obj = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	$obj.style.left=arguments[1]?arguments[1]:(document.documentElement.clientWidth>$obj.offsetWidth?document.documentElement.clientWidth-$obj.offsetWidth:0)/2+"px";
	$obj.style.top=arguments[2]?arguments[2]:(document.documentElement.clientHeight>$obj.offsetHeight?document.documentElement.clientHeight-$obj.offsetHeight:0)/2+document.documentElement.scrollTop+document.body.scrollTop+"px";
}

Pennyutils.hideObject = function () {
	$obj = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$obj) return;
//	$obj.setAttribute("style", "display:none");
//	$obj.style.cssText = "display:none";
	$obj.style.visibility="hidden";
	Pennyutils.replaceCSSText($obj, "display", "display:none");
}

// [0] reference
// [1] target
Pennyutils.relateSite = function () {
	if (arguments[0]) {
		var $temp_element;
		var $select_pos_x = 0;
		var $select_pos_y = 0;
		$temp_element = $(arguments[0]);
		while ($temp_element!=null) {
			$select_pos_x += $temp_element.offsetLeft;
			$select_pos_y += $temp_element.offsetTop;
			$temp_element = $temp_element.offsetParent;
		}
		/*
		var $parent_pos_x = 0;
		var $parent_pos_y = 0;
		$parent_obj = arguments[2]?"string"==typeof arguments[2]?document.getElementById(arguments[2]):arguments[2]:null;
		if ($parent_obj) {
			$temp_element = $parent_obj;
			while ($temp_element!=null) {
				$parent_pos_x += $temp_element.offsetLeft;
				$parent_pos_y += $temp_element.offsetTop;
//				alert($temp_element.id);
				$temp_element = $temp_element.offsetParent;
			}
		}
		
		var $select_pos_x = $(arguments[0]).offsetLeft;
		var $select_pos_y = $(arguments[0]).offsetTop;
		*/
//		alert($(arguments[0]).clientWidth+":"+$select_pos_x+":"+$parent_pos_x+":"+$select_pos_y+":"+$parent_pos_y);
//		alert($select_pos_y+":"+$(arguments[0]).clientHeight+"::"+($select_pos_y-$(arguments[0]).clientHeight));
//		alert($(arguments[0]).clientWidth+":"+$select_pos_x+":"+$select_pos_y);
//		alert($(arguments[0]).clientWidth+":"+$(arguments[0]).offsetWidth+":"+$(arguments[0]).width+"  "+$(arguments[0]).clientHeight+":"+$(arguments[0]).offsetHeight+":"+$(arguments[0]).height);
		$obj=$(arguments[1]);
		$_left=($(arguments[0]).offsetWidth+$select_pos_x);
//		alert($select_pos_x+":"+$select_pos_y);
		if (arguments[2]) {
			switch (arguments[2]) {
				case 'l':
					$_left=0;
					break
				case 'c':
					$_left=(document.documentElement.clientWidth>$obj.offsetWidth?document.documentElement.clientWidth-$obj.offsetWidth:0)/2;
					break
				case 'r':
					$_left=(document.documentElement.clientWidth>$obj.offsetWidth?document.documentElement.clientWidth-$obj.offsetWidth:0);
					break
				case 'tl':
					$_left=$select_pos_x;
					break
				case 'tc':
					$_left=($(arguments[0]).offsetWidth-$obj.offsetWidth+$select_pos_x)/2;
					break
				case 'tr':
					$_left=$(arguments[0]).offsetWidth-$obj.offsetWidth+$select_pos_x;
					break
				case 'ttl':
					$_left=$select_pos_x-$obj.offsetWidth;
					break
				case 'ttr':
					$_left=$select_pos_x+$obj.offsetWidth;
					break
				default:
					$_left+=arguments[2];
			}
		}
		$_top=$select_pos_y;
		if (arguments[3]) {
			switch (arguments[3]) {
				case 't':
					$_top=0;
					break
				case 'c':
					$_top=(document.documentElement.clientHeight>$obj.offsetHeight?document.documentElement.clientHeight-$obj.offsetHeight:0)/2+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'b':
					$_top=(document.documentElement.clientHeight>$obj.offsetHeight?document.documentElement.clientHeight-$obj.offsetHeight:0)+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'tt':
					$_top=$select_pos_y;
					break
				case 'tc':
					$_top=($(arguments[0]).offsetHeight-$obj.offsetHeight+$select_pos_y)/2+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'tb':
					$_top=$(arguments[0]).offsetHeight-$obj.offsetHeight+$select_pos_y+document.documentElement.scrollTop+document.body.scrollTop;
					break
				case 'ttt':
					$_top=$select_pos_y+document.documentElement.scrollTop+document.body.scrollTop-$obj.offsetHeight;
					break
				case 'ttb':
					$_top=$(arguments[0]).offsetHeight+$select_pos_y+document.documentElement.scrollTop+document.body.scrollTop;
					break
				default:
					$_top+=arguments[3];
			}
		}
		$(arguments[1]).style.left=$_left+"px";
		$(arguments[1]).style.top=$_top+"px";
	}
}

Pennyutils.replaceCSSText = function () {
	$obj = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$obj) return;
	var $cssText = $obj.style.cssText+"";
	var $cssTextBuffer = new StringBuffer();
	var $cssTextArray = $cssText.split(";");
	var $cssValueArray;
	var $cssKey=arguments[1].toLowerCase();
	for (var $index=0; $index<$cssTextArray.length; $index++) {
		if ($cssTextArray[$index].trim().length>0) {
			$cssValueArray = $cssTextArray[$index].split(":");
			if ($cssKey!=$cssValueArray[0].trim().toLowerCase()) {
				$cssTextBuffer.append($cssValueArray[0]+":"+$cssValueArray[1]+";");
			}
		}
	}
//	$cssTextBuffer.append($cssKey+":"+arguments[2]+";");
	$cssTextBuffer.append(arguments[2]+";");
//	alert($cssTextBuffer.toString());
	$obj.style.cssText=$cssTextBuffer.toString();
}

Pennyutils.showMessage = function () {
	if ($("message_win")) {

	} else {
		var message_win=document.createElement("div");
		message_win.setAttribute("id","message_win");
		
		var textarea=document.createElement("textarea");
		textarea.setAttribute("id","message_win_inner");
		message_win.appendChild(textarea);
		
		var button=document.createElement("input");
		button.type = "button";
		button.accessKey = "w";
		button.value = Pennyutils.lang.close+".W";
		Pennyutils.setClassName(button, "button");
		button.onclick = function() {Pennyutils.hideObject("message_win");}
		message_win.appendChild(button);
		
		document.body.appendChild(message_win);
	}
	
	if (arguments[1]=="1") {	//	Info
		Pennyutils.replaceCSSText($("message_win"), "border", "border:3px solid #66cc00");
	} else if (arguments[1]=="2") {	//	Warn
		Pennyutils.replaceCSSText($("message_win"), "border", "border:3px solid #ffcc00");
	} else {	//	Error\Fatal
		Pennyutils.replaceCSSText($("message_win"), "border", "border:3px solid #990000");
	}
	
	$("message_win_inner").value=arguments[0];
	
	Pennyutils.showObject("message_win", true);
}

Pennyutils.debug = function ($value) {
	if ($("debug_textarea")) {
		if(document.uniqueID) {		// IE.
			$("debug_textarea").appendChild(document.createTextNode($value+"\r"));	// IE.
		} else {
			$("debug_textarea").value+=$value+"\n";	// FF.
		}
		$("debug_textarea").scrollTop = $("debug_textarea").scrollHeight;
	}
}

/* Pennyutils.events. begin. */
Pennyutils.events = new Object;
Pennyutils.events.addEventHandler = function ($target, $event_type, $function_handler) {
	if ($target.addEventListener) {
		$target.addEventListener($event_type, $function_handler, false);
	} else if ($target.attachEvent) {
		$target.attachEvent("on" + $event_type, $function_handler);
	} else {
		$target["on" + $event_type] = $function_handler;
	}
};

Pennyutils.events.removeEventHandler = function ($target, $event_type, $function_handler) {
    if ($target.removeEventListener) {
        $target.removeEventListener($event_type, $function_handler, false);
    } else if ($target.detachEvent) {
        $target.detachEvent("on" + $event_type, $function_handler);
    } else { 
        $target["on" + $event_type] = null;
    }
};

Pennyutils.events.formatEvent = function ($event) {
    if (typeof $event.charCode == "undefined") {
        $event.charCode = ($event.type == "keypress") ? $event.keyCode : 0;
        $event.isChar = ($event.charCode > 0);
    }
    
    if ($event.srcElement && !$event.target) {
        $event.eventPhase = 2;
        $event.pageX = $event.clientX + document.body.scrollLeft;
        $event.pageY = $event.clientY + document.body.scrollTop;
        
        if (!$event.preventDefault) {
                $event.preventDefault = function () {
                    this.returnValue = false;
                };
        }

        if ($event.type == "mouseout") {
            $event.relatedTarget = $event.toElement;
        } else if ($event.type == "mouseover") {
            $event.relatedTarget = $event.fromElement;
        }

        if (!$event.stopPropagation) {
                $event.stopPropagation = function () {
                    this.cancelBubble = true;
                };
        }
        
        $event.target = $event.srcElement;
        $event.time = (new Date).getTime();
    }
    
    return $event;
};

Pennyutils.events.getEvent = function() {
    if (window.event) {
        return this.formatEvent(window.event);
    } else {
        return Pennyutils.events.getEvent.caller.arguments[0];
    }
};
/* Pennyutils.events. end. */

/* Pennyutils.input. begin. */
Pennyutils.input = new Object;

Pennyutils.input.mouOver = function ($id, $type, $index) {
	var $img = document.getElementById($id + "_img_" + $index);
	if ($img.src.indexOf("_o.gif")<0) {
		var $reg=new RegExp(".gif","g");
		$img.src = $img.src.replace($reg, "_o.gif");
	}
}

Pennyutils.input.mouOut = function ($id, $type, $index) {
	var $img = document.getElementById($id + "_img_" + $index);
	if ($img.src.indexOf("_o.gif")) {
		var $reg=new RegExp("_o.gif","g");
		$img.src = $img.src.replace($reg, ".gif");
	}
}

Pennyutils.input.setRadioValue = function ($id, $index) {
	var $input = document.getElementById($id);
	var $img = document.getElementById($id + "_img_" + $index);
	var $reg=new RegExp("img/cms_radio.*.gif","g");
	var $radio_group = document.getElementById($id + "_hide_radio_group_list");
	for ($i=0;$i<$radio_group.length;$i++) {
		$img=document.getElementById($id + "_img_" + $i);
		$img.src = $img.src.replace($reg, "img/cms_radio.gif");
		$radio_group.options[$i].selected=false;
	}
	
	// Selected.
	$img=document.getElementById($id + "_img_" + $index);
	$img.src = $img.src.replace($reg, "img/cms_radio_y.gif");
	$radio_group.options[$index].selected=true;
	$input.value=$radio_group.options[$index].value;
}

Pennyutils.input.setSelectRadioValue = function ($id, $index) {
	var $input = document.getElementById($id);
	var $img = document.getElementById($id + "_img_" + $index);
	var $reg=new RegExp("img/cms_radio.*.gif","g");
	var $radio_group = document.getElementById($id + "_hide_select_list");
	for ($i=0;$i<$radio_group.length;$i++) {
		$img=document.getElementById($id + "_img_" + $i);
		$img.src = $img.src.replace($reg, "img/cms_radio.gif");
		$radio_group.options[$i].selected=false;
	}
	
	// Selected.
	$img=document.getElementById($id + "_img_" + $index);
	$img.src = $img.src.replace($reg, "img/cms_radio_y.gif");
	$radio_group.options[$index].selected=true;
	$input.value=$radio_group.options[$index].value;
//	$($id+"_list_field").value=$radio_group.options[$index].text;
	$($id+"_select_field").value=$radio_group.options[$index].text;
//	$($id+"_list_field").focus();
}

Pennyutils.input.setCheckboxValue = function ($id, $_index) {
	var $checkbox_group=document.getElementById($id+"_hide_checkbox_group_list");
	var $img;
	var $reg=new RegExp("img/cms_checkbox.*.gif","g");
	if ($checkbox_group.options[$_index].selected==true) {
		$img = document.getElementById($id + "_img_" + $_index);
		$img.src = $img.src.replace($reg, "img/cms_checkbox.gif");
		$checkbox_group.options[$_index].selected=false;
	} else {
		$img = document.getElementById($id + "_img_" + $_index);
		$img.src = $img.src.replace($reg, "img/cms_checkbox_y.gif");
		$checkbox_group.options[$_index].selected=true;
	}
	$img=null;
	// Set value & sort value.
	$value = new StringBuffer();
	for ($i=0;$i<$checkbox_group.length;$i++) {
		if ($checkbox_group.options[$i].selected==true) {
			$value.append($checkbox_group.options[$i].value);
			$value.append(",");
		}
	}
	// Cut ','.
	$value=$value.toString();
	$len=$value.length;
	if ($len>0&&$value.charAt($len-1)==',') {
		$value=$value.substr(0, $len-1);
	}
	document.getElementById($id).value = $value.toString();
}

Pennyutils.input.setSelectCheckboxValue = function ($id, $_index) {
	var $checkbox_group=document.getElementById($id+"_hide_checkbox_select_list");
	var $img;
	var $reg=new RegExp("img/cms_checkbox.*.gif","g");
	if ($checkbox_group.options[$_index].selected==true) {
		$img = document.getElementById($id + "_img_" + $_index);
		$img.src = $img.src.replace($reg, "img/cms_checkbox.gif");
		$checkbox_group.options[$_index].selected=false;
	} else {
		$img = document.getElementById($id + "_img_" + $_index);
		$img.src = $img.src.replace($reg, "img/cms_checkbox_y.gif");
		$checkbox_group.options[$_index].selected=true;
	}
	$img=null;
	// Set value & sort value.
	$value = new StringBuffer();
	$text = new StringBuffer();
	for ($i=0;$i<$checkbox_group.length;$i++) {
		if ($checkbox_group.options[$i].selected==true) {
			$value.append($checkbox_group.options[$i].value);
			$value.append(",");
			
			$text.append($checkbox_group.options[$i].text);
			$text.append(",");
		}
	}
	// Cut ','.
	$text=$text.toString();
	$len=$text.length;
	if ($len>0&&$text.charAt($len-1)==',') {
		$text=$text.substr(0, $len-1);
	}
	$($id+"_select_field").value = $text.toString();
	$value=$value.toString();
	$len=$value.length;
	if ($len>0&&$value.charAt($len-1)==',') {
		$value=$value.substr(0, $len-1);
	}
	$($id).value = $value.toString();
	$($id+"_select_field").focus();
}

Pennyutils.input.setInputValue = function ($id, $value) {
	var $input = document.getElementById($id);
	$input.value = $value;
	
	var $checkbox_group = document.getElementById($id + "_hide_checkbox_group_list");
	if ($checkbox_group) {	// checkbox_group.
		var $reg=new RegExp("img/cms_checkbox.*.gif","g");
		for ($i=0;$i<$checkbox_group.length;$i++) {
			$img=document.getElementById($id + "_img_" + $i);
			$img.src=$img.src.replace($reg, "img/cms_checkbox.gif");
			$checkbox_group.options[$i].selected=false;
		}
		var $value = $value.length>0?$value.split(","):"";
		for ($index=0;$index<$value.length;$index++) {
			for ($i=0;$i<$checkbox_group.length;$i++) {
				if ($checkbox_group.options[$i].value==$value[$index]) {
					$checkbox_group.options[$i].selected=true;
					$img=document.getElementById($id + "_img_" + $i);
					$img.src=$img.src.replace($reg, "img/cms_checkbox_y.gif");
					break;
				}
			}
		}
	}
	var $radio_group = document.getElementById($id + "_hide_radio_group_list");
	if ($radio_group) {	// radio_group.
		var $reg=new RegExp("img/cms_radio.*.gif","g");
		var $value=$value.length>0?$value:"";
		for ($i=0;$i<$radio_group.length;$i++) {
			$img=document.getElementById($id + "_img_" + $i);
			$img.src=$img.src.replace($reg, "img/cms_radio.gif");
			$radio_group.options[$i].selected=false;
			if ($radio_group.options[$i].value==$value) {
				$radio_group.options[$i].selected=true;
				$img=document.getElementById($id + "_img_" + $i);
				$img.src=$img.src.replace($reg, "img/cms_radio_y.gif");
			}
		}
	}
	var $combo_box = document.getElementById($id + "_combo_box_list");
	if ($combo_box) {	// combo_box.
		var $options = $combo_box.getElementsByTagName("option");
		if ($options) {
			for (var $index=0; $index<$options.length; $index++) {
				if ($value==$options[$index].value) {
					var combo_box_field = document.getElementById($id + "_combo_box_field");
					if (combo_box_field) {
						combo_box_field.value = $options[$index].text;
						$options[$index].selected = true;
					}
				} else {
					$options[$index].selected = false;
				}
			}
		}
	}
	var $select=document.getElementById($id+"_hide_checkbox_select_list");
	if ($select) {	// select.
		var $reg=new RegExp("img/cms_checkbox.*.gif","g");
		for ($i=0;$i<$select.length;$i++) {
			$img=document.getElementById($id + "_img_" + $i);
			$img.src=$img.src.replace($reg, "img/cms_checkbox.gif");
			$select.options[$i].selected=false;
		}
		var $text = new StringBuffer();
		var $value = $value.length>0?$value.split(","):"";
		for ($index=0;$index<$value.length;$index++) {
			for ($i=0;$i<$select.length;$i++) {
				if ($select.options[$i].value==$value[$index]) {
					$text.append($select.options[$i].text);
					$select.options[$i].selected=true;
					$img=document.getElementById($id + "_img_" + $i);
					$img.src=$img.src.replace($reg, "img/cms_checkbox_y.gif");
					break;
				}
			}
			if ($index<($value.length-1)) {
				$text.append(",");
			}
		}
		$($id+"_select_field").value = $text.toString();
	}
	var $select=document.getElementById($id+"_hide_select_list");
	if ($select) {	// select.
		var $reg=new RegExp("img/cms_radio.*.gif","g");
		for ($i=0;$i<$select.length;$i++) {
			$img=document.getElementById($id + "_img_" + $i);
			$img.src=$img.src.replace($reg, "img/cms_radio.gif");
			$select.options[$i].selected=false;
		}
		var $text = new StringBuffer();
		var $value = $value.length>0?$value.split(","):"";
		for ($index=0;$index<$value.length;$index++) {
			for ($i=0;$i<$select.length;$i++) {
				if ($select.options[$i].value==$value[$index]) {
					$text.append($select.options[$i].text);
					$select.options[$i].selected=true;
					$img=document.getElementById($id + "_img_" + $i);
					$img.src=$img.src.replace($reg, "img/cms_radio_y.gif");
					break;
				}
			}
			if ($index<($value.length-1)) {
				$text.append(",");
			}
		}
		$($id+"_select_field").value = $text.toString();
	}
	var $list = document.getElementById($id + "_hide_radio_list");
	if ($list) {	// list.
		var $reg=new RegExp("img/cms_radio.*.gif","g");
		var $value=$value.length>0?$value:"";
		for ($i=0;$i<$list.length;$i++) {
			$img=document.getElementById($id + "_img_" + $i);
			$img.src=$img.src.replace($reg, "img/cms_radio.gif");
			$list.options[$i].selected=false;
			if ($list.options[$i].value==$value) {
				var list_field = document.getElementById($id + "_list_field");
				if (list_field) {
					list_field.value = $list.options[$i].text;
				}
				$list.options[$i].selected=true;
				$img=document.getElementById($id + "_img_" + $i);
				$img.src=$img.src.replace($reg, "img/cms_radio_y.gif");
			}
		}
	}
	var $ym = document.getElementById($id + "_ym");
	if ($ym) {	// ym.
		if ($value!=null&&$value.length==7) {
			$($id+"_year").value=$value.substr(0, 4);
			$($id+"_month").value=$value.substr(5, 2);
		} else {
			$($id+"_year").value='';
			$($id+"_month").value='';
		}
	}
	var $date = document.getElementById($id + "_date");
	if ($date) {	// date.
		if ($value!=null&&$value.length==10) {
			$($id+"_year").value=$value.substr(0, 4);
			$($id+"_month").value=$value.substr(5, 2);
			$($id+"_date").value=$value.substr(8, 2);
		} else {
			$($id+"_year").value='';
			$($id+"_month").value='';
			$($id+"_date").value='';
		}
	}
	var $time = document.getElementById($id + "_hour");
	if ($time) {	// time.
		if ($value!=null&&($value.length==5||$value.length==8)) {
			if ($value.length==5) {
				$($id+"_hour").value=$value.substr(0, 2);
				$($id+"_minute").value=$value.substr(3, 2);
			} else if ($value.length==8) {
				$($id+"_hour").value=$value.substr(0, 2);
				$($id+"_minute").value=$value.substr(3, 2);
				$($id+"_second").value=$value.substr(6, 2);
			}
		} else {
			if ($($id+"_hour")){$($id+"_hour").value='';}
			if ($($id+"_minute")){$($id+"_minute").value='';}
			if ($($id+"_second")){$($id+"_second").value='';}
		}
	}
	if ($date&&$time) {	// date time.
		if ($value!=null&&$value.length==19) {
			$($id+"_year").value=$value.substr(0, 4);
			$($id+"_month").value=$value.substr(5, 2);
			$($id+"_date").value=$value.substr(8, 2);
			$($id+"_hour").value=$value.substr(11, 2);
			$($id+"_minute").value=$value.substr(14, 2);
			$($id+"_second").value=$value.substr(17, 2);
		} else {
			$($id+"_year").value='';
			$($id+"_month").value='';
			$($id+"_date").value='';
			$($id+"_hour").value='';
			$($id+"_minute").value='';
			$($id+"_second").value='';
		}
	}
	var $json_result=document.getElementById($id+"_json_result");
	if ($json_result) {	// json_result.
		$json_result.innerHTML="";
		if ($value) {
			var json_result=null;
			try {
				json_result=JSON.parse($value);
			} catch(err) {
				alert("Error connect!\nerr:"+err+"\nerr.message:"+err.message+"\n"+$value);
			}
			if (json_result) {
//				var $structure=document.getElementById($id + "_json_result_structure")
				$table_div=document.createElement("div");
				var $json_result_structure=document.getElementById($id+"_json_result_structure");
				var $head=Array();
				if ($json_result_structure) {
					try {
//						alert($json_result_structure.value);
						var json_result_structure=JSON.parse($json_result_structure.value);
//						alert($json_result_structure.value+"\n"+json_result_structure);
						var $had_div=document.createElement("div");
						var $col=0;
						for (var $t=0; $t<json_result_structure.length; $t++) {
							if ("h"!=json_result_structure[$t][2]) {
								var $til_div=document.createElement("div");
								Pennyutils.setIdName($til_div, 'odt_h_i_'+$col);
								Pennyutils.setClassName($til_div, 'c_i');
								
								var $txt_div=document.createElement("div");
								Pennyutils.setIdName($txt_div, 'odt_h_t_'+$col);
								Pennyutils.setClassName($txt_div, 'l');
								$txt_div.appendChild(document.createTextNode(json_result_structure[$t][1]));
								$til_div.appendChild($txt_div);
								
								var $mov_div=document.createElement("div");
								Pennyutils.setIdName($mov_div, 'odt_h_m_'+$col);
								Pennyutils.setClassName($mov_div, 'c_m r');
								$mov_div.appendChild(document.createTextNode(' '));
								$til_div.appendChild($mov_div);
								
								var $c_div=document.createElement("div");
								Pennyutils.setClassName($c_div, 'c');
								$til_div.appendChild($c_div);
								
								$had_div.appendChild($til_div);
								
								$til_div.addEventListener("mousedown",new Function("handleMD('odt', "+$col+")"),false);
								
								$col++;
							}
							
							$head[$t]=json_result_structure[$t][2];
						}
						var $cle_div=document.createElement("div");
						Pennyutils.setClassName($cle_div, 'c');
						$had_div.appendChild($cle_div);
						
						$table_div.appendChild($had_div);
					} catch(err) {
					}
				}
				
				for (var $r=0; $r<json_result.length; $r++) {
					var $row_div=document.createElement("div");
					var $col=0;
					for (var $c=0; $c<json_result[$r].length; $c++) {
						if ("h"!=$head[$c]) {
							var $col_div=document.createElement("div");
							Pennyutils.setIdName($col_div, 'odt_c_i_'+$col+'_'+$r);
							Pennyutils.setClassName($col_div, 'c_i');
							
							var $txt_div=document.createElement("div");
							Pennyutils.setIdName($txt_div, 'odt_c_t_'+$col+'_'+$r);
							$txt_div.appendChild(document.createTextNode(json_result[$r][$c]));
							$col_div.appendChild($txt_div);
							
							$row_div.appendChild($col_div);
							
							$col++;
						}
					}
					var $cle_div=document.createElement("div");
					Pennyutils.setClassName($cle_div, 'c');
					$row_div.appendChild($cle_div);
					
					$table_div.appendChild($row_div);
				}
				$json_result.appendChild($table_div);
			}
		}
	}
	var $json_record = document.getElementById($id + "_json_record");
	if ($json_record) {	// json_record.
		
	}
}

Pennyutils.input.getInputText = function ($id) {
	var $value = document.getElementById($id).value;
	
	var $checkbox_group = document.getElementById($id + "_hide_checkbox_group_list");
	if ($checkbox_group) {	// checkbox_group.
		var $value = $value.length>0?$value.split(","):"";
		var $text = new StringBuffer();
		for ($i=0;$i<$checkbox_group.length;$i++) {
			for ($index=0;$index<$value.length;$index++) {
				if ($checkbox_group.options[$i].value==$value[$index]) {
					$text.append($checkbox_group.options[$i].text);
					$text.append(" ");
					break;
				}
			}
		}
		return $text.toString();
	}
	var $radio = document.getElementById($id + "_hide_radio_group_list");
	if ($radio) {	// radio.
		for ($i=0;$i<$radio.length;$i++) {
			if ($value==$radio.options[$i].value) {
				return $radio.options[$i].text;
			}
		}
	}
	
	return $value;
}
/* Pennyutils.input. end. */

/* Drag. begin */
Pennyutils.drag = new Object;

Pennyutils.drag = function () {
	this.$element="string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	this.ondragstart=arguments[1]?arguments[1]:this.defaultDragStart;
	this.ondraging=arguments[2]?arguments[2]:this.defaultDraging;
	this.ondragend=arguments[3]?arguments[3]:this.defaultDragEnd;
	this.$event;
	this.offsetX=0;
	this.offsetY=0;
//	var offsetX=$event.offsetX?$event.offsetX:$event.layerX; 
	
	var $drag_obj=this;
	var $mouseDown=function(){
//		$("debug_textarea").value="";
		$drag_obj.$event=arguments[0]||window.event;
//		$("debug_textarea").value+=$drag_obj.$event+"\n";
//		$("debug_textarea").value+=$drag_obj.$element.offsetLeft+":"+$drag_obj.$element.layerX+"\n";
		$drag_obj.offsetX = typeof $drag_obj.$element.layerX=="undefined"?$drag_obj.$element.offsetLeft:$drag_obj.$element.layerX;
		$drag_obj.offsetY = typeof $drag_obj.$element.layerY=="undefined"?$drag_obj.$element.offsetTop:$drag_obj.$element.layerY;
		$drag_obj.offsetX = $drag_obj.$event.clientX - $drag_obj.offsetX;
		$drag_obj.offsetY = $drag_obj.$event.clientY - $drag_obj.offsetY;
//		$("debug_textarea").value+=$drag_obj.offsetX+":"+$drag_obj.offsetY+"\n";
		this.offsetX=$drag_obj.offsetX;
		this.offsetY=$drag_obj.offsetY;
		
		$drag_obj.attachEventHandlers();	// Drag start.
		$drag_obj.ondragstart.call(this);
	};
	if(this.$element.addEventListener){
		this.$element.addEventListener("mousedown",$mouseDown,false);
	}else if(this.$element.attachEvent){
		this.$element.attachEvent("onmousedown",$mouseDown);
	}else{
		throw new Error("Not support this browser!");
	}
}
Pennyutils.drag.prototype.attachEventHandlers=function(){
	var $drag_obj=this;
	this.$mouseMove=function(){	// start drag
		this.$event=arguments[0]||window.event;
		this.offsetX=$drag_obj.offsetX;
		this.offsetY=$drag_obj.offsetY;
		// ... 
		$drag_obj.ondraging.call(this);
	};
	$drag_obj.$mouseUp=function(){
		$drag_obj.detachEventHandlers();	// stop drag/drop
		// ... 
		$drag_obj.ondragend.call(this);
	};
	if(document.addEventListener){
		document.addEventListener("mousemove",this.$mouseMove,false);
		document.addEventListener("mouseup",this.$mouseUp,false);
	}else if(document.body.attachEvent){
		document.body.attachEvent("onmousemove",this.$mouseMove);
		document.body.attachEvent("onmouseup",this.$mouseUp);
	}else{
		throw new Error("Not support this browser!");
	}
}
Pennyutils.drag.prototype.detachEventHandlers=function(){
	if(document.removeEventListener){
		document.removeEventListener("mousemove",this.$mouseMove,false);
		document.removeEventListener("mouseup",this.$mouseUp,false);
	}else if(document.body.detachEvent){
		document.body.detachEvent("onmousemove",this.$mouseMove);
		document.body.detachEvent("onmouseup",this.$mouseUp);
	}else{
		throw new Error("Not support this browser!");
	}
}
Pennyutils.drag.prototype.defaultDragStart=function(){
//	$("debug_textarea").value+="drag start\n";
}
Pennyutils.drag.prototype.defaultDraging=function(){
//	var $event=arguments[0]||window.event;
//	debug_textarea.value+=$event.clientX+":"+this.$event.clientX+"\n";
}
Pennyutils.drag.prototype.defaultDragEnd=function(){
//	$("debug_textarea").value+="drag stop\n";
}
/* Drag. end */

Pennyutils.showPhoto=function () {
	var $show_img=arguments[0];
//	$show_img.style.opacity=0;
//	$show_img.style.filter='alpha(opacity=0)';
	$show_img.src.onload=new Function("Pennyutils.setOpacity(this, 100, 5, 1)");
	$show_img.src=arguments[1];
	var $img_width=arguments[2];
	var $img_height=arguments[3];
	Pennyutils.resizeImage($show_img, $img_width, $img_height);
	$show_img.style.left=($img_width-$show_img.width)/2+"px";
	$show_img.style.top=($img_height-$show_img.height)/2+"px";
	$show_img.style.opacity=0;
	$show_img.style.filter='alpha(opacity=0)';
	Pennyutils.setOpacity($show_img, 100, 10, 1);
}
Pennyutils.setOpacity=function () {
	$element=arguments[0];
	$alpha=arguments[1];
	$target=arguments[2];
	$destine=arguments[3]?arguments[3]:1;
	$element='object'==typeof $element?$element:$($element);
	var $opacity=$element.style.opacity||Pennyutils.getStyleProperty($element, 'opacity');
//	var $destine=$alpha>$opacity*100?1:-1;
	window.status=$opacity+","+arguments[1]+","+arguments[2]+","+$destine;
	$element.style.opacity=$opacity;
//	clearInterval($element.$interval_id);
	$element.$interval_id=setInterval(function(){Pennyutils.tween($element, $alpha, $destine, $target)}, 20);
}
Pennyutils.tween=function () {
	$element=arguments[0];
	$alpha=arguments[1];
	$destine=arguments[2];
	$target=arguments[3];
	$element='object'==typeof $element?$element:$($element);
	var $opacity=Math.round($element.style.opacity*100);
//	window.status=$destine+"::"+$opacity+">="+$alpha;
	if ($destine<0) {
		if($opacity<=$alpha){
			clearInterval($element.$interval_id);
		}
	} else {
		if($opacity>=$alpha){
			clearInterval($element.$interval_id);
		}
	}
	
	var $now_opacity=$opacity+Math.ceil(Math.abs($alpha-$opacity)/$target)*$destine;
	$element.style.opacity=$now_opacity/100;
	$element.style.filter='alpha(opacity='+$now_opacity+')';
//	window.status=$now_opacity;
}
Pennyutils.getStyleProperty=function() {
	$element=arguments[0];
	$property=arguments[1];
	$element=typeof $element=="object"?$element:$($element);
	return $element.currentStyle?$element.currentStyle[$property]:document.defaultView.getComputedStyle($element,null).getPropertyValue($property);
}

Pennyutils.removeAllChildNode=function() {
	$obj = "string"==typeof arguments[0]?document.getElementById(arguments[0]):arguments[0];
	if (!$obj) return;
	var length = $obj.childNodes.length;
	for (var index=length-1;index>=0;index--) {
		$obj.removeChild($obj.childNodes[index]);
	}
}

Pennyutils.repaintPNG=function() {
	png_img=arguments[0];
	var arVersion = navigator.appVersion.split("MSIE");
	var version = parseFloat(arVersion[1]);

	var src_img=new Image();
	src_img.src = png_img.src;
	
	if ((version >= 5.5) && (version < 7) && (document.body.filters)) {
		var img_id = (png_img.id) ? "id='" + png_img.id + "' " : "";
		var img_class = (png_img.className) ? "class='" + png_img.className + "' " : "";
		var img_title = (png_img.title) ? "title='" + png_img.title  + "' " : "title='" + png_img.alt + "' ";
		var img_style = "display:inline-block;" + png_img.style.cssText;
		var strNewHTML = "<span " + img_id + img_class + img_title
				  + " style=\"" + "width:" + src_img.width 
				  + "px; height:" + src_img.height 
				  + "px;" + img_style + ";"
				  + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
				  + "(src=\'" + png_img.src + "\', sizingMethod='scale');\"></span>";
		png_img.outerHTML = strNewHTML;
	}
}

var move_table_name,move_col;
function handleMouseMove () {
	var $event = Pennyutils.events.getEvent();
	var $move = $(move_table_name+"_header_move_"+move_col);
	var $target = $(move_table_name+"_header_item_"+move_col);
	var margin = 0;
	var t_left = 0;
	
	if(document.uniqueID) {
		$move.style.left = $event.clientX;
		var nLt=0;
		var offsetParent = $target;
		while (offsetParent!=null && offsetParent!=document.body) {
			nLt+=offsetParent.offsetLeft;
			if(document.uniqueID){
				parseInt(offsetParent.currentStyle.borderLeftWidth)>0?nLt+=parseInt(offsetParent.currentStyle.borderLeftWidth):"";
			}
			offsetParent=offsetParent.offsetParent;
		}
		t_left = nLt;
	} else {
		$move.style.setProperty("left", $event.clientX+"px", "");
		t_left = $target.offsetLeft;
	}
	
	margin = $event.clientX - t_left;
	
//	set_element_value("test_grid", "t_left:"+t_left+" , margin:"+margin+" , width:"+$target.offsetWidth);
	
	if ((margin-2) <= 0) {
//		_draggable.moveTo(_dropTarget.getLeft(), _dropTarget.getTop());
	}
	
	saw_width=7;
	var total_width=0;
	for (index=0; index<move_col; index++) {
		total_width+=$(move_table_name+"_header_item_"+index).scrollWidth+",";
	}
//	window.status=$(move_table_name+"_grid_table").scrollWidth+","+$(move_table_name+"_grid_table").scrollLeft+","+move_col+":"+total_width;
	if (margin > 0) {
		$(move_table_name+"_header_item_"+move_col).style.width=(margin+saw_width)+"px";
		$(move_table_name+"_header_text_"+move_col).style.width=margin+"px";
		
		if ($("all_batch_id").value.length>0) {
			var id_array = $("all_batch_id").value.split(",");
			for (id in id_array) {
//				alert(move_table_name+"_column_item_"+move_col+"_"+id_array[id]+","+$(move_table_name+"_column_item_"+move_col+"_"+id_array[id]));
				$(move_table_name+"_column_item_"+move_col+"_"+id_array[id]).style.width=(margin+saw_width)+"px";				
			}
		}
	}
}

function handleMouseDown () {
	move_table_name = arguments[0];
	move_col = arguments[1];
	
	var $target = $(move_table_name+"_header_move_"+move_col);
//	alert((move_table_name+"_header_move_"+move_col)+":"+$target);
	Pennyutils.setCssStyle($target, "position:absolute;");

	Pennyutils.events.addEventHandler(document.body, "mousemove", handleMouseMove);
	Pennyutils.events.addEventHandler(document.body, "mouseup", handleMouseUp);
}

function handleMouseUp () {
	var $move = $(move_table_name+"_header_move_"+move_col);
	Pennyutils.setCssStyle($move, "position:static;");
	
	Pennyutils.events.removeEventHandler(document.body, "mousemove", handleMouseMove);
	Pennyutils.events.removeEventHandler(document.body, "mouseup", handleMouseUp);
}

function handleMM () {
	var $event = Pennyutils.events.getEvent();
	var $move = $(move_table_name+"_h_m_"+move_col);
	var $target = $(move_table_name+"_h_i_"+move_col);
	var margin = 0;
	var t_left = 0;
	
	if(document.uniqueID) {
		$move.style.left = $event.clientX;
		var nLt=0;
		var offsetParent = $target;
		while (offsetParent!=null && offsetParent!=document.body) {
			nLt+=offsetParent.offsetLeft;
			if(document.uniqueID){
				parseInt(offsetParent.currentStyle.borderLeftWidth)>0?nLt+=parseInt(offsetParent.currentStyle.borderLeftWidth):"";
			}
			offsetParent=offsetParent.offsetParent;
		}
		t_left = nLt;
	} else {
		$move.style.setProperty("left", $event.clientX+"px", "");
		t_left = $target.offsetLeft;
	}
	
	margin = $event.clientX - t_left;
	
//	set_element_value("test_grid", "t_left:"+t_left+" , margin:"+margin+" , width:"+$target.offsetWidth);
	
	if ((margin-2) <= 0) {
//		_draggable.moveTo(_dropTarget.getLeft(), _dropTarget.getTop());
	}
	
	saw_width=7;
	var total_width=0;
	for (index=0; index<move_col; index++) {
		total_width+=$(move_table_name+"_h_i_"+index).scrollWidth+",";
	}
//	window.status=$(move_table_name+"_grid_table").scrollWidth+","+$(move_table_name+"_grid_table").scrollLeft+","+move_col+":"+total_width;
	if (margin > 0) {
		$(move_table_name+"_h_i_"+move_col).style.width=(margin+saw_width)+"px";
		$(move_table_name+"_h_t_"+move_col).style.width=margin+"px";
		var i=0;
		while (true) {
			if ($(move_table_name+"_c_i_"+move_col+"_"+i)) {
				$(move_table_name+"_c_i_"+move_col+"_"+i).style.width=(margin+saw_width)+"px";
				i++;
			} else {
				break;
			}
		}
	}
}

function handleMD () {
	move_table_name = arguments[0];
	move_col = arguments[1];
	
	var $target = $(move_table_name+"_h_m_"+move_col);
//	alert((move_table_name+"_h_m_"+move_col)+":"+$target);
	Pennyutils.setCssStyle($target, "position:absolute;");

	Pennyutils.events.addEventHandler(document.body, "mousemove", handleMM);
	Pennyutils.events.addEventHandler(document.body, "mouseup", handleMU);
}

function handleMU () {
	var $move = $(move_table_name+"_h_m_"+move_col);
	Pennyutils.setCssStyle($move, "position:static;");
	
	Pennyutils.events.removeEventHandler(document.body, "mousemove", handleMM);
	Pennyutils.events.removeEventHandler(document.body, "mouseup", handleMU);
}

$blankColor="background-color:#ffffff;";
$clickColor="background-color:#dddddd;";
$overColor="background-color:#f0f0f0;";
$overClickColor="background-color:#eeeeee;";

function clickRow (obj) {
	if (this.oldClickRow) {
		this.oldClickRow.setAttribute("show", "0");
		Pennyutils.setCssStyle(this.oldClickRow, $blankColor);
	}
	
	if (obj.getAttribute("show")==1) {
		obj.setAttribute("show", "0");
		Pennyutils.setCssStyle(obj, $blankColor);
	} else {
		obj.setAttribute("show", "1");
		Pennyutils.setCssStyle(obj, $clickColor);
	}
	
	this.oldClickRow = obj;
}

function overRow (obj) {
	if (obj.getAttribute("show")==1) {
		Pennyutils.setCssStyle(obj, $overClickColor);
	} else {
		Pennyutils.setCssStyle(obj, $overColor);
	}
}

function outRow (obj) {
	if (obj.getAttribute("show")==1) {
		Pennyutils.setCssStyle(obj, $clickColor);
	} else {
		Pennyutils.setCssStyle(obj, $blankColor);
	}
}

var $move_object;
var $target_object;
var $margin_width;
var $margin_height;
function handleMoveObjectMouseDown () {
	var $event=Pennyutils.events.getEvent(arguments[0]);
	$move_object = "string"==typeof arguments[1]?document.getElementById(arguments[1]):arguments[1];
	$margin_width=$event.clientX-parseInt($move_object.style.left);
	$margin_height=$event.clientY-parseInt($move_object.style.top);
	Pennyutils.events.addEventHandler(document.body, "mousemove", handleMoveObjectMouseMove);
	Pennyutils.events.addEventHandler(document.body, "mouseup", handleMoveObjectMouseUp);
}

function handleMoveObjectMouseUp () {
	Pennyutils.events.removeEventHandler(document.body, "mousemove", handleMoveObjectMouseMove);
	Pennyutils.events.removeEventHandler(document.body, "mouseup", handleMoveObjectMouseUp);
}

function handleMoveObjectMouseMove () {
	var $event = Pennyutils.events.getEvent();
	var $left=$event.clientX-$margin_width;
	var $top=$event.clientY-$margin_height;
	if(document.uniqueID) {
		$move_object.style.left = $left;
		$move_object.style.top = $top;
	} else {
		$move_object.style.setProperty("left", $left+"px", "");
		$move_object.style.setProperty("top", $top+"px", "");
	}
}

function getKeyCode () {
	var $event=Pennyutils.events.getEvent(arguments[0]);
	return $event.keyCode;
}