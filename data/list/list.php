<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
$LIST_FILE=array (
  'admin_user_group' => 
  array (
    'guest' => 'guest',
    'admin' => 'admin',
    'super' => 'super',
  ),
  'boolean' => 
  array (
    'no' => 'no',
    'yes' => 'yes',
  ),
  'status' => 
  array (
    'false' => 'false',
    'true' => 'true',
  ),
  'delivery_type' => 
  array (
    'deliver_goods' => 'deliver_goods',
    'expressage_goods' => 'expressage_goods',
    'post_goods' => 'post_goods',
    'self_goods' => 'self_goods',
  ),
  'sex' => 
  array (
    'female' => 'female',
    'male' => 'male',
  ),
  'url_type' => 
  array (
    '' => '',
    'null' => 'null',
    'index' => 'index',
    'process' => 'process',
    'merchandise' => 'merchandise',
    'products' => 'products',
    'news_one' => 'news_one',
    'news_more' => 'news_more',
  ),
  'degree' => 
  array (
    'doctor' => 'doctor',
    'master' => 'master',
    'bachelor' => 'bachelor',
    'college' => 'college',
    'senior' => 'senior',
    'junior' => 'junior',
    'primary' => 'primary',
  ),
  'process_type' => 
  array (
    'complete_order' => 'complete_order',
    'sign_order' => 'sign_order',
    'send_order' => 'send_order',
    'payment_order' => 'payment_order',
    'new_order' => 'new_order',
    'cancel_order' => 'cancel_order',
  ),
  'payment_type' => 
  array (
    'transfer_pay' => 'transfer_pay',
    'self_pay' => 'self_pay',
    'online_pay' => 'online_pay',
    'deliver_pay' => 'deliver_pay',
  ),
  'merchandise_cat' => 
  array (
    'sslx' => 'sslx',
    'tbdh' => 'tbdh',
    'gdgg' => 'gdgg',
  ),
  'rate_type' => 
  array (
    'vote' => 'vote',
    'view' => 'view',
    'comment' => 'comment',
  ),
  'banner_type' => 
  array (
    'ad1' => 'ad1',
    'ad2' => 'ad2',
    'ad3' => 'ad3',
    'ad4' => 'ad4',
    'ad5' => 'ad5',
    'ad6' => 'ad6',
  ),
  'marriage' => 
  array (
    'divorced' => 'divorced',
    'married' => 'married',
    'unmarried' => 'unmarried',
  ),
  'language_level' => 
  array (
    'very_good' => 'very_good',
    'good' => 'good',
    'general' => 'general',
    'unstudied' => 'unstudied',
  ),
  'key_value_type' => 
  array (
    'target' => 'target',
    'status' => 'status',
    'sex' => 'sex',
    'all_language' => 'all_language',
    'language' => 'language',
    'feedback_type' => 'feedback_type',
    'export_file_type' => 'export_file_type',
    'custom_style' => 'custom_style',
    'custom_purpose' => 'custom_purpose',
    'custom_budget' => 'custom_budget',
    'custom_type' => 'custom_type',
    'shipment_time' => 'shipment_time',
    'products_type' => 'products_type',
    'process_type' => 'process_type',
    'orders_status' => 'orders_status',
    'common_user_group' => 'common_user_group',
    'shipment_status' => 'shipment_status',
    'payment_status' => 'payment_status',
    'can_sell' => 'can_sell',
    'sell_status' => 'sell_status',
    'payment_type' => 'payment_type',
    'shipment_type' => 'shipment_type',
    'marriage' => 'marriage',
    'language_level' => 'language_level',
    'degree' => 'degree',
    'url_type' => 'url_type',
    'boolean' => 'boolean',
    'admin_user_group' => 'admin_user_group',
    'key_value_type' => 'key_value_type',
  ),
  'common_user_group' => 
  array (
    'normal' => 'normal',
    'super' => 'super',
  ),
  'export_file_type' => 
  array (
    'xml' => 'xml',
    'sql' => 'sql',
    'csv' => 'csv',
    'arr' => 'arr',
  ),
  'feedback_type' => 
  array (
    'feedback' => 'feedback',
    'consultation' => 'consultation',
    'complain' => 'complain',
    'booking' => 'booking',
  ),
  'logical' => 
  array (
    'and not' => 'and not',
    'or not' => 'or not',
    'or' => 'or',
    'and' => 'and',
  ),
  'operator' => 
  array (
    'like' => 'like',
    '=' => '=',
    '>' => '>',
    '>=' => '>=',
    '<' => '<',
    '<=' => '<=',
    '!=' => '!=',
    'in' => 'in',
  ),
  'all_language' => 
  array (
    'zh_tw' => 'zh_tw',
    'zh_cn' => 'zh_cn',
    'en_us' => 'en_us',
    '' => '',
  ),
  'language' => 
  array (
    'zh_tw' => 'zh_tw',
    'zh_cn' => 'zh_cn',
    'en_us' => 'en_us',
  ),
  'channel' => 
  array (
    '' => '',
    'index' => 'index',
    'enterprise' => 'enterprise',
    'events' => 'events',
    'trends' => 'trends',
    'feedback' => 'feedback',
    'copyright' => 'copyright',
    'about_us' => 'about_us',
    'contact_us' => 'contact_us',
    'orignal' => 'orignal',
    'crossover' => 'crossover',
    'promotion' => 'promotion',
    'exclusive' => 'exclusive',
    'trend' => 'trend',
    'scene' => 'scene',
    'saba_log' => 'saba_log',
    'dining_hall' => 'dining_hall',
    'shopping_guid' => 'shopping_guid',
    'payment_type' => 'payment_type',
    'shipment_type' => 'shipment_type',
    'after-sales' => 'after-sales',
    'self_service' => 'self_service',
    'about_download' => 'about_download',
    'official_weibo' => 'official_weibo',
    'official_weixin' => 'official_weixin',
    'official_website' => 'official_website',
    'member_index' => 'member_index',
    'member_register' => 'member_register',
    'member_login' => 'member_login',
    'member_lost_password' => 'member_lost_password',
    'channel_a' => 'channel_a',
    'channel_b' => 'channel_b',
    'channel_c' => 'channel_c',
    'channel_d' => 'channel_d',
    'channel_e' => 'channel_e',
    'channel_f' => 'channel_f',
    'channel_g' => 'channel_g',
    'channel_h' => 'channel_h',
    'channel_i' => 'channel_i',
    'channel_j' => 'channel_j',
    'channel_k' => 'channel_k',
    'channel_l' => 'channel_l',
    'orders_check_out' => 'orders_check_out',
    'orders_check_out_success' => 'orders_check_out_success',
    'member_center' => 'member_center',
    'orders_online_pay' => 'orders_online_pay',
    'member_center_content' => 'member_center_content',
    'future' => 'future',
    'originality' => 'originality',
    'orders_carts' => 'orders_carts',
  ),
  'news_type' => 
  array (
    'enterprise' => 'enterprise',
    'events' => 'events',
    'trends' => 'trends',
    'feedback' => 'feedback',
    'copyright' => 'copyright',
    'about_us' => 'about_us',
    'contact_us' => 'contact_us',
    'orignal' => 'orignal',
    'crossover' => 'crossover',
    'promotion' => 'promotion',
    'exclusive' => 'exclusive',
    'trend' => 'trend',
    'scene' => 'scene',
    'saba_log' => 'saba_log',
    'dining_hall' => 'dining_hall',
    'shopping_guid' => 'shopping_guid',
    'payment_type' => 'payment_type',
    'shipment_type' => 'shipment_type',
    'after-sales' => 'after-sales',
    'self_service' => 'self_service',
    'about_download' => 'about_download',
    'official_weibo' => 'official_weibo',
    'official_weixin' => 'official_weixin',
    'official_website' => 'official_website',
    'channel_a' => 'channel_a',
    'channel_b' => 'channel_b',
    'channel_c' => 'channel_c',
    'channel_d' => 'channel_d',
    'channel_e' => 'channel_e',
    'channel_f' => 'channel_f',
    'channel_g' => 'channel_g',
    'channel_h' => 'channel_h',
    'channel_i' => 'channel_i',
    'channel_j' => 'channel_j',
    'channel_k' => 'channel_k',
    'channel_l' => 'channel_l',
    'member_center_content' => 'member_center_content',
    'future' => 'future',
    'originality' => 'originality',
  ),
  'web_channel' => 
  array (
    '' => '',
    'index' => '',
    'enterprise' => '',
    'events' => '',
    'trends' => '',
    'feedback' => '',
    'copyright' => '',
    'about_us' => '',
    'contact_us' => '',
    'orignal' => '',
    'crossover' => '',
    'promotion' => '',
    'exclusive' => '',
    'trend' => '',
    'scene' => '',
    'saba_log' => '',
    'dining_hall' => '',
    'shopping_guid' => '',
    'payment_type' => '',
    'shipment_type' => '',
    'after-sales' => '',
    'self_service' => '',
    'about_download' => '',
    'official_weibo' => '',
    'official_weixin' => '',
    'official_website' => '',
    'member_index' => '',
    'member_register' => '',
    'member_login' => '',
    'member_lost_password' => '',
    'channel_a' => '',
    'channel_b' => '',
    'channel_c' => '',
    'channel_d' => '',
    'channel_e' => '',
    'channel_f' => '',
    'channel_g' => '',
    'channel_h' => '',
    'channel_i' => '',
    'channel_j' => '',
    'channel_k' => '',
    'channel_l' => '',
    'orders_check_out' => '',
    'orders_check_out_success' => '',
    'member_center' => '',
    'orders_online_pay' => '',
    'member_center_content' => '',
    'future' => '',
    'originality' => '',
    'orders_carts' => '',
  ),
  'shipment_type' => 
  array (
    'self_goods' => 'self_goods',
    'post_goods' => 'post_goods',
    'expressage_goods' => 'expressage_goods',
    'deliver_goods' => 'deliver_goods',
  ),
  'sell_status' => 
  array (
    'coming_soon' => 'coming_soon',
    'presell' => 'presell',
    'sellout' => 'sellout',
    'selling' => 'selling',
  ),
  'payment_status' => 
  array (
    'finished' => 'finished',
    'halfway' => 'halfway',
    '' => '',
  ),
  'shipment_status' => 
  array (
    'finished' => 'finished',
    'halfway' => 'halfway',
    '' => '',
  ),
  'order_status' => 
  array (
    'create' => 'create',
    'confirm' => 'confirm',
    'finished' => 'finished',
    'cancel' => 'cancel',
  ),
  'target' => 
  array (
    '_top' => '_top',
    '_parent' => '_parent',
    '_self' => '_self',
    '_blank' => '_blank',
  ),
  'orders_status' => 
  array (
    'cancel' => 'cancel',
    'holdon' => 'holdon',
    'finished' => 'finished',
    'confirm' => 'confirm',
    'create' => 'create',
  ),
  'can_sell' => 
  array (
    'presell' => 'presell',
    'selling' => 'selling',
  ),
  'popedom_channel' => 
  array (
  ),
  'products_type' => 
  array (
    'custom' => 'custom',
    'recommended' => 'recommended',
    'promotion' => 'promotion',
    'crossover' => 'crossover',
    'orignal' => 'orignal',
  ),
  'shipment_time' => 
  array (
    'shipment_time_2' => 'shipment_time_2',
    'shipment_time_1' => 'shipment_time_1',
    'shipment_time_0' => 'shipment_time_0',
  ),
  'custom_budget' => 
  array (
    'custom_budget_4' => 'custom_budget_4',
    'custom_budget_3' => 'custom_budget_3',
    'custom_budget_2' => 'custom_budget_2',
    'custom_budget_1' => 'custom_budget_1',
    'custom_budget_0' => 'custom_budget_0',
  ),
  'custom_purpose' => 
  array (
    'custom_purpose_4' => 'custom_purpose_4',
    'custom_purpose_3' => 'custom_purpose_3',
    'custom_purpose_2' => 'custom_purpose_2',
    'custom_purpose_1' => 'custom_purpose_1',
    'custom_purpose_0' => 'custom_purpose_0',
  ),
  'custom_style' => 
  array (
    'custom_style_4' => 'custom_style_4',
    'custom_style_3' => 'custom_style_3',
    'custom_style_2' => 'custom_style_2',
    'custom_style_1' => 'custom_style_1',
    'custom_style_0' => 'custom_style_0',
  ),
  'custom_type' => 
  array (
    'custom_type_0' => 'custom_type_0',
    'custom_type_2' => 'custom_type_2',
    'custom_type_1' => 'custom_type_1',
  ),
)
?>