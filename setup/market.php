<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
// drop table.
echo 'market drop ...';
$query = 'drop table if exists '.PENNY_CMS_DB_TABLE_PREFIX.'_market;';
create_update($query);	// process.

echo 'create...';
// create table.
$query = 'create table if not exists '.PENNY_CMS_DB_TABLE_PREFIX.'_market ('.
'_market_id	integer primary key autoincrement,'.
'_market_name	varchar (255) not null default \'\','.
'_actual_name	varchar (255) not null default \'\','.
'_contact	varchar (255) not null default \'\','.
'_summary	text not null default \'\','.
'_e_mail	varchar (255) not null default \'\','.
'_website	varchar (255) not null default \'\','.
'_phone	varchar (255) not null default \'\','.
'_fax	varchar (255) not null default \'\','.
'_mobile	varchar (255) not null default \'\','.
'_qq	varchar (255) not null default \'\','.
'_msn	varchar (255) not null default \'\','.
'_address	varchar (255) not null default \'\','.
'_zip_code	varchar (255) not null default \'\','.
'_map	varchar (255) not null default \'\','.
'_map_link	varchar (255) not null default \'\','.
'_logo	varchar (255) not null default \'\','.
'_banner	varchar (255) not null default \'\','.
'_banners	varchar (255) not null default \'\','.
'_image	varchar (255) not null default \'\','.
'_images	text not null default \'\','.
'_photo	varchar (255) not null default \'\','.
'_photos	text not null default \'\','.
'_videos	text not null default \'\','.
'_files	text not null default \'\','.
'_json	text not null default \'\','.
'_details	text not null default \'\','.
'_remark	text not null default \'\','.
'_content	text not null default \'\','.
'_start_date_time	varchar (50) not null default \'\','.
'_close_date_time	varchar (50) not null default \'\','.
'_tag	varchar (255) not null default \'\','.
'_order_by	varchar (10) not null default \'\','.
'_status	varchar (100) not null default \'\','.
'_create_user	varchar(255)	not null	default \'\','.
'_create_date_time	varchar(50)	not null	default \'\','.
'_update_user	varchar(255)	not null	default \'\','.
'_update_date_time	varchar(50)	not null	default \'\','.
'_version_num	int	not null	default 0'.
');';
create_update($query);	// process.

// create unique_index.
echo 'unique index...';
//$query = 'CREATE UNIQUE INDEX unique_index_market ON '.PENNY_CMS_DB_TABLE_PREFIX.'_market (_market_name);';
//create_update($query);	// process.
echo 'market all ok.<br />';
?>