<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
$module['words']['columns']=
'_words_id,'.
'_words_type,'.
'_chapter,'.
'_phrase,'.
'_sentence,'.
'_english,'.
'_english_voice,'.
'_phonetic,'.
'_property,'.
'_chinese,'.
'_chinese_voice,'.
'_content,'.
'_details,'.
'_remark,'.
'_json,'.
'_status,'.
'_create_user,'.
'_create_date_time,'.
'_update_user,'.
'_update_date_time,'.
'_version_num';

$module['words']['list_name']=
','.
'words_type,'.
'chapter,'.
','.
','.
','.
','.
','.
'property,'.
','.
','.
','.
','.
','.
','.
'status,'.
','.
','.
','.
','.
'';

$module['words']['gridx']=
'0,'.
'1,'.
'2,'.
'3,'.
'4,'.
'5,'.
'6,'.
'7,'.
'8,'.
'9,'.
'10,'.
'0,'.
'1,'.
'2,'.
'3,'.
'4,'.
'0,1,2,3,4';

$module['words']['gridy']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'1,'.
'1,'.
'2,'.
'2,'.
'3,'.
'3,'.
'4,'.
'4,'.
'4,'.
'4,'.
'5,'.
'-1,-1,-1,-1,-1';

$module['words']['gridh']=
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,2,2,2,2';

$module['words']['gridw']=
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'"1_3","1_9","1_3","1_9","1_0"';

$module['words']['lw']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'70,70,70,70,70';

$module['words']['w']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'60,120,60,120,30';

$module['words']['lh']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,0,0,0,0';

$module['words']['h']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,0,0,0,0';

$module['words']['input']=
'number,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'date_time,'.
'text,'.
'date_time,'.
'number';

$module['words']['field']=
'id,'.
'radio,'.
'checkbox_select,'.
'text,'.
'text,'.
'text,'.
'sound,'.
'text,'.
'select,'.
'text,'.
'sound,'.
'text_area,'.
'text_area,'.
'text_area,'.
'text_area,'.
'radio,'.
'text_hidden,'.
'text_hidden,'.
'text_hidden,'.
'text_hidden,'.
'text_hidden';

$module['words']['max_length']=
'11,'.
'50,'.
'255,'.
'255,'.
'255,'.
'255,'.
'255,'.
'255,'.
'50,'.
'255,'.
'255,'.
'20000,'.
'20000,'.
'20000,'.
'20000,'.
'100,'.
'255,'.
'19,'.
'255,'.
'19,'.
'11';

$module['words']['show_columns']=
'_words_type,'.
'_chapter,'.
'_phrase,'.
'_sentence,'.
'_english,'.
'_english_voice,'.
'_phonetic,'.
'_property,'.
'_chinese,'.
'_chinese_voice,'.
'_content,'.
'_details,'.
'_remark,'.
'_json,'.
'_status,'.
'_create_user,'.
'_create_date_time,'.
'_update_user,'.
'_update_date_time,'.
'_version_num';

$module['words']['batch_columns']=
'_words_type,'.'_chapter,'.'_phrase,'.'_sentence,'.'_english,'.'_english_voice,'.'_phonetic,'.'_property,'.'_chinese,'.'_chinese_voice,'.'_content,'.'_details,'.'_remark,'.'_json,'.'_status';

$module['words']['batch_list_name']=
'words_type,'.'chapter,'.','.','.','.','.','.'property,'.','.','.','.','.','.','.'status';

$module['words']['batch_input']=
'radio,'.'checkbox_select,'.'text,'.'text,'.'text,'.'sound,'.'text,'.'select,'.'text,'.'sound,'.'text_area,'.'text_area,'.'text_area,'.'text_area,'.'radio';
?>