<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
$key_value_type_index=0;
$key_value_array=array(
array('super','0',0,'admin_user_group'),
array('admin','1',1,'admin_user_group'),
array('guest','2',2,'admin_user_group'),
array('admin_user_group','admin_user_group',$key_value_type_index++,'key_value_type'),

array('no','0',1,'boolean'),
array('yes','1',0,'boolean'),
array('boolean','boolean',$key_value_type_index++,'key_value_type'),

array('en_us','1',1,'language'),
array('zh_cn','2',2,'language'),
array('zh_tw','3',3,'language'),
array('language','language',$key_value_type_index++,'key_value_type'),

array('male','0',0,'sex'),
array('female','1',1,'sex'),
array('sex','sex',$key_value_type_index++,'key_value_type'),

array('true','1',0,'status'),
array('false','0',1,'status'),
array('status','status',$key_value_type_index++,'key_value_type'),

array('word','0','','words_type'),
array('phrase','1','','words_type'),
array('sentence','2','','words_type'),
array('words_type','words_type',$key_value_type_index++,'key_value_type'),

array('n.','1','','property'),
array('v.','2','','property'),
array('adj.','3','','property'),
array('adv.','4','','property'),
array('','0','','property'),
array('property','property',$key_value_type_index++,'key_value_type'),

array('shop_type_1','shop_type_1','','shop_type'),
array('shop_type_2','shop_type_2','','shop_type'),
array('shop_type_3','shop_type_3','','shop_type'),
array('shop_type_4','shop_type_4','','shop_type'),
array('shop_type','shop_type',$key_value_type_index++,'key_value_type'),

array('shop_mold_1','shop_mold_1','','shop_mold'),
array('shop_mold_2','shop_mold_2','','shop_mold'),
array('shop_mold_3','shop_mold_3','','shop_mold'),
array('shop_mold_4','shop_mold_4','','shop_mold'),
array('shop_mold','shop_mold',$key_value_type_index++,'key_value_type'),

array('shop_area_1','shop_area_1','','shop_area'),
array('shop_area_2','shop_area_2','','shop_area'),
array('shop_area_3','shop_area_3','','shop_area'),
array('shop_area_4','shop_area_4','','shop_area'),
array('shop_area_5','shop_area_5','','shop_area'),
array('shop_area_6','shop_area_6','','shop_area'),
array('shop_area','shop_area',$key_value_type_index++,'key_value_type'),

array('shop_way_1','shop_way_1','','shop_way'),
array('shop_way_2','shop_way_2','','shop_way'),
array('shop_way_3','shop_way_3','','shop_way'),
array('shop_way_4','shop_way_4','','shop_way'),
array('shop_way','shop_way',$key_value_type_index++,'key_value_type'),

array('shop_target_1','shop_target_1','','shop_target'),
array('shop_target_2','shop_target_2','','shop_target'),
array('shop_target','shop_target',$key_value_type_index++,'key_value_type'),

array('key_value_type','key_value_type',0,'key_value_type')
);

$query_begin='insert into '.PENNY_CMS_DB_TABLE_PREFIX.'_key_value (_key,_value,_order_by,_key_value_type)values';
foreach ($key_value_array as $key_value) {
	create_update($query_begin.'(\''.$key_value[0].'\',\''.$key_value[1].'\',\''.$key_value[2].'\',\''.$key_value[3].'\');');	// process.
}
create_update('update '.PENNY_CMS_DB_TABLE_PREFIX.'_key_value set _status=\'true\';');
echo " ok.<br />";

// Write js list file.
$list_file=array();

$query='select * from '.PENNY_CMS_DB_TABLE_PREFIX.'_key_value where _status="true" order by _order_by desc, _key_value_id asc';
$results=create_query($query);
if ($results) {
	foreach ($results as $record) {
		$key_value_type=$record['_key_value_type'];
		$key=$record['_key'];
		$value=$record['_value'];
		
		if (!isset($list_file[$key_value_type])) {
			$list_file[$key_value_type]=array();
		}
		
		$list_file[$key_value_type][]=array(
				'key'=>$key
				,
				'value'=>get_name($key)
			);
	}
}

write_file('../utils/lib/list_data.js', 'var $LIST='.json_encode($list_file));
?>