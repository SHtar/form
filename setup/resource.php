<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
// drop table.
echo 'resource drop ...';
$query = 'drop table if exists '.PENNY_CMS_DB_TABLE_PREFIX.'_resource;';
create_update($query);	// process.

echo 'create...';
// create table.
$query = 'create table if not exists '.PENNY_CMS_DB_TABLE_PREFIX.'_resource ('.
'_resource_id	integer primary key autoincrement,'.
'_language	varchar (100) not null default \'\','.
'_key	varchar (100) not null default \'\','.
'_value	text not null default \'\','.
'_status	varchar (100) not null default \'\','.
'_create_user	varchar(255)	not null	default \'\','.
'_create_date_time	varchar(50)	not null	default \'\','.
'_update_user	varchar(255)	not null	default \'\','.
'_update_date_time	varchar(50)	not null	default \'\','.
'_version_num	int	not null	default 0'.
');';
create_update($query);	// process.

echo 'resource all ok.<br />';
?>