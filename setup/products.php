<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
// drop table.
echo 'products drop ...';
$query = 'drop table if exists '.PENNY_CMS_DB_TABLE_PREFIX.'_products;';
create_update($query);	// process.

echo 'create...';
// create table.
$query = 'create table if not exists '.PENNY_CMS_DB_TABLE_PREFIX.'_products ('.
'_products_id	integer primary key autoincrement,'.
'_products_type	varchar (200) not null default \'\','.
'_products_name	varchar (255) not null default \'\','.
'_news_id	varchar (50) not null default \'\','.
'_news_type	varchar (50) not null default \'\','.
'_custom_code	varchar (255) not null default \'\','.
'_hs_code	varchar (255) not null default \'\','.
'_barcode	varchar (255) not null default \'\','.
'_model_no	varchar (255) not null default \'\','.
'_item_no	varchar (255) not null default \'\','.
'_sales_dept	varchar (255) not null default \'\','.
'_brand	varchar (255) not null default \'\','.
'_supplyer	varchar (11) not null default \'\','.
'_collection	varchar (255) not null default \'\','.
'_style	varchar (255) not null default \'\','.
'_alias	varchar (255) not null default \'\','.
'_category	varchar (255) not null default \'\','.
'_merchandise	varchar (255) not null default \'\','.
'_standard	varchar (255) not null default \'\','.
'_color	varchar (255) not null default \'\','.
'_units	varchar (255) not null default \'\','.
'_unit_cost	decimal (38,2) not null default \'0.00\','.
'_unit_price	decimal (38,2) not null default \'0.00\','.
'_unit_value	decimal (38,2) not null default \'0.00\','.
'_amount	decimal (11,6) not null default \'0\','.
'_abbreviate	varchar (255) not null default \'\','.
'_title	text not null default \'\','.
'_hint	varchar (255) not null default \'\','.
'_preview	text not null default \'\','.
'_tag	varchar (255) not null default \'\','.
'_author	varchar (255) not null default \'\','.
'_source	varchar (255) not null default \'\','.
'_gross_weight	decimal (11,6) not null default \'0\','.
'_weight	decimal (11,6) not null default \'0\','.
'_size	varchar (255) not null default \'\','.
'_volume	decimal (11,6) not null default \'0\','.
'_pqty	decimal (11,6) not null default \'0\','.
'_moq	decimal (11,6) not null default \'0\','.
'_warning_upper	decimal (11,6) not null default \'0\','.
'_warning_lower	decimal (11,6) not null default \'0\','.
'_warning_sell	decimal (11,6) not null default \'0\','.
'_producing_area	varchar (255) not null default \'\','.
'_useful_life	integer (11) not null default \'0\','.
'_useful_life_units	varchar (255) not null default \'\','.
'_selling_amount	decimal (11,6) not null default \'0\','.
'_selling_total_amount	decimal (11,6) not null default \'0\','.
'_warehouse_amount	decimal (11,6) not null default \'0\','.
'_warehouse_total_amount	decimal (11,6) not null default \'0\','.
'_inner_quantity	decimal (11,6) not null default \'0\','.
'_inner_units	varchar (100) not null default \'\','.
'_inner_weight	decimal (11,6) not null default \'0\','.
'_package_quantity	decimal (11,6) not null default \'0\','.
'_package_units	varchar (100) not null default \'\','.
'_package_weight	decimal (11,6) not null default \'0\','.
'_file	text not null default \'\','.
'_files	text not null default \'\','.
'_video	text not null default \'\','.
'_videos	text not null default \'\','.
'_banner	text not null default \'\','.
'_banners	text not null default \'\','.
'_image	text not null default \'\','.
'_images	text not null default \'\','.
'_photo	text not null default \'\','.
'_photos	text not null default \'\','.
'_picture	text not null default \'\','.
'_pictures	text not null default \'\','.
'_remark	text not null default \'\','.
'_content	text not null default \'\','.
'_details	text not null default \'\','.
'_json	text not null default \'\','.
'_materials	text not null default \'\','.
'_procedures	text not null default \'\','.
'_extend_a	text not null default \'\','.
'_extend_b	text not null default \'\','.
'_extend_c	text not null default \'\','.
'_extend_d	text not null default \'\','.
'_extend_e	text not null default \'\','.
'_other	text not null default \'\','.
'_start_date_time	varchar (50) not null default \'\','.
'_close_date_time	varchar (50) not null default \'\','.
'_start_time	varchar (50) not null default \'\','.
'_close_time	varchar (50) not null default \'\','.
'_start_term	text not null default \'\','.
'_close_term	text not null default \'\','.
'_comments	varchar (255) not null default \'\','.
'_likes	varchar (255) not null default \'\','.
'_hits	varchar (255) not null default \'\','.
'_visits	varchar (255) not null default \'\','.
'_votes	varchar (255) not null default \'\','.
'_commend	varchar (255) not null default \'\','.
'_setting	varchar (255) not null default \'\','.
'_order_by	varchar (100) not null default \'\','.
'_comment_status	varchar (100) not null default \'\','.
'_commend_status	varchar (100) not null default \'\','.
'_sell_status	varchar (100) not null default \'\','.
'_status	varchar (100) not null default \'\','.
'_create_user	varchar(255)	not null	default \'\','.
'_create_date_time	varchar(50)	not null	default \'\','.
'_update_user	varchar(255)	not null	default \'\','.
'_update_date_time	varchar(50)	not null	default \'\','.
'_version_num	int	not null	default 0'.
');';
create_update($query);	// process.

echo 'products all ok.<br />';
?>