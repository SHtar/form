<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
$module['chapters']['columns']=
'_chapters_id,'.
'_chapter,'.
'_chapter_name,'.
'_course,'.
'_content,'.
'_details,'.
'_remark,'.
'_json,'.
'_status,'.
'_create_user,'.
'_create_date_time,'.
'_update_user,'.
'_update_date_time,'.
'_version_num';

$module['chapters']['list_name']=
','.
','.
','.
','.
','.
','.
','.
','.
'status,'.
','.
','.
','.
','.
'';

$module['chapters']['gridx']=
'0,'.
'1,'.
'2,'.
'3,'.
'0,'.
'1,'.
'2,'.
'3,'.
'4,'.
'0,1,2,3,4';

$module['chapters']['gridy']=
'0,'.
'0,'.
'0,'.
'0,'.
'1,'.
'1,'.
'1,'.
'1,'.
'2,'.
'-1,-1,-1,-1,-1';

$module['chapters']['gridh']=
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,2,2,2,2';

$module['chapters']['gridw']=
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'2,'.
'"1_3","1_9","1_3","1_9","1_0"';

$module['chapters']['lw']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'70,70,70,70,70';

$module['chapters']['w']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'60,120,60,120,30';

$module['chapters']['lh']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,0,0,0,0';

$module['chapters']['h']=
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,'.
'0,0,0,0,0';

$module['chapters']['input']=
'number,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'text,'.
'date_time,'.
'text,'.
'date_time,'.
'number';

$module['chapters']['field']=
'id,'.
'text,'.
'text,'.
'text,'.
'text_area,'.
'text_area,'.
'text_area,'.
'text_area,'.
'radio,'.
'text_hidden,'.
'text_hidden,'.
'text_hidden,'.
'text_hidden,'.
'text_hidden';

$module['chapters']['max_length']=
'11,'.
'255,'.
'255,'.
'255,'.
'20000,'.
'20000,'.
'20000,'.
'20000,'.
'100,'.
'255,'.
'19,'.
'255,'.
'19,'.
'11';

$module['chapters']['show_columns']=
'_chapter,'.
'_chapter_name,'.
'_course,'.
'_content,'.
'_details,'.
'_remark,'.
'_json,'.
'_status,'.
'_create_user,'.
'_create_date_time,'.
'_update_user,'.
'_update_date_time,'.
'_version_num';

$module['chapters']['batch_columns']=
'_chapter,'.'_chapter_name,'.'_course,'.'_content,'.'_details,'.'_remark,'.'_json,'.'_status';

$module['chapters']['batch_list_name']=
','.','.','.','.','.','.','.'status';

$module['chapters']['batch_input']=
'text,'.'text,'.'text,'.'text_area,'.'text_area,'.'text_area,'.'text_area,'.'radio';
?>