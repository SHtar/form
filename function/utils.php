<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/

function get_resize_img ($img, $img_width, $img_height, $img_suffix=NULL, $c_w=NULL, $c_h=NULL, $c_w_p=-1, $c_h_p=-1, $is_rewrite=FALSE) {
	if (strlen($img)==0 || strpos($img, '://')>0) {
		return $img;
	}
	$temp_img=PENNY_CMS_DOC_PATH.$img;
	if (file_exists($temp_img)) {
		$img_suffix=isset($img_suffix)?$img_suffix:'_'.$img_width.'_'.$img_height;
		$path_parts=pathinfo($temp_img);
		$temp_new_img=$path_parts['dirname'].'/r_'.str_replace('.'.$path_parts['extension'], '', $path_parts['basename']).$img_suffix.'.'.$path_parts['extension'];
//		echo filemtime($temp_new_img).' , '.filemtime($temp_img);
		if ($is_rewrite || !file_exists($temp_new_img) || filemtime($temp_img)>filemtime($temp_new_img)) {
			list($i_w,$i_h,$i_t,$i_a)=getimagesize($temp_img);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					$s_i=imagecreatefromjpeg($temp_img);
					break;
				case IMAGETYPE_PNG:
					$s_i=imagecreatefrompng($temp_img);
					break;
				case IMAGETYPE_GIF:
					$s_i=imagecreatefromgif($temp_img);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
			
//			$i_w=imagesx($s_i);
//			$i_h=imagesy($s_i);
			$r=1.0;
//			if ($img_width>$i_w && $img_height>$i_h) {
//				$i_w_r=$r*$i_w/$img_width;
//				$i_w_h=$r*$i_h/$img_height;
//			} else {
//				$i_w_r=$r*$img_width/$i_w;
//				$i_w_h=$r*$img_height/$i_h;
//			}
//			$i_w_r=$img_width>$i_w?$r*$img_width/$i_w:$r*$i_w/$img_width;
//			$i_w_h=$img_height>$i_h?$r*$img_height/$i_h:$r*$i_h/$img_height;
			$i_w_r=$r*$i_w/$img_width;
			$i_h_r=$r*$i_h/$img_height;
//			if (($i_w_r<1 && $i_h_r<1)||($i_w_r>1 && $i_h_r>1)) {
//				if () {
//				}
//			} else {
//			}
			$r=$i_w_r<$i_h_r?$i_w_r:$i_h_r;
			$n_w=(int)($i_w/$r);
			$n_h=(int)($i_h/$r);
//			echo $i_w_r.' , '.$i_h_r.' , '.$r.' , '.$n_w.' , '.$n_h.' , '.$i_w.' , '.$i_h;
//			$i_i=imagecreatetruecolor($img_width,$img_height);
//			imagecopy($i_i,$s_i,0,0,0,0,$n_w,$n_h);
			$i_i=imagecreatetruecolor($n_w,$n_h);
			imagecopyresampled($i_i,$s_i,0,0,0,0,$n_w,$n_h,$i_w,$i_h);
//			imagejpeg($n_i,$d_i,100);
			$n_i=imagecreatetruecolor($img_width,$img_height);
			// Cut site.
			switch ($c_w) {
				case 0:
					$_c_w=$n_w-$img_width>0?($n_w-$img_width)/2:0;
					break;
				case 1:
					$_c_w=$n_w-$img_width>0?$n_w-$img_width:0;
					break;
				default:
					$_c_w=0;
			}
			switch ($c_h) {
				case 0:
					$_c_h=$n_h-$img_height>0?($n_h-$img_height)/2:0;
					break;
				case 1:
					$_c_h=$n_h-$img_height>0?$n_h-$img_height:0;
					break;
				default:
					$_c_h=0;
			}
			if ($c_w_p>=0) {
				$_c_w=$c_w_p;
			}
			if ($c_h_p>=0) {
				$_c_h=$c_h_p;
			}
			// Store img.
			imagecopy($n_i,$i_i,0,0,$_c_w,$_c_h,$img_width,$img_height);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					imagejpeg($n_i,$temp_new_img,100);
					break;
				case IMAGETYPE_PNG:
					imagepng($n_i,$temp_new_img,8);
					break;
				case IMAGETYPE_GIF:
					imagegif($n_i,$temp_new_img,100);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
		}
		return str_replace(PENNY_CMS_DOC_DIR, PENNY_CMS_URL_DIR, $temp_new_img);
	}
	return NULL;
}
function get_fitsize_img ($img, $img_width, $img_height, $img_suffix=NULL, $c_w=0, $c_h=0, $c_w_p=-1, $c_h_p=-1, $is_rewrite=FALSE) {
	if (strlen($img)==0 || strpos($img, '://')>0) {
		return $img;
	}
	$temp_img=PENNY_CMS_DOC_PATH.$img;
	if (file_exists($temp_img)) {
		$img_suffix=isset($img_suffix)?$img_suffix:'_'.$img_width.'_'.$img_height;
		$path_parts=pathinfo($temp_img);
		$temp_new_img=$path_parts['dirname'].'/r_'.str_replace('.'.$path_parts['extension'], '', $path_parts['basename']).$img_suffix.'.'.$path_parts['extension'];
		if ($is_rewrite || !file_exists($temp_new_img) || filemtime($temp_img)>filemtime($temp_new_img)) {
			list($i_w,$i_h,$i_t,$i_a)=getimagesize($temp_img);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					$s_i=imagecreatefromjpeg($temp_img);
					break;
				case IMAGETYPE_PNG:
					$s_i=imagecreatefrompng($temp_img);
					break;
				case IMAGETYPE_GIF:
					$s_i=imagecreatefromgif($temp_img);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
			$r=1.0;
			$i_w_r=$r*$img_width/$i_w;
			$i_h_r=$r*$img_height/$i_h;
			$r=$i_w_r<$i_h_r?$i_w_r:$i_h_r;
			$n_w=(int)($i_w*$r);
			$n_h=(int)($i_h*$r);
			$i_i=imagecreatetruecolor($n_w,$n_h);
			imagecopyresampled($i_i,$s_i,0,0,0,0,$n_w,$n_h,$i_w,$i_h);
			$n_i=imagecreatetruecolor($img_width,$img_height);
			$blank=imagecolorallocate($n_i, 255, 255, 255);
			imagefill($n_i, 0, 0, $blank);
			// Cut site.
			switch ($c_w) {
				case 0:
					$_c_w=$img_width-$n_w>0?($img_width-$n_w)/2:0;
					break;
				case 1:
					$_c_w=$img_width-$n_w>0?$img_width-$n_w:0;
					break;
				default:
					$_c_w=0;
			}
			switch ($c_h) {
				case 0:
					$_c_h=$img_height-$n_h>0?($img_height-$n_h)/2:0;
					break;
				case 1:
					$_c_h=$img_height-$n_h>0?$img_height-$n_h:0;
					break;
				default:
					$_c_h=0;
			}
			if ($c_w_p>=0) {
				$_c_w=$c_w_p;
			}
			if ($c_h_p>=0) {
				$_c_h=$c_h_p;
			}
			// Store img.
			imagecopy($n_i,$i_i,$_c_w,$_c_h,0,0,$n_w,$n_h);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					imagejpeg($n_i,$temp_new_img,100);
					break;
				case IMAGETYPE_PNG:
					imagepng($n_i,$temp_new_img,8);
					break;
				case IMAGETYPE_GIF:
					imagegif($n_i,$temp_new_img,100);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
		}
		return str_replace(PENNY_CMS_DOC_DIR, PENNY_CMS_URL_DIR, $temp_new_img);
	}
	return NULL;
}
function get_most_size_img ($img, $img_width, $img_height, $img_suffix=NULL, $c_w=NULL, $c_h=NULL, $c_w_p=-1, $c_h_p=-1, $is_rewrite=FALSE) {
	if (strlen($img)==0 || strpos($img, '://')>0) {
		return $img;
	}
	$temp_img=PENNY_CMS_DOC_PATH.$img;
	if (file_exists($temp_img)) {
		$img_suffix=isset($img_suffix)?$img_suffix:'_'.$img_width.'_'.$img_height;
		$path_parts=pathinfo($temp_img);
		$temp_new_img=$path_parts['dirname'].'/r_'.str_replace('.'.$path_parts['extension'], '', $path_parts['basename']).$img_suffix.'.'.$path_parts['extension'];
//		echo filemtime($temp_new_img).' , '.filemtime($temp_img);
		if ($is_rewrite || !file_exists($temp_new_img) || filemtime($temp_img)>filemtime($temp_new_img)) {
			list($i_w,$i_h,$i_t,$i_a)=getimagesize($temp_img);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					$s_i=imagecreatefromjpeg($temp_img);
					break;
				case IMAGETYPE_PNG:
					$s_i=imagecreatefrompng($temp_img);
					break;
				case IMAGETYPE_GIF:
					$s_i=imagecreatefromgif($temp_img);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
			
			$r=1.0;
			$i_w_r=$r*$i_w/$img_width;
			$i_h_r=$r*$i_h/$img_height;
//			echo $i_w_r.' , '.$i_h_r;
			if ($i_w_r<1 && $i_h_r<1) {	// Don't over size, return.
				return $img;
			}
			// Store img.
			$r=$i_w_r>$i_h_r?$i_w_r:$i_h_r;
			$n_w=(int)($i_w/$r);
			$n_h=(int)($i_h/$r);
			$n_i=imagecreatetruecolor($n_w,$n_h);
			imagecopyresampled($n_i,$s_i,0,0,0,0,$n_w,$n_h,$i_w,$i_h);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					imagejpeg($n_i,$temp_new_img,100);
					break;
				case IMAGETYPE_PNG:
					imagepng($n_i,$temp_new_img,8);
					break;
				case IMAGETYPE_GIF:
					imagegif($n_i,$temp_new_img,100);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
		}
		return str_replace(PENNY_CMS_DOC_DIR, PENNY_CMS_URL_DIR, $temp_new_img);
	}
	return NULL;
}

function get_most_side_size_img ($img, $img_width_or_height, $width_or_height=TRUE, $img_suffix=NULL, $c_w=NULL, $c_h=NULL, $c_w_p=-1, $c_h_p=-1, $is_rewrite=FALSE) {
	if (strlen($img)==0 || strpos($img, '://')>0) {
		return $img;
	}
	$temp_img=PENNY_CMS_DOC_PATH.$img;
	if (file_exists($temp_img)) {
		$img_suffix=isset($img_suffix)?$img_suffix:'_'.$img_width_or_height;
		$path_parts=pathinfo($temp_img);
		$temp_new_img=$path_parts['dirname'].'/r_'.str_replace('.'.$path_parts['extension'], '', $path_parts['basename']).$img_suffix.'.'.$path_parts['extension'];
//		echo filemtime($temp_new_img).' , '.filemtime($temp_img);
		if ($is_rewrite || !file_exists($temp_new_img) || filemtime($temp_img)>filemtime($temp_new_img)) {
			list($i_w,$i_h,$i_t,$i_a)=getimagesize($temp_img);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					$s_i=imagecreatefromjpeg($temp_img);
					break;
				case IMAGETYPE_PNG:
					$s_i=imagecreatefrompng($temp_img);
					break;
				case IMAGETYPE_GIF:
					$s_i=imagecreatefromgif($temp_img);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
			
			$r=1.0;
			
//			$i_w_r=$r*$i_w/$img_width;
//			$i_h_r=$r*$i_h/$img_height;
			
			if ($width_or_height) {
				$r=$r*$i_w/$img_width_or_height;
				if ($r<1) {
					return $img;
				}
			} else {
				$r=$r*$i_h/$img_width_or_height;
				if ($r<1) {
					return $img;
				}
			}
//			echo $i_w_r.' , '.$i_h_r;
//			if ($i_w_r<1 && $i_h_r<1) {	// Don't over size, return.
//				return $img;
//			}
			// Store img.
///			$r=$i_w_r>$i_h_r?$i_w_r:$i_h_r;
			$n_w=(int)($i_w/$r);
			$n_h=(int)($i_h/$r);
			$n_i=imagecreatetruecolor($n_w,$n_h);
			imagecopyresampled($n_i,$s_i,0,0,0,0,$n_w,$n_h,$i_w,$i_h);
			switch ($i_t) {
				case IMAGETYPE_JPEG:
					imagejpeg($n_i,$temp_new_img,80);
					break;
				case IMAGETYPE_PNG:
					imagepng($n_i,$temp_new_img,8);
					break;
				case IMAGETYPE_GIF:
					imagegif($n_i,$temp_new_img,100);
					break;
				default:
					return PENNY_CMS_URL_PATH.$img;
			}
		}
		return str_replace(PENNY_CMS_DOC_DIR, PENNY_CMS_URL_DIR, $temp_new_img);
	}
	return NULL;
}

function get_img ($img) {
	if (strlen($img)==0 || strpos($img, '://')>0) {
		return $img;
	}
	
	return ((strpos($img, '/')===0||strpos($img, '\\')===0)?PENNY_CMS_URL_PATH:PENNY_CMS_URL_DIR).$img;
}

function get_video ($_movie, $_width, $_height, $_flashvars='', $_transparent='transparent') {
	if (strpos($_movie, 'application/x-shockwave-flash')===false) {
		return create_flash(PENNY_CMS_URL_PATH."/Player.swf", $_width, $_height+50, "xml=".PENNY_CMS_URL_PATH."/player.php?file_name=".get_url_path($_movie).'&file_width='.'40'.'&file_height='.'30');
	} else {
		return $_movie;
	}
}

/**
 * create_flash
 * create flash object element.
 * @$file_name	file name.
 * return null.
 */
function create_flash ($_movie, $_width, $_height, $_flashvars='', $_transparent='transparent') {
	$temp_str=
			'<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0"'.
			'height="'.$_height.'"'.
			'width="'.$_width.'">'.
			'<param name="movie" value="'.$_movie.'"/>'.
			'<param name="quality" value="high"/>'.
			'<param name="scale" value="exactfit"/>'.
			'<param name="flashvars" value="'.$_flashvars.'"/>'.
			'<param name="wmode" value="'.$_transparent.'"/>'.
			'<embed '.
			'flashvars="'.$_flashvars.'"'.
			'height="'.$_height.'"'.
			'pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash"'.
			'quality="high"'.
			'scale="exactfit"'.
			'src="'.$_movie.'"'.
			'type="application/x-shockwave-flash"'.
			'width="'.$_width.'"'.
			'wmode="'.$_transparent.'"'.
			'/>'.
			'</object>'
		;
	
	return $temp_str;
}

/**
 * calculate the pages.
 * 
 */
function page_calculate($page_count=1, $specify_page=1, $display_pages=9) {
	
	$specify_page = $specify_page > $page_count ? $page_count : $specify_page;
	$specify_page = $specify_page < 1 ? 1 : $specify_page;
	
	if ($display_pages>1 && $display_pages%2==0) {
		$display_pages--;
	}
	
	if ($page_count > $display_pages) {
		$half_pages = ceil(($display_pages - 1) / 2);
	
		$loop_page = $display_pages;
		
		$first_page = 1;
		$previous_page = ($specify_page - 1) < 1 ? 1 : $specify_page - 1;
		$next_page = ($specify_page + 1) > $page_count ? $page_count : $specify_page + 1;
		$last_page = $page_count;
		
		if (($specify_page - $half_pages) < 1) {
			$specify_page = $half_pages + 1;
		}
		
		if (($specify_page + $half_pages) > $page_count) {
			$specify_page = $page_count - $half_pages;
		}
	} else {
		$half_pages = $specify_page - 1;
		$loop_page = $page_count;
		
		$first_page = 1;
		$previous_page = ($specify_page - 1) < 1 ? 1 : $specify_page - 1;
		$next_page = ($specify_page + 1) > $page_count ? $page_count : $specify_page + 1;
		$last_page = $page_count;
		$display_pages = $page_count;
	}
	
	$pages_info['half_pages'] = $half_pages;
	$pages_info['first_page'] = $first_page;
	$pages_info['previous_page'] = $previous_page;
	$pages_info['next_page'] = $next_page;
	$pages_info['last_page'] = $last_page;
	$pages_info['display_pages'] = $display_pages;
	$pages_info['specify_page'] = $specify_page;
	
	return $pages_info;
}

function get_random_code ($code_length=6, $code_type=2) {
	$code_array=array(
		'23456789',
		'abcdefghijkmnpqrstuvwxyz',
		'ABCDEFGHJKLMNPQRTUVWXYZ',
		'~!@#$%^&*()-_+=[{]}|;:,<.>/?',
		'0I1loSO'
	);
//	if ($code_type==1) {
//		$password_chars='abcdefghijklmnopqrstuvwxyz1234567890';
//	} else if ($code_type==2) {
//		$password_chars='AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz~1!2@3#4$5%6^7&8*9(0)-_+=[{]}|;:,<.>/?';
//	} else {
//		$password_chars='AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz23456789';
//	}
	$code_type=$code_type<0?0:$code_type>4?4:$code_type;
	$random_code='';
	for ($index=0; $index<$code_length; $index++) {
		$_index=mt_rand(0, $code_type);
		$random_code.=$code_array[$_index][mt_rand(0, strlen($code_array[$_index])-1)];
	}
	return $random_code;
}


function backupZipFile ($_db_name, $_table_names, $_export_file_type='arr', $_is_file=true, $_start_row=0, $_row_count=65536) {
	if (class_exists('ZipArchive')) {
		$_file_name=PENNY_CMS_DOC_PATH.'/data/'.$_db_name.date('_Y_m_d_H_i_s').'.zip';
		$zip = new ZipArchive();
		if ($zip->open($_file_name, ZIPARCHIVE::CREATE)===TRUE) {
			if (!is_array($_table_names)) {
				$_table_names=explode(',', $_table_names);
			}
			foreach ($_table_names as $_table_name) {
				if ($_table_name) {
					$zip->addFromString($_table_name.'.'.$_export_file_type, backupDatabase($_export_file_type, $_is_file, $_table_name, $_start_row, $_row_count));
				}
			}
			$zip->close();
		}
		return true;
	} else {
		$_file_name=PENNY_CMS_DOC_PATH.'/data/'.$_db_name.date('_Y_m_d_H_i_s').'.php';
		$contents='<?php'."\r\n";
		if (!is_array($_table_names)) {
			$_table_names=explode(',', $_table_names);
		}
		foreach ($_table_names as $_table_name) {
			if ($_table_name) {
				$contents.=backupDatabase($_export_file_type, $_is_file, $_table_name, $_start_row, $_row_count);
			}
		}
		$contents.='?>';
		$handle=fopen($_file_name, 'w');
		if (!$handle) {
			return false;
		} else {
			fwrite($handle, $contents);
			fclose($handle);
		}
		return true;
	}
}



/**
 * write_common_user_popedom_file.
 */
function write_resource_file () {
	$query='select _key as select_key, _value as select_value, _language as language from '.PENNY_CMS_DB_TABLE_PREFIX.'_resource where _status="true" order by _language';
	$results=create_query($query);
	global $RESOURCE_FILE;
	$RESOURCE_FILE=array();
	if ($results) {
		foreach ($results as $record) {
			$language=$record['language'];
			$key=$record['select_key'];
			$value=$record['select_value'];
			
			if (array_key_exists($language, $RESOURCE_FILE)) {
			} else {
				$RESOURCE_FILE[$language]=array();
			}
			$RESOURCE_FILE[$language][$key]=$value;
		}
//		$fp=fopen(PENNY_CMS_LIST_DOC_DIR.'resource.php', 'w');
//		fwrite($fp, serialize($RESOURCE_FILE));
//		fclose($fp);
		write_file('../data/list/resource.php', "<?php\r\n".get_pennycms_license().'$RESOURCE_FILE='.var_export($RESOURCE_FILE, TRUE)."\r\n?>");
	}
}

/**
 * get_name
 * Return resource name then contain key else return key string.
 * $key resource key.
 * return resource name.
 */
function get_name ($key, $return_language=null) {
	if (!isset($return_language)) {
		$return_language=isset($_SESSION[PENNY_CMS_TAG.'session_language'])?$_SESSION[PENNY_CMS_TAG.'session_language']:null;
	}
	$return_language=$return_language?$return_language:'zh_cn';
//	echo '$return_language:'.$return_language;
	global $RESOURCE_FILE;
	if (empty($RESOURCE_FILE[$return_language][$key])) {
		return $key;
	} else {
		return $RESOURCE_FILE[$return_language][$key];
	}
}

/**
 * get_names
 * Return resource name then contain key else return key string.
 * $key resource key.
 * return resource name.
 */
function get_names ($keys, $return_language=null, $in_separator=',', $out_separator=',') {
	if (count($keys)==0) {
		return '';
	}
	if (!isset($return_language)) {
		$return_language=isset($_SESSION[PENNY_CMS_TAG.'session_language'])?$_SESSION[PENNY_CMS_TAG.'session_language']:null;
	}
	$return_language=$return_language?$return_language:'zh_cn';
	global $RESOURCE_FILE;
	if (!is_array($keys)) {
		$keys=explode($in_separator, $keys);
	}
	$values=array();
	foreach ($keys as $key) {
		if (empty($RESOURCE_FILE[$return_language][$key])) {
			array_push($values, $key);
		} else {
			array_push($values, $RESOURCE_FILE[$return_language][$key]);
		}
	}
	return implode($out_separator, $values);
}


function read_file ($file_name, $is_show_log=FALSE) {
	$contents='';
	try {
		$handle=fopen($file_name, 'r');
//		if ($is_show_log) {
//			echo '$handle : '.$handle."\r\n<br />";
//		}
		if (!$handle) {
		} else {
			while (!feof($handle)) {
				$contents.=fgets($handle, 128);
			}
			fclose($handle);
		}
	} catch (Exception $e) {
		if ($is_show_log) {
			echo $e;
			echo "\r\n<br />";
		}
	}
	return $contents;
}

function write_file ($file_name, $contents='', $is_show_log=FALSE) {
	try {
		$handle = fopen($file_name, 'w');
		if (!$handle) {
			return false;
		} else {
			fwrite($handle, $contents);
			fclose($handle);
		}
		
		return true;
	} catch (Exception $e) {
		if ($is_show_log) {
			echo $e;
			echo "\r\n<br />";
		}
		
		return false;
	}
	
	return true;
}

function read_write_file ($read_file_name, $write_file_name, $search='', $replace='', $is_show_log=FALSE) {
	$contents=read_file($read_file_name, $is_show_log);
	if (strlen($replace)>0) {
		$contents=str_replace($search, $replace, $contents);
	}
	if ($is_show_log) {
		echo '$contents.strlen() : '.strlen($contents);
		echo "\r\n<br />";
	}
	write_file($write_file_name, $contents, $is_show_log);
}

/**
 * del_file
 * if folder exists then delete.
 * @$path	folder path.
 * return null.
 */
function del_folder ($path) {
	if (file_exists($path)) {
		if (is_dir($path)) {
			$entries = array();
			
			if ($handle = opendir($path)) {
				while (false !== ($file = readdir($handle))) {
					$entries[] = $file;
//					echo $file."<br />";
				}
				closedir($handle);
			}
			
			foreach ($entries as $entry) {
				if ($entry != '.' && $entry != '..') {
					del_folder($path.'/'.$entry);
				}
			}
			
			return @ rmdir($path);
		} else {
			return @ unlink($path);
		}
	} else {
		return FALSE;
	}
}


function encrypt ($string, $operation, $key='') {
    $key=md5($key);
    $key_length=strlen($key);
    $string=$operation?base64_decode($string):substr(md5($string.$key),0,8).$string;
    $string_length=strlen($string);
    $rndkey=$box=array();
    $result='';
    for ($i=0;$i<=255;$i++) {
        $rndkey[$i]=ord($key[$i%$key_length]);
        $box[$i]=$i;
    }
    for ($j=$i=0;$i<256;$i++) {
        $j=($j+$box[$i]+$rndkey[$i])%256;
        $tmp=$box[$i];
        $box[$i]=$box[$j];
        $box[$j]=$tmp;
    }
    for ($a=$j=$i=0;$i<$string_length;$i++) {
        $a=($a+1)%256;
        $j=($j+$box[$a])%256;
        $tmp=$box[$a];
        $box[$a]=$box[$j];
        $box[$j]=$tmp;
        $result.=chr(ord($string[$i])^($box[($box[$a]+$box[$j])%256]));
    }
    if ($operation) {
        if (substr($result,0,8)==substr(md5(substr($result,8).$key),0,8)) {
            return substr($result,8);
        } else {
            return'';
        }
    } else {
        return str_replace('=','',base64_encode($result));
    }
}

function script_nat ($NATS_CACHE) {
	
}

function get_pennycms_license () {
	return  
'/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
'
;
}
?>