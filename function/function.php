<?php
function write_file ($file_name, $contents='', $is_show_log=FALSE) {
	try {
		$handle=fopen($file_name, 'w');
		if (!$handle) {
			return false;
		} else {
			fwrite($handle, $contents);
			fclose($handle);
		}
		
		return true;
	} catch (Exception $e) {
		if ($is_show_log) {
			echo $e;
			echo "\r\n<br />";
		}
		
		return false;
	}
	
	return true;
}

function encrypt ($string, $operation, $key='') {
    $key=md5($key);
    $key_length=strlen($key);
    $string=$operation?base64_decode($string):substr(md5($string.$key),0,8).$string;
    $string_length=strlen($string);
    $rndkey=$box=array();
    $result='';
    for ($i=0;$i<=255;$i++) {
        $rndkey[$i]=ord($key[$i%$key_length]);
        $box[$i]=$i;
    }
    for ($j=$i=0;$i<256;$i++) {
        $j=($j+$box[$i]+$rndkey[$i])%256;
        $tmp=$box[$i];
        $box[$i]=$box[$j];
        $box[$j]=$tmp;
    }
    for ($a=$j=$i=0;$i<$string_length;$i++) {
        $a=($a+1)%256;
        $j=($j+$box[$a])%256;
        $tmp=$box[$a];
        $box[$a]=$box[$j];
        $box[$j]=$tmp;
        $result.=chr(ord($string[$i])^($box[($box[$a]+$box[$j])%256]));
    }
    if ($operation) {
        if (substr($result,0,8)==substr(md5(substr($result,8).$key),0,8)) {
            return substr($result,8);
        } else {
            return'';
        }
    } else {
        return str_replace('=','',base64_encode($result));
    }
}

function script_nat ($NATS_CACHE) {
	
}
?>