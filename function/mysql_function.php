<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/

function connect ($PENNY_CMS_DB_SERVER=null, $PENNY_CMS_DB_NAME=null, $PENNY_CMS_DB_USER_NAME=null, $PENNY_CMS_DB_PASSWORD=null) {
	$conn = mysql_pconnect(
			$PENNY_CMS_DB_SERVER?$PENNY_CMS_DB_SERVER:PENNY_CMS_DB_SERVER, 
			$PENNY_CMS_DB_USER_NAME?$PENNY_CMS_DB_USER_NAME:PENNY_CMS_DB_USER_NAME, 
			$PENNY_CMS_DB_PASSWORD?$PENNY_CMS_DB_PASSWORD:PENNY_CMS_DB_PASSWORD
		) or die("Connect DB Error!" . mysql_error());
//	$conn = mysql_connect(PENNY_CMS_DB_SERVER, PENNY_CMS_DB_USER_NAME, PENNY_CMS_DB_PASSWORD) or die("Connect DB Error!" . mysql_error());
	mysql_select_db($PENNY_CMS_DB_NAME?$PENNY_CMS_DB_NAME:PENNY_CMS_DB_NAME) or die("Select DB Error!" . mysql_error());
	
	return $conn;
}

function disconnect ($conn) {
	mysql_close($conn);
}

function begin_transaction () {

}

function rollback_transaction () {

}

function commit_transaction () {

}

function create_execute ($sql) {
	$conn = connect();
	$results = mysql_query($sql, $conn);	// process update.
	$affected_rows = mysql_affected_rows();
	if ($affected_rows>=0) {	// if save or update success.
		return $affected_rows;
	} else {	// if save or update error.
		echo "Execute Query:<br/>".$sql."<br/>";
		echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
		return false;
	}
}

function create_update ($sql) {
	$conn = connect();
	$results = mysql_query($sql, $conn);	// process update.
	if (mysql_affected_rows()>=0) {	// if save or update success.
		return true;
	} else {	// if save or update error.
		echo "Update Query:<br/>".$sql."<br/>";
		echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
		return false;
	}
}

function create_insert ($sql) {
	$conn = connect();
	$results = mysql_query($sql, $conn);	// process insert.
	
	if (mysql_affected_rows() >= 0) {	// if save or update success.
		return mysql_insert_id();
	} else {	// if save or update error.
		echo "Insert Query:<br/>".$sql."<br/>";
		echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
		return false;
	}
}

function create_delete ($sql) {
	$conn = connect();
	$results = mysql_query($sql, $conn);	// process delete.
	
	if (mysql_affected_rows() >= 0) {	// if save or update success.
//		return mysql_affected_rows();
		return true;
	} else {	// if save or update error.
		echo "Delete Query:<br/>".$sql."<br/>";
		echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
		return false;
	}
}

function create_find ($sql) {
	$conn = connect();
	$results = mysql_query($sql, $conn);	// process select.
	
	if ($results) {
		$row_count = mysql_num_rows($results);	// get row count.
		$record=array();
		for ($i=0; $i < $row_count; $i++) {
			/* fetch data. begin */
			$row = mysql_fetch_assoc($results);
			$record = $row;
		}
		/* fetch data. end */
		@mysql_free_result($results);	// release sql resource.
		
		return $record;
	} else {
		echo "Find Query:<br/>".$sql."<br/>";
		echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
		return false;
	}
}

function create_query ($sql) {
	$conn = connect();
	$results = mysql_query($sql, $conn);	// process select.
	
	if ($results) {
		$row_count = mysql_num_rows($results);	// get row count.
		
		$record=array();
		
		for ($i=0; $i < $row_count; $i++) {
			/* fetch data. begin */
//			$row = mysql_fetch_array($results, MYSQL_NUM);
			$row = mysql_fetch_assoc($results);
//			echo "ab:".count($row);
			$record[$i]=$row;
		}
		/* fetch data. end */
		@mysql_free_result($results);	// release sql resource.
		
		return $record;
	} else {
		echo "Create Query:<br/>".$sql."<br/>";
		echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
		return false;
	}
}
?>