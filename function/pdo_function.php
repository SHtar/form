<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/

global $DB_CONNECT;
$DB_CONNECT=null;

function connect ($PENNY_CMS_DB_SERVER=null, $PENNY_CMS_DB_NAME=null, $PENNY_CMS_DB_USER_NAME=null, $PENNY_CMS_DB_PASSWORD=null) {
	global $DB_CONNECT;
	
	// PDO. sqlite
	$DB_CONNECT=new PDO(
			'sqlite:../data/'.($PENNY_CMS_DB_NAME?$PENNY_CMS_DB_NAME:PENNY_CMS_DB_NAME).'.php',
			$PENNY_CMS_DB_USER_NAME?$PENNY_CMS_DB_USER_NAME:PENNY_CMS_DB_USER_NAME, 
			$PENNY_CMS_DB_PASSWORD?$PENNY_CMS_DB_PASSWORD:PENNY_CMS_DB_PASSWORD
		);	// sqlite
//	array(PDO::ATTR_PERSISTENT=>true)
	$DB_CONNECT->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
//	// Mysql.
//	$DB_CONNECT=mysql_pconnect(PENNY_CMS_DB_SERVER, PENNY_CMS_DB_USER_NAME, PENNY_CMS_DB_PASSWORD) or die("Connect DB Error!" . mysql_error());
//	mysql_select_db(PENNY_CMS_DB_NAME) or die("Select DB Error!" . mysql_error());
//	return $DB_CONNECT;
	
	
//	$DB_CONNECT=mysql_connect(PENNY_CMS_DB_SERVER, PENNY_CMS_DB_USER_NAME, PENNY_CMS_DB_PASSWORD) or die("Connect DB Error!" . mysql_error());
//	mysql_select_db(PENNY_CMS_DB_NAME) or die("Select DB Error!" . mysql_error());
//	return $DB_CONNECT;
}

function disconnect () {
	global $DB_CONNECT;
	if (isset($DB_CONNECT)) {
		if ($DB_CONNECT instanceof PDO) {
			$DB_CONNECT=null;
		} else {
			mysql_close($DB_CONNECT);
		}
	}
}

function begin_transaction () {
	global $DB_CONNECT;
	$DB_CONNECT->beginTransaction();
}

function rollback_transaction () {
	global $DB_CONNECT;
	$DB_CONNECT->rollBack();
}

function commit_transaction () {
	global $DB_CONNECT;
	$DB_CONNECT->commit();
}

function create_execute ($sql) {
	global $DB_CONNECT;
	if (!isset($DB_CONNECT)||empty($DB_CONNECT)) {
		connect();
	}
	
	if ($DB_CONNECT instanceof PDO) {
//		echo 'test : '.str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql);
		$affected=$DB_CONNECT->exec(str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql));
		if ($affected>=0) {	// if save or update success.
			return $affected;
		} else {
			echo 'Execute Query:<br/>'.$sql.'<br/>';
			echo $DB_CONNECT->errorCode().'<br/>';
			print_r($DB_CONNECT->errorInfo());
			return false;
		}
	} else {
		$results=mysql_query($sql, $DB_CONNECT);	// process update.
		$affected=mysql_affected_rows();
		if ($affected>=0) {	// if save or update success.
			return $affected;
		} else {	// if save or update error.
			echo "Execute Query:<br/>".$sql."<br/>";
			echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
			return false;
		}
	}
}

function create_update ($sql) {
	global $DB_CONNECT;
	if (!isset($DB_CONNECT)||empty($DB_CONNECT)) {
		connect();
	}
	
	if ($DB_CONNECT instanceof PDO) {
//		echo 'test : '.str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql);
		$affected=$DB_CONNECT->exec(str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql));
		if ($affected >= 0) {	// if save or update success.
			return true;
		} else {
			echo 'Updat Query:<br/>'.$sql.'<br/>';
			echo $DB_CONNECT->errorCode().'<br/>';
			print_r($DB_CONNECT->errorInfo());
			return false;
		}
	} else {
		$results = mysql_query($sql, $DB_CONNECT);	// process update.
		if (mysql_affected_rows() >= 0) {	// if save or update success.
			return true;
		} else {	// if save or update error.
			echo "Update Query:<br/>".$sql."<br/>";
			echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
			return false;
		}
	}
}

function create_insert ($sql) {
	global $DB_CONNECT;
	if (!isset($DB_CONNECT)||empty($DB_CONNECT)) {
		connect();
	}
	
	if ($DB_CONNECT instanceof PDO) {
		$DB_CONNECT->exec(str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql));
		$id=$DB_CONNECT->lastInsertId();
		if ($id>=0) {
			return $id;
		}
		echo 'Insert Query:<br/>'.$sql.'<br/>';
		echo $DB_CONNECT->errorCode().'<br/>';
		print_r($DB_CONNECT->errorInfo());
		return false;
	} else {
		$results = mysql_query($sql, $DB_CONNECT);	// process insert.
		if (mysql_affected_rows() >= 0) {	// if save or update success.
			return mysql_insert_id();
		} else {	// if save or update error.
			echo "Insert Query:<br/>".$sql."<br/>";
			echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
			return false;
		}
	}
}

function create_delete ($sql) {
	global $DB_CONNECT;
	if (!isset($DB_CONNECT)||empty($DB_CONNECT)) {
		connect();
	}
	if ($DB_CONNECT instanceof PDO) {
		$affected=$DB_CONNECT->exec(str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql));
		if ($affected >= 0) {	// if save or update success.
			return true;
		} else {
			echo 'Delete Query:<br/>'.$sql.'<br/>';
			echo $DB_CONNECT->errorCode().'<br/>';
			print_r($DB_CONNECT->errorInfo());
			return false;
		}
	} else {
		$results = mysql_query($sql, $DB_CONNECT);	// process delete.
		if (mysql_affected_rows() >= 0) {	// if save or update success.
			return true;
		} else {	// if save or update error.
			echo "Delete Query:<br/>".$sql."<br/>";
			echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
			return false;
		}
	}
}

function create_find ($sql) {
	global $DB_CONNECT;
	if (!isset($DB_CONNECT)||empty($DB_CONNECT)) {
		connect();
	}
	if ($DB_CONNECT instanceof PDO) {
		$result=$DB_CONNECT->query(str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql));
		if ($DB_CONNECT->errorCode()!='00000'){
			echo 'Find Query:<br/>'.$sql.'<br/>';
			echo $DB_CONNECT->errorCode().'<br/>';
			print_r($DB_CONNECT->errorInfo());
			return false;
		}
//		foreach ($result->fetch(PDO::FETCH_ASSOC) as $row) {
//		}
		$record=array();
		while($row=$result->fetch(PDO::FETCH_ASSOC)){
//			echo '$row:'.$row[''];
			$record=$row;
		}
//		echo $record;
//		if (get_magic_quotes_gpc()) {
//			foreach ($record as $key=>$value) {
//				$record[$key] = stripslashes($value);
//			}
//		}
		return $record;
//	if ($DB_CONNECT instanceof PDO) {
//		$rows=$DB_CONNECT->query($sql);
//		if ($rows) {
//			while($row=$rows->fetch(PDO::FETCH_ASSOC)){
//			echo '$row:'.$row;
//			}
//			return $row;
//		}
//		return array();
	} else {
		$results = mysql_query($sql, $DB_CONNECT);	// process select.
		
		if ($results) {
			$row_count = mysql_num_rows($results);	// get row count.
			
			for ($index=0; $index<$row_count; $index++) {
				/* fetch data. begin */
				$row = mysql_fetch_array($results);
				$record = $row;
			}
			/* fetch data. end */
			@mysql_free_result($results);	// release sql resource.
			
			return $record;
		} else {
			echo "Find Query:<br/>".$sql."<br/>";
			echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
			return false;
		}
	}
}

function create_query ($sql) {
	global $DB_CONNECT;
	if (!isset($DB_CONNECT)||empty($DB_CONNECT)) {
		connect();
	}
	if ($DB_CONNECT instanceof PDO) {
		$result=$DB_CONNECT->query(str_replace(array('\"', '\n', "\\'"), array('""', "\n", '\''), $sql));
		if ($DB_CONNECT->errorCode()!='00000'){
			echo 'Find Query:<br/>'.$sql.'<br/>';
			echo $DB_CONNECT->errorCode().'<br/>';
			print_r($DB_CONNECT->errorInfo());
			return false;
		}
		$results = $result->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	} else {
		$results = mysql_query($sql, $DB_CONNECT);	// process select.
		
		if ($results) {
			$row_count = mysql_num_rows($results);	// get row count.
			
			for ($index=0; $index<$row_count; $index++) {
				/* fetch data. begin */
				$row = mysql_fetch_assoc($results);
				$record[$index]=$row;
			}
			/* fetch data. end */
			@mysql_free_result($results);	// release sql resource.
			
			return $record;
		} else {
			echo "Create Query:<br/>".$sql."<br/>";
			echo "Error:<br/>".mysql_errno().": ".mysql_error()."<br/>";
			return false;
		}
	}
}
?>