<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/

/**
 * Save or Update data.
 * @$table table name.
 * @$result data array.
 * @$primary_key primary key, user save or update.
 */
function save_or_update ($table, $result, $primary_key, $is_add_slashes=TRUE) {
	$sql='';
	if (isset($result[$primary_key]) && $result[$primary_key]) {		// update;
		$sql.='update '.$table.' set ';
		$temp=array();
		foreach ($result as $key=>$value) {	// key = value.
			if ('_version_num'==$key) {
				$temp[]=$key.'='.($value+1);
			} else {
				if ($is_add_slashes) {
					$temp[]=$key.'="'.addslashes($value).'"';
				} else {
					$temp[]=$key.'="'.$value.'"';
				}
			}
		}
		$sql.=join($temp, ',');
		$sql.=' where '.$primary_key.'='.$result[$primary_key];
		$sql.=' and _version_num='.$result['_version_num'];
//		echo 'update $sql:'.$sql."\n";
		if (create_update($sql))	{	// update sql.
			return $result[$primary_key];
		} else {
			return false;
		}
	} else {	// insert;
		$sql.='insert into '.$table.' (';
		$temp=array();
		foreach ($result as $key=>$value) {	// key.
			if ($key!=$primary_key) {
				$temp[]=$key;
			}
		}
		$sql.=join($temp, ',');
		$sql.=') values (';
		$temp=array();
		foreach ($result as $key=>$value) {	// value.
			if ($key!=$primary_key) {
				if ($is_add_slashes) {
					$temp[]='"'.addslashes($value).'"';
				} else {
					$temp[]='"'.$value.'"';
				}
			}
		}
		$sql.=join($temp, ',');
		$sql.=');';
//		echo 'insert $sql:'.$sql."\n";
		return create_insert($sql);	// insert sql.
	}
}

/*
 * count
 */
function create_count ($table, $where) {
	$sql='select count(*) as _count from '.$table.' where 1=1 '.$where;
	$sql=str_replace('where 1=1 and ', 'where ', $sql);
	$sql=str_replace('where 1=1 or ', 'where ', $sql);
	$row=create_find($sql);
	if (isset($row['_count'])) {
		return $row['_count'];
	} else {
		return -1;
	}
}

/*
 * count
 */
function create_duplicate_id ($table, $_duplicate_id, $where) {
	$sql='select '.$_duplicate_id.' as _duplicate_id from '.$table.' where 1=1 '.$where;
	$sql=str_replace('where 1=1 and ', 'where ', $sql);
	$sql=str_replace('where 1=1 or ', 'where ', $sql);
	$row=create_find($sql);
	return $row["_duplicate_id"];
}

/*
 * query
 */
function get_results ($table, $where='', $this_page=1, $show_rows=100, $order_by=' 1 asc ', $pass_rows=0) {
	$this_page=intval($this_page)>1?$this_page:1;
	$start_row=($this_page - 1) * intval($show_rows) + intval($pass_rows);
	$start_row=$start_row>0?$start_row:0;
	$sql=
		' select * from '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$table.
		' where 1=1 '.$where.
		($order_by?(' order by '.$order_by):'').
		' limit '.$start_row.', '.$show_rows;
	$sql=str_replace('where 1=1 and ', 'where ', $sql);
	$sql=str_replace('where 1=1 or ', 'where ', $sql);
	return create_query($sql);	// process select.
}

/*
 * query
 */
function get_record ($table, $where='', $this_page=1, $show_rows=1, $order_by=' 1 desc ') {
	$this_page=intval($this_page)>1?$this_page:1;
	$sql=
		' select * from '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$table.
		' where 1=1 '.$where.
		($order_by?(' order by '.$order_by):'').
		' limit '.(($this_page - 1) * $show_rows).', '.$show_rows;
	$sql=str_replace('where 1=1 and ', 'where ', $sql);
	$sql=str_replace('where 1=1 or ', 'where ', $sql);
	return create_find($sql);	// process select.
}

/*
 * query
 */
function get_show_count ($table, $where, $use_status=TRUE) {
	global $STATIC;
	$sql=
		' select count(*) as _count from '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$table.
		' where '.
		($use_status?' _status="true" ':' _status!="" ').
		' and ((_start_date_time<="'.date('Y-m-d H:i:s').'" and _close_date_time>"'.date('Y-m-d H:i:s').'")or(_start_date_time="" and _close_date_time>"'.date('Y-m-d H:i:s').'")or(_start_date_time="" and _close_date_time="")) '.
//		' and (_language = "'.$_SESSION[PENNY_CMS_TAG.'session_language'].'" or _language="") '.
		$where;
	$row=create_find($sql);	// process select.
	if (isset($row['_count'])) {
		return $row['_count'];
	} else {
		return -1;
	}
}

/*
 * query
 */
function get_show_record ($table, $where, $order_by='desc', $sort='asc', $use_status=TRUE) {
	global $STATIC;
	$sql=
		' select * from '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$table.
		' where '.
		($use_status?' _status="true" ':' _status!="" ').
		' and ((_start_date_time<="'.date('Y-m-d H:i:s').'" and _close_date_time>"'.date('Y-m-d H:i:s').'")or(_start_date_time="" and _close_date_time>"'.date('Y-m-d H:i:s').'")or(_start_date_time="" and _close_date_time="")) '.
//		' and (_language = "'.$_SESSION[PENNY_CMS_TAG.'session_language'].'" or _language="") '.
		$where.
		' order by _order_by '.$order_by.', _'.$table.'_id '.$sort.
		' limit 0,1';
	
	return create_find($sql);	// process select.
}

/*
 * query
 */
function get_show_results ($table, $where, $this_page, $show_rows, $pass_rows=0, $use_status=TRUE, $order_by=NULL) {
	$this_page=intval($this_page)>1?$this_page:1;
	$start_row=($this_page - 1) * intval($show_rows) + intval($pass_rows);
	$start_row=$start_row>0?$start_row:0;
	$sql=
		' select * from '.PENNY_CMS_DB_TABLE_PREFIX.'_'.$table.
		' where '.
		($use_status?' _status="true" ':' _status!="" ').
		' and ((_start_date_time<="'.date('Y-m-d H:i:s').'" and _close_date_time>"'.date('Y-m-d H:i:s').'")or(_start_date_time="" and _close_date_time>"'.date('Y-m-d H:i:s').'")or(_start_date_time="" and _close_date_time="")) '.
//		' and (_language = "'.$_SESSION[PENNY_CMS_TAG.'session_language'].'" or _language="") '.
		$where.
		' order by '.($order_by?$order_by:'_order_by desc, _'.$table.'_id desc ').
		' limit '.$start_row.', '.$show_rows;
	return create_query($sql);	// process select.
}
?>