<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
date_default_timezone_set('PRC');
ini_set('memory_limit', -1);
set_time_limit(0);

session_start();	//  start session

require('setting.php');	// setting file.
require('path.php');	// path file.

$_SESSION[PENNY_CMS_TAG.'session_language']='zh_cn';

global $LIST_FILE;
if (file_exists(PENNY_CMS_LIST_DOC_DIR.'list.php')) {
	require(PENNY_CMS_LIST_DOC_DIR.'list.php');	// function file.
	if (!is_array($LIST_FILE)) {
		$LIST_FILE=array();
	}
} else {
	$LIST_FILE=array();
}

global $RESOURCE_FILE;
if (file_exists(PENNY_CMS_LIST_DOC_DIR.'resource.php')) {
	require(PENNY_CMS_LIST_DOC_DIR.'resource.php');	// function file.
	if (!is_array($RESOURCE_FILE)) {
		$RESOURCE_FILE=array();
	}
} else {
	$RESOURCE_FILE=array();
}

require('utils.php');	// utils file.
if ('sqlite'==PENNY_CMS_DB_DRIVE) {
	require('pdo_function.php');
} else {
	require('mysql_function.php');
}
require('sql_function.php');	// SQL

//  language session variable.
if (!isset($_SESSION[PENNY_CMS_TAG.'session_language'])) {
	$_SESSION[PENNY_CMS_TAG.'session_language']=PENNY_CMS_DEFAULT_LANGUAGE;
	if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		$HTTP_ACCEPT_LANGUAGE=strtolower(str_replace('-', '_', $_SERVER['HTTP_ACCEPT_LANGUAGE']));
		if (!defined('PENNY_CMS_SYSTEM_LANGUAGE')) {
//			$_SESSION[PENNY_CMS_TAG.'session_language']=PENNY_CMS_DEFAULT_LANGUAGE;
		} else {
			if (strpos(PENNY_CMS_SYSTEM_LANGUAGE, $HTTP_ACCEPT_LANGUAGE)===false) {
//				$_SESSION[PENNY_CMS_TAG.'session_language']=PENNY_CMS_DEFAULT_LANGUAGE;
			} else {
				$_SESSION[PENNY_CMS_TAG.'session_language']=$HTTP_ACCEPT_LANGUAGE;
			}
		}
	}
}

// module name.
if (isset($module_key)) {
	$module_name=get_name($module_key);
} else {
	$module_name='';
}
?>