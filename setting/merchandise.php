<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
$SETTING=array(
array('_field_name'=>'_merchandise_id', '_label_name'=>get_name('_merchandise_id'), '_edit_field'=>'system_id', '_sql_type'=>'integer', '_config'=>'config', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'0', '_gridy'=>'0'),

array(
'_field_name'=>'_products_id', 
'_label_name'=>get_name('_products_id'),
'_edit_field'=>'relate_text',

'_module'=>'products','_key'=>'_products_id','_value'=>'_products_id',



'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'0'
),
array(
'_field_name'=>'_products_type', 
'_label_name'=>get_name('_products_type'),
'_edit_field'=>'checkbox',
'_list_name'=>'news_type',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'0'
),
array(
'_field_name'=>'_products_name', 
'_label_name'=>get_name('_products_name'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'0'
),
array(
'_field_name'=>'_news_id', 
'_label_name'=>get_name('_news_id'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'0'
),
array(
'_field_name'=>'_news_type', 
'_label_name'=>get_name('_news_type'),
'_edit_field'=>'selects',
'_list_name'=>'news_type',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'0'
),
array(
'_field_name'=>'_custom_code', 
'_label_name'=>get_name('_custom_code'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'1'
),
array(
'_field_name'=>'_hs_code', 
'_label_name'=>get_name('_hs_code'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'1'
),
array(
'_field_name'=>'_barcode', 
'_label_name'=>get_name('_barcode'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'1'
),
array(
'_field_name'=>'_model_no', 
'_label_name'=>get_name('_model_no'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'1'
),
array(
'_field_name'=>'_item_no', 
'_label_name'=>get_name('_item_no'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'1'
),
array(
'_field_name'=>'_sales_dept', 
'_label_name'=>get_name('_sales_dept'),
'_edit_field'=>'suggest',

'_module'=>'sales_dept','_map'=>'_sales_dept','_key'=>'_sales_dept_id','_value'=>'_sales_dept_id,_sales_dept_actual_name','_like'=>'_sales_dept_id,_sales_dept_name,_sales_dept_actual_name','_order_by'=>'_sales_dept_name asc','_type'=>'1',



'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'1'
),
array(
'_field_name'=>'_brand', 
'_label_name'=>get_name('_brand'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'2'
),
array(
'_field_name'=>'_supplyer', 
'_label_name'=>get_name('_supplyer'),
'_edit_field'=>'relate_name_text',

'_module'=>'client',



'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'11',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'2'
),
array(
'_field_name'=>'_collection', 
'_label_name'=>get_name('_collection'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'2'
),
array(
'_field_name'=>'_style', 
'_label_name'=>get_name('_style'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'2'
),
array(
'_field_name'=>'_alias', 
'_label_name'=>get_name('_alias'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'2'
),
array(
'_field_name'=>'_category', 
'_label_name'=>get_name('_category'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'3'
),
array(
'_field_name'=>'_merchandise', 
'_label_name'=>get_name('_merchandise'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'3'
),
array(
'_field_name'=>'_standard', 
'_label_name'=>get_name('_standard'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'3'
),
array(
'_field_name'=>'_color', 
'_label_name'=>get_name('_color'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'3'
),
array(
'_field_name'=>'_units', 
'_label_name'=>get_name('_units'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'4'
),
array(
'_field_name'=>'_unit_cost', 
'_label_name'=>get_name('_unit_cost'),
'_edit_field'=>'currency',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'38',
'_default_value'=>"0.00",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'4'
),
array(
'_field_name'=>'_unit_price', 
'_label_name'=>get_name('_unit_price'),
'_edit_field'=>'currency',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'38',
'_default_value'=>"0.00",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'4'
),
array(
'_field_name'=>'_unit_value', 
'_label_name'=>get_name('_unit_value'),
'_edit_field'=>'currency',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'38',
'_default_value'=>"0.00",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'4'
),
array(
'_field_name'=>'_amount', 
'_label_name'=>get_name('_amount'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'4'
),
array(
'_field_name'=>'_abbreviate', 
'_label_name'=>get_name('_abbreviate'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'4'
),
array(
'_field_name'=>'_title', 
'_label_name'=>get_name('_title'),
'_edit_field'=>'html_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'5'
),
array(
'_field_name'=>'_hint', 
'_label_name'=>get_name('_hint'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'5'
),
array(
'_field_name'=>'_preview', 
'_label_name'=>get_name('_preview'),
'_edit_field'=>'html_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'5'
),
array(
'_field_name'=>'_tag', 
'_label_name'=>get_name('_tag'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'5'
),
array(
'_field_name'=>'_author', 
'_label_name'=>get_name('_author'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'5'
),
array(
'_field_name'=>'_source', 
'_label_name'=>get_name('_source'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'5'
),
array(
'_field_name'=>'_gross_weight', 
'_label_name'=>get_name('_gross_weight'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'6'
),
array(
'_field_name'=>'_weight', 
'_label_name'=>get_name('_weight'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'6'
),
array(
'_field_name'=>'_size', 
'_label_name'=>get_name('_size'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'6'
),
array(
'_field_name'=>'_volume', 
'_label_name'=>get_name('_volume'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'6'
),
array(
'_field_name'=>'_pqty', 
'_label_name'=>get_name('_pqty'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'6'
),
array(
'_field_name'=>'_moq', 
'_label_name'=>get_name('_moq'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'6'
),
array(
'_field_name'=>'_warning_upper', 
'_label_name'=>get_name('_warning_upper'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'7'
),
array(
'_field_name'=>'_warning_lower', 
'_label_name'=>get_name('_warning_lower'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'7'
),
array(
'_field_name'=>'_warning_sell', 
'_label_name'=>get_name('_warning_sell'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'7'
),
array(
'_field_name'=>'_producing_area', 
'_label_name'=>get_name('_producing_area'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'7'
),
array(
'_field_name'=>'_useful_life', 
'_label_name'=>get_name('_useful_life'),
'_edit_field'=>'integer',





'_config'=>'',
'_sql_type'=>'integer',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'7'
),
array(
'_field_name'=>'_useful_life_units', 
'_label_name'=>get_name('_useful_life_units'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'7'
),
array(
'_field_name'=>'_selling_amount', 
'_label_name'=>get_name('_selling_amount'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'8'
),
array(
'_field_name'=>'_selling_total_amount', 
'_label_name'=>get_name('_selling_total_amount'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'8'
),
array(
'_field_name'=>'_warehouse_amount', 
'_label_name'=>get_name('_warehouse_amount'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'8'
),
array(
'_field_name'=>'_warehouse_total_amount', 
'_label_name'=>get_name('_warehouse_total_amount'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'8'
),
array(
'_field_name'=>'_inner_quantity', 
'_label_name'=>get_name('_inner_quantity'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'9'
),
array(
'_field_name'=>'_inner_units', 
'_label_name'=>get_name('_inner_units'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'9'
),
array(
'_field_name'=>'_inner_weight', 
'_label_name'=>get_name('_inner_weight'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'9'
),
array(
'_field_name'=>'_package_quantity', 
'_label_name'=>get_name('_package_quantity'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'9'
),
array(
'_field_name'=>'_package_units', 
'_label_name'=>get_name('_package_units'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'9'
),
array(
'_field_name'=>'_package_weight', 
'_label_name'=>get_name('_package_weight'),
'_edit_field'=>'number',





'_config'=>'',
'_sql_type'=>'decimal',
'_length'=>'11',
'_default_value'=>"0",
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'9'
),
array(
'_field_name'=>'_file', 
'_label_name'=>get_name('_file'),
'_edit_field'=>'file_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'10'
),
array(
'_field_name'=>'_files', 
'_label_name'=>get_name('_files'),
'_edit_field'=>'files_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'10'
),
array(
'_field_name'=>'_video', 
'_label_name'=>get_name('_video'),
'_edit_field'=>'file_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'10'
),
array(
'_field_name'=>'_videos', 
'_label_name'=>get_name('_videos'),
'_edit_field'=>'files_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'10'
),
array(
'_field_name'=>'_banner', 
'_label_name'=>get_name('_banner'),
'_edit_field'=>'photo_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'10'
),
array(
'_field_name'=>'_banners', 
'_label_name'=>get_name('_banners'),
'_edit_field'=>'photos_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'10'
),
array(
'_field_name'=>'_image', 
'_label_name'=>get_name('_image'),
'_edit_field'=>'photo_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'11'
),
array(
'_field_name'=>'_images', 
'_label_name'=>get_name('_images'),
'_edit_field'=>'photos_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'11'
),
array(
'_field_name'=>'_photo', 
'_label_name'=>get_name('_photo'),
'_edit_field'=>'photo_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'11'
),
array(
'_field_name'=>'_photos', 
'_label_name'=>get_name('_photos'),
'_edit_field'=>'photos_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'11'
),
array(
'_field_name'=>'_picture', 
'_label_name'=>get_name('_picture'),
'_edit_field'=>'photo_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'11'
),
array(
'_field_name'=>'_pictures', 
'_label_name'=>get_name('_pictures'),
'_edit_field'=>'photos_text',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'11'
),
array(
'_field_name'=>'_remark', 
'_label_name'=>get_name('_remark'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'12'
),
array(
'_field_name'=>'_content', 
'_label_name'=>get_name('_content'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'12'
),
array(
'_field_name'=>'_details', 
'_label_name'=>get_name('_details'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'12'
),
array(
'_field_name'=>'_json', 
'_label_name'=>get_name('_json'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'12'
),
array(
'_field_name'=>'_materials', 
'_label_name'=>get_name('_materials'),
'_edit_field'=>'array_structure',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'12'
),
array(
'_field_name'=>'_procedures', 
'_label_name'=>get_name('_procedures'),
'_edit_field'=>'array_structure',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'12'
),
array(
'_field_name'=>'_extend_a', 
'_label_name'=>get_name('_extend_a'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'13'
),
array(
'_field_name'=>'_extend_b', 
'_label_name'=>get_name('_extend_b'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'13'
),
array(
'_field_name'=>'_extend_c', 
'_label_name'=>get_name('_extend_c'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'13'
),
array(
'_field_name'=>'_extend_d', 
'_label_name'=>get_name('_extend_d'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'13'
),
array(
'_field_name'=>'_extend_e', 
'_label_name'=>get_name('_extend_e'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'13'
),
array(
'_field_name'=>'_other', 
'_label_name'=>get_name('_other'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'13'
),
array(
'_field_name'=>'_start_date_time', 
'_label_name'=>get_name('_start_date_time'),
'_edit_field'=>'date_time',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'14'
),
array(
'_field_name'=>'_close_date_time', 
'_label_name'=>get_name('_close_date_time'),
'_edit_field'=>'date_time',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'14'
),
array(
'_field_name'=>'_start_time', 
'_label_name'=>get_name('_start_time'),
'_edit_field'=>'date_time',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'14'
),
array(
'_field_name'=>'_close_time', 
'_label_name'=>get_name('_close_time'),
'_edit_field'=>'date_time',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'14'
),
array(
'_field_name'=>'_start_term', 
'_label_name'=>get_name('_start_term'),
'_edit_field'=>'array_structure',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'14'
),
array(
'_field_name'=>'_close_term', 
'_label_name'=>get_name('_close_term'),
'_edit_field'=>'array_structure',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'14'
),
array(
'_field_name'=>'_comments', 
'_label_name'=>get_name('_comments'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'15'
),
array(
'_field_name'=>'_likes', 
'_label_name'=>get_name('_likes'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'15'
),
array(
'_field_name'=>'_hits', 
'_label_name'=>get_name('_hits'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'15'
),
array(
'_field_name'=>'_visits', 
'_label_name'=>get_name('_visits'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'15'
),
array(
'_field_name'=>'_votes', 
'_label_name'=>get_name('_votes'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'15'
),
array(
'_field_name'=>'_commend', 
'_label_name'=>get_name('_commend'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'15'
),
array(
'_field_name'=>'_setting', 
'_label_name'=>get_name('_setting'),
'_edit_field'=>'char',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'16'
),
array(
'_field_name'=>'_order_by', 
'_label_name'=>get_name('_order_by'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'16'
),
array(
'_field_name'=>'_comment_status', 
'_label_name'=>get_name('_comment_status'),
'_edit_field'=>'radio',
'_list_name'=>'status',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>"true",
'_duplicate_default_value'=>"true",
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'16'
),
array(
'_field_name'=>'_commend_status', 
'_label_name'=>get_name('_commend_status'),
'_edit_field'=>'radio',
'_list_name'=>'status',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>"true",
'_duplicate_default_value'=>"true",
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'16'
),
array(
'_field_name'=>'_sell_status', 
'_label_name'=>get_name('_sell_status'),
'_edit_field'=>'radio',
'_list_name'=>'sell_status',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>"true",
'_duplicate_default_value'=>"true",
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'16'
),
array(
'_field_name'=>'_status', 
'_label_name'=>get_name('_status'),
'_edit_field'=>'radio',
'_list_name'=>'status',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>"true",
'_duplicate_default_value'=>"true",
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'16'
),

array('_field_name'=>'_create_user', '_label_name'=>get_name('_create_user'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'0', '_gridy'=>'10000'),
array('_field_name'=>'_create_date_time', '_label_name'=>get_name('_create_date_time'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'1', '_gridy'=>'10000'),
array('_field_name'=>'_update_user', '_label_name'=>get_name('_update_user'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'2', '_gridy'=>'10000'),
array('_field_name'=>'_update_date_time', '_label_name'=>get_name('_update_date_time'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'3', '_gridy'=>'10000'),
array('_field_name'=>'_version_num', '_label_name'=>get_name('_version_num'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'0', '_duplicate_default_value'=>'0', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'4', '_gridy'=>'10000')

);
?>