<?php
/*** ***
License
This software is published under the BSD license as listed below.
 
Copyright (c) 2007 pennycms.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

 . Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 

 . Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution. 

 . Neither the name of the pennycms.com nor the names of its contributors 
   may be used to endorse or promote products derived from this software without 
   specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*** ***/
?>
<?php
$SETTING=array(
array('_field_name'=>'_words_id', '_label_name'=>get_name('_words_id'), '_edit_field'=>'system_id', '_sql_type'=>'integer', '_config'=>'config', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'0', '_gridy'=>'0'),

array(
'_field_name'=>'_words_type', 
'_label_name'=>get_name('_words_type'),
'_edit_field'=>'selects',
'_list_name'=>'words_type',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'5',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'4',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'0'
),
array(
'_field_name'=>'_chapter', 
'_label_name'=>get_name('_chapter'),
'_edit_field'=>'module_trees',
'_list_name'=>'chapter',

'_module'=>'chapters','_key'=>'_chapter','_value'=>'_chapter_name',
'_parent'=>'_course',


'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'7',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'7',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'0'
),
array(
'_field_name'=>'_english', 
'_label_name'=>get_name('_english'),
'_edit_field'=>'suggest',

'_module'=>'words','_key'=>'_words_id,_words_type,_chapter,_english,_english_voice,_phonetic,_property,_phrase,_sentence,_chinese,_chinese_voice,_content,_details,_remark,_json,_status,_create_user,_create_date_time,_update_user,_update_date_time,_version_num','_value'=>'_english,_chinese','_like'=>'_english','_order_by'=>'_english asc','_type'=>'1',



'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'9',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'9',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',

'_required'=>'true',

'_gridx'=>'3',
'_gridy'=>'1'
),
array(
'_field_name'=>'_english_voice', 
'_label_name'=>get_name('_english_voice'),
'_edit_field'=>'audio',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'4',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'4',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'1'
),
array(
'_field_name'=>'_phonetic', 
'_label_name'=>get_name('_phonetic'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'5',
'_gridy'=>'2'
),
array(
'_field_name'=>'_property', 
'_label_name'=>get_name('_property'),
'_edit_field'=>'select',
'_list_name'=>'property',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'50',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'6',
'_gridy'=>'2'
),
array(
'_field_name'=>'_phrase', 
'_label_name'=>get_name('_phrase'),
'_edit_field'=>'select',
'_list_name'=>'property',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'7',
'_gridy'=>'2'
),
array(
'_field_name'=>'_sentence', 
'_label_name'=>get_name('_sentence'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'8',
'_gridy'=>'2'
),
array(
'_field_name'=>'_chinese', 
'_label_name'=>get_name('_chinese'),
'_edit_field'=>'text',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'9',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'9',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'9',
'_gridy'=>'3'
),
array(
'_field_name'=>'_chinese_voice', 
'_label_name'=>get_name('_chinese_voice'),
'_edit_field'=>'audio',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'255',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'4',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'4',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'10',
'_gridy'=>'3'
),
array(
'_field_name'=>'_content', 
'_label_name'=>get_name('_content'),
'_edit_field'=>'checkbox',
'_list_name'=>'language',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'0',
'_gridy'=>'4'
),
array(
'_field_name'=>'_details', 
'_label_name'=>get_name('_details'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'1',
'_gridy'=>'4'
),
array(
'_field_name'=>'_remark', 
'_label_name'=>get_name('_remark'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'2',
'_gridy'=>'4'
),
array(
'_field_name'=>'_json', 
'_label_name'=>get_name('_json'),
'_edit_field'=>'text_area',





'_config'=>'',
'_sql_type'=>'text',
'_length'=>'20000',
'_default_value'=>'',
'_duplicate_default_value'=>'',
'_show'=>'',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'3',
'_gridy'=>'4'
),
array(
'_field_name'=>'_status', 
'_label_name'=>get_name('_status'),
'_edit_field'=>'radio',
'_list_name'=>'status',





'_config'=>'',
'_sql_type'=>'varchar',
'_length'=>'100',
'_default_value'=>"true",
'_duplicate_default_value'=>"true",
'_show'=>'true',
'_table_width'=>'',
'_table_height'=>'',
'_table_class'=>'',
'_table_style'=>'',
'_field_width'=>'',
'_field_height'=>'',
'_field_class'=>'',
'_field_style'=>'',
'_label_width'=>'',
'_label_height'=>'',
'_label_class'=>'',
'_label_style'=>'',
'_input_width'=>'',
'_input_height'=>'',
'_input_class'=>'',
'_input_style'=>'',


'_gridx'=>'4',
'_gridy'=>'5'
),

array('_field_name'=>'_create_user', '_label_name'=>get_name('_create_user'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'0', '_gridy'=>'10000'),
array('_field_name'=>'_create_date_time', '_label_name'=>get_name('_create_date_time'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'1', '_gridy'=>'10000'),
array('_field_name'=>'_update_user', '_label_name'=>get_name('_update_user'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'2', '_gridy'=>'10000'),
array('_field_name'=>'_update_date_time', '_label_name'=>get_name('_update_date_time'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'', '_duplicate_default_value'=>'', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'3', '_gridy'=>'10000'),
array('_field_name'=>'_version_num', '_label_name'=>get_name('_version_num'), '_edit_field'=>'hidden_text', '_config'=>'config', '_sql_type'=>'varchar', '_length'=>'255', '_default'=>'0', '_duplicate_default_value'=>'0', '_show'=>'true', '_table_width'=>'', '_table_height'=>'', '_table_class'=>'', '_table_style'=>'', '_label_width'=>'', '_label_height'=>'', '_label_class'=>'', '_label_style'=>'', '_field_width'=>'', '_field_height'=>'', '_field_class'=>'', '_field_style'=>'', '_gridx'=>'4', '_gridy'=>'10000')

);
?>